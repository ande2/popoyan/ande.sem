






{ TABLE "informix".pemddatossap row size = 1298 number of columns = 9 index size 
              = 0 }
create table "informix".pemddatossap 
  (
    idboletaing integer not null ,
    numordpro varchar(255),
    tipoart smallint,
    codart varchar(255),
    descart varchar(255),
    costoart decimal(18,2),
    cantart smallint,
    codalmart varchar(255),
    cuentaafe varchar(255)
  );
revoke all on "informix".pemddatossap from "public" as "informix";




