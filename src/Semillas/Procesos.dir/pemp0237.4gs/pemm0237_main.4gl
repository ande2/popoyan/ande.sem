DATABASE db0001 

--Registro para conexion a SAP
DEFINE gr_con RECORD    
  ipserver      VARCHAR (15) ,
  userDB        VARCHAR (80) ,
  passDB        VARCHAR (80) ,
  userSap       VARCHAR (80) ,
  passSap       VARCHAR (80)
END RECORD

--Registro de trabajo
DEFINE gr_reg RECORD 
    idSapPro              LIKE sapProducirEnc.idsappro ,
	docNum                LIKE sapProducirEnc.docnum  ,
	docType               LIKE sapProducirEnc.doctype  ,
	docDate               LIKE sapProducirEnc.docdate  ,
	docDueDate            LIKE sapProducirEnc.docduedate  ,
	docCurrency           LIKE sapProducirEnc.doccurrency  ,
	Reference1            LIKE sapProducirEnc.reference1  ,
	Comments              LIKE sapProducirEnc.comments  ,
	TaxDate               LIKE sapProducirEnc.taxdate  ,
	Series                LIKE sapProducirEnc.series  ,
	idBoletaIng           LIKE sapProducirEnc.idboletaing   ,
	estado                LIKE sapProducirEnc.estado  ,
	fectran               LIKE sapProducirEnc.fectran   ,
	usuOpero              LIKE sapProducirEnc.usuopero
END RECORD 

MAIN

--Librería empacada

--Paso 1 Conector SAP:
--Función que recibe los parámetros de conexión al servidor 
--Parametros: IP del servidor, usuario DB, contraseña DB, Usuario SAP, 
--contraseña SAP, etc), esta clase se puede conectar a una empresa que se envíe 
--como parámetro y no creará ningúna ventana de logeo, solo recibirá los datos 
--como parámetros de función, la función devuelve un INT indicando el resultado 
--de la operación

 --a. Obtener parametros para conexion
 LET gr_con.ipserver = getValParam("SAP - IP SERVIDOR")
 LET gr_con.userDB   = getValParam("SAP - USUARIO DB")
 LET gr_con.passDB   = getValParam("SAP - CONTRASEÑA DB")
 LET gr_con.userSap  = getValParam("SAP - USUARIO SAP")
 LET gr_con.passSap  = getValParam("SAP - CONTRASEÑA SAP")
 
 DISPLAY "IP        ", gr_con.ipserver
 DISPLAY "UserDB    ", gr_con.userDB
 DISPLAY "PassDB    ", gr_con.passDB
 DISPLAY "UserSap   ", gr_con.userSap
 DISPLAY "PassSap   ", gr_con.passSap

--Paso 2 Validación de costos: 
--Función interna que verificará en donde sea necesario que el costo del producto 
--que se está enviando como parámetro esté acorde al que tiene el sistema
--Parámetros: 
--Valores que retorna: Res (INT) indica el resultado de la operación

--Paso 3 Función crearReciboProducción: 
--Función que creará un recibo de producción. 
--Parámetros: Número de orden de producción, fecha de contabilización, 
--serie, un array de códigos de productos, un array con las descripciones de 
--los productos, un array con las cantidades de los productos, 
--un array con los costos de los productos.
--Valores que retorna: Res (INT) indica el resultado de la operación

--Paso 4 Función crearSalidaInventario: 
--Función que crea una salida de inventario. 
--Parámetros: fecha de contabilización, serie, array de cantidades de productos, 
--array de descripción de productos, array de costo de productos.
--Valores que retorna: Res (INT) indica el resultado de la operación

--Paso 5 Función crearEntradaInventario: 
--Función que crea una entrada de inventario
--Parámetros: fecha de contabilización, serie, array de cantidades de productos, 
--array de descripción de productos, array de costo de productos, 
--array de cuentas de inventario.
--Valores que retorna: Res (INT) indica el resultado de la operación

--Paso 6 Función crearEntradaMercancia: 
--Función que crea una entrada de mercancia.
--Parámetros: fecha de contabilización, serie, código de socio de negocio, 
--array de cantidades de productos, array de descripción de productos, 
--array de costo de productos.
--Valores que retorna: Res (INT) indica el resultado de la operación.

--Todos los documentos anteriores crearán la trazabilidad, log y partidas 
--contables exactamente igual a si se hicieran dentro de SAP B1

--Fin

--Llenar tabla de encabezado con datos para SAP solamente para las boletas locales
--Produccion / Recibo de produccion

DECLARE cur01 CURSOR FOR 
SELECT idboletaing 
FROM pemmboleta 
WHERE tipoBoleta = 1 --locales
AND estado = 0       --Cerradas
AND idboletaing NOT IN 
    (SELECT idboletaing FROM sapProducirEnc) --no han sido procesadas
FOREACH cur01 INTO gr_reg.idBoletaIng
  DISPLAY gr_reg.idBoletaIng
  --Generar archivo para envio
    --idSapPro
  LET gr_reg.idSapPro = 0 --serial
  --docNum
  SELECT idOrdPro INTO gr_reg.docNum FROM pemMBoleta 
  WHERE idboletaing = gr_reg.idBoletaIng
  --docType
  --LET gr_reg.docType = ??  
	{docDate            
	docDueDate         
	docCurrency        
	Reference1         
	Comments           
	TaxDate            
	Series             
	idBoletaIng        
	estado             
	fectran            
	usuOpero           
  SELECT }
END FOREACH  




--Ejecutar proceso 

--Actualizar estado de 

END MAIN
