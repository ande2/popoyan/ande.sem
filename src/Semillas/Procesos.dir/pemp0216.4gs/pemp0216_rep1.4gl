################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
--SCHEMA db0000
GLOBALS "pemp0207_glob.4gl"


DEFINE i SMALLINT 
DEFINE grRep RECORD LIKE pemMBoleta.*
DEFINE pLine VARCHAR(255)

FUNCTION mainReport1(lresp)
DEFINE lresp INTEGER

  --PROMPT "Boleta? " FOR resp
  START REPORT rRep
  CALL report1(lresp)
  --SKIP 5 LINES
  FOR i = 1 TO  8
     OUTPUT TO REPORT rRep(grRep.*,pLine)
  END FOR 
  CALL report1(lresp)
  FINISH REPORT rRep
END FUNCTION  

FUNCTION report1(resp)  
DEFINE resp INTEGER  

  DEFINE vcoditem SMALLINT 
  DEFINE vnomItem VARCHAR(50)
  DEFINE vTipoCaja VARCHAR(50)
  
  DEFINE pos, vSumCajas SMALLINT 
  DEFINE vEmpresa LIKE ande:commemp.nombre
  DEFINE lDbname CHAR(6)
  DEFINE lCodigo VARCHAR(30) 
  DEFINE vUsuario char(10)
  DEFINE lVersion VARCHAR(30)
  DEFINE vEstructura VARCHAR(70)
  DEFINE vproductor VARCHAR(50) 
  DEFINE vEmplEntrego, vEmplRecibio VARCHAR(50)
  
  --DECLARE curRep CURSOR FOR 
      SELECT * 
        INTO grRep.* 
        FROM pemMBoleta
       WHERE idBoletaIng = resp

   --Para código
    SELECT valchr 
      INTO lCodigo
      FROM glb_paramtrs
      WHERE nompar = "RECEPCIÓN DE FRUTA - CODIGO" 
    LET pLine[65,99] = "Código: ", lCodigo 
    OUTPUT TO REPORT rRep(grRep.*,pLine)
    LET pLine = ""
    
    --Para empresa
    LET lDbname = arg_val(1)
    SELECT nombre INTO vEmpresa FROM ande:commemp a WHERE dbname = lDbname
    LET pLine[1,30]  = vEmpresa
    
    --Para titulo
    LET pLine[28,60] = "REGISTRO DE RECEPCIÓN DE FRUTA"
    
    --Version
    SELECT valchr 
      INTO lVersion
      FROM glb_paramtrs
      WHERE nompar = "RECEPCION DE FRUTA - VERSION"
    LET pLine[65,99] = "Versión: ", lVersion
    OUTPUT TO REPORT rRep(grRep.*,pLine)
    LET pLine = "" 

    --Fecha de elaboración
    SELECT valchr 
      INTO lVersion
      FROM glb_paramtrs
      WHERE nompar = "RECEPCION DE FRUTA - FECHA"
    LET pLine[65,99] = "Fecha de elaboración ", lVersion
    OUTPUT TO REPORT rRep(grRep.*,pLine)
    LET pLine = "" 

    --Páginas
    SELECT valchr 
      INTO lVersion
      FROM glb_paramtrs
      WHERE nompar = "RECEPCION DE FRUTA - PAGINAS"
    LET pLine[65,99] = "Páginas: ", lVersion
    OUTPUT TO REPORT rRep(grRep.*,pLine)
    LET pLine = "" 
    
    --, 
    --LET pLine[75,100] = "HOJA No.: 1" --, PAGENO USING "&&"
    --DISPLAY "pLine ", pLine
    --OUTPUT TO REPORT rRep(grRep.*,pLine)  
    --LET pLine = ""
    --

    --LET pLine[1,29]  = "FECHA   : ", TODAY USING "dd/mm/yyyy"
    --
    --
    --OUTPUT TO REPORT rRep(grRep.*,pLine)  
    --LET pLine = ""

    --Para usuario
    --LET vUsuario = get_AndeVarLog("LOGNAME")
    --IF vUsuario IS NULL THEN
       --LET vUsuario = "prueba"
    --END IF 
    
   
    --LET pLine[1,30]  = "USUARIO: ", vUsuario  
    --
    --OUTPUT TO REPORT rRep(grRep.*,pLine)  
    --LET pLine = ""
          
    LET pLine = "==============================================================================================="
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""

    --Numero de boleta
    LET pLine[65,99] = "No.: ", grRep.idboletaing USING "<<&,&&&"
    OUTPUT TO REPORT rRep(grRep.*,pLine)
    LET pLine = "" 

    
    --OUTPUT TO REPORT rRep(grRep.*,pLine)  

    LET pLine[1,50] = "INFORMACIÓN DE RECEPCIÓN"
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""

    IF grRep.tipoboleta = 1 THEN 
       --Para estructura
       SELECT trim(numordpro)||'-'||trim(nomordpro) INTO vEstructura
       FROM pemmordpro WHERE idordpro = grRep.idordpro
       LET pLine[1,70] = "Estructura     : ", vEstructura
       OUTPUT TO REPORT rRep(grRep.*,pLine)  
       LET pLine = ""
       
       LET pLine[1,50] = "Fecha documento: ", date(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)  
       LET pLine = ""
       
       LET pLine[1,50] = "Hora           : ", time(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)  
       LET pLine = ""
       
       LET pLine[1,50] = "# Documento    : ", grRep.docpropio
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       
    ELSE 
       SELECT trim(nombre) INTO vProductor FROM ande:commemp WHERE id_commemp = grRep.productor
       LET pLine[1,50] = "Productor      : ", vProductor
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       LET pLine[1,50] = "Fecha documento: ", date(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       LET pLine[1,50] = "Hora           : ", time(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       LET pLine[1,50] = "# Documento    : ", grRep.docproductor
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
    END IF 
   OUTPUT TO REPORT rRep(grRep.*,pLine)

   LET pLine[37,60] = "INFORMACIÓN DEL PRODUCTO"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
   
   LET pLine[37,60] = "========================"
   
   LET pLine = ""
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   
   LET pLine[1,10]  = "CÓDIGO" 
   LET pLine[15,35] = "PRODUCTO"
   LET pLine[40,55] = "TIPO CAJA"
   LET pLine[60,74] = "CANT CAJAS"
   LET pLine[75,99] = "PESO EN LBS"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
   
   LET pLine[1,10]  = "------" 
   LET pLine[15,35] = "-----------------------"
   LET pLine[40,55] = "--------------"
   LET pLIne[60,74] = "----------"
   LET pLine[75,99] = "-----------"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
   
   --LET pos = 1

   --Para codigo y nombre de item
   IF grRep.tipoboleta = 1 THEN 
          
      SELECT c.iditem, c.desitemlg
        INTO vCodItem, vNomItem
        FROM  pemmboleta a, pemDOrdPro b, glbmitems c
       WHERE a.idordpro = b.idordpro
         AND a.iddordpro = b.iddordpro
         AND b.iditem = c.iditem
         AND a.idboletaing = grRep.idboletaing
          
       LET pLine[1,14]  = vCodItem
       LET pLIne[15,39] = vNomItem
          
   ELSE
       SELECT b.idItem, b.desItemLg
         INTO vCodItem, vNomItem
         FROM pemmboleta a, glbMItems b
        WHERE a.idItem = b.idItem
          AND a.idboletaing = grRep.idboletaing
          
       LET pLine[1,14]  = vCodItem
       LET pLIne[15,39] = vNomItem
   END IF
   --Para Tipo de Caja
   SELECT b.nomcatempaque
     INTO vTipoCaja
     FROM pemMBoleta a, pemmcatemp b
    WHERE a.tipocaja = b.idcatempaque
      AND  a.idBoletaIng = grRep.idboletaing
   LET pLine[40,59] = vTipoCaja    
   --Total de cajas
   SELECT SUM(cantCajas) 
     INTO vSumCajas 
     FROM pemDBoleta
    WHERE idBoletaIng = grRep.idboletaing
   LET pLine[65,70] = vSumCajas

   --Peso total
   LET pLine[78,90] = grRep.pesototal
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""

   --SKIP 5 LINES
   FOR i = 1 TO  5
      OUTPUT TO REPORT rRep(grRep.*,pLine)
   END FOR 

   --Lineas de Piloto y Receptor
   LET pLine[5,53] = "-----------------------------------------"
   LET pLine[55,90] = "----------------------------------------"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""

   --Para nombre piloto
   IF grRep.tipoboleta = 1 THEN
      SELECT TRIM(b.nombre)||' '||TRIM(b.apellido)
        INTO vEmplEntrego
        FROM pemmboleta a, ande:commempl b
       WHERE a.emplentrego = b.id_commempl
         AND idboletaing = grRep.idboletaing
   ELSE 
      SELECT nomentrego 
        INTO vEmplEntrego
        FROM pemmboleta 
       WHERE idboletaing = grRep.idboletaing 
   END IF 
   --Para receptor
   SELECT TRIM(b.nombre)||' '||TRIM(b.apellido)
     INTO vEmplRecibio
     FROM pemmboleta a, ande:commempl b
    WHERE a.emplrecibio = b.id_commempl
      AND idboletaing = grRep.idboletaing 
      
   LET pLine[5,11] = "Piloto:"
   LET pLine[19,53] = vEmplEntrego
   LET pLine[55,64] = "Receptor:"
   LET pLine[65,95] = vEmplRecibio
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""

   
END FUNCTION 


REPORT rRep(lrRep,lLine)

DEFINE lrRep RECORD LIKE pemMBoleta.*
DEFINE lLine VARCHAR(255)

FORMAT 
    
    ON EVERY ROW 
       PRINT COLUMN 001, lLine

END REPORT 