
GLOBALS "pemp0207_glob.4gl"

DEFINE esPropia SMALLINT 

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE sql_stmt STRING 
DEFINE preCantCajas SMALLINT 

DEFINE sqlTxt STRING 

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      CALL cleanAll()
      --INITIALIZE g_reg.* TO NULL
      CALL dispReg()
      
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT --BY NAME  
      --g_reg.idBoletaIng, g_reg.fecha, g_reg.tipoBoleta, g_reg.estado 
      g_reg.idOrdPro, g_reg.idDOrdPro, g_reg.emplEntrego, g_reg.docPropio,
      --CS g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
      g_reg.tipoCaja, g_reg.tieneTarima
      --, g_reg.pesoTotal
      --g_reg.emplRecibio, g_reg.fecTran,
     FROM
      --fIdBoletaIng, fFecha, fTipoBoleta, fEstado, 
      fIdOrdPro, fIdDOrdPro, fEmplEntrego, fdocpropio,
      --fproductor, fIdItem, fNomEntrego, fdocproductor,
      --fEmplRecibio, fFecTran, 
      fTipoCaja, fTieneTarima
      --, fPesoTotal
      ATTRIBUTES (WITHOUT DEFAULTS)
      
      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)
         --Para id de boleta
         LET g_reg.idBoletaIng = 0 
         DISPLAY g_reg.idBoletaIng TO fidboletaing 
         
         --Para fecha
         LET g_reg.fecha = TODAY 
         DISPLAY g_reg.fecha TO fFecha

         --Para estado
         LET g_reg.estado = 1
         DISPLAY g_reg.estado TO festado

         LET g_reg.tieneTarima = 1
         LET g_reg.pesoTotal = 0.0

         --Para usuario que recibio - LOGNAME
         LET sql_stmt = "SELECT id_commempl ",
                        " FROM ande:commempl WHERE usuLogin = '", get_AndeVarLog("LOGNAME"), "'"
         PREPARE ex_emplRecibio FROM sql_stmt
         EXECUTE ex_emplRecibio INTO g_reg.emplRecibio
         IF g_reg.emplRecibio IS NULL THEN
            LET g_reg.emplRecibio = 100
         END IF
         DISPLAY g_reg.emplRecibio TO femplrecibio1  
         DISPLAY g_reg.emplRecibio TO femplrecibio  

      ON CHANGE fIdOrdPro
         LET sql_stmt = 
             "SELECT idDOrdPro, fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemCt ",
             " FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e ",
             " WHERE a.idfinca = b.idfinca ",
             " AND a.idestructura = c.idEstructura ",
             " AND a.idValvula = d.idValvula ",
             " AND a.idItem = e.idItem ",
             " AND a.idOrdPro = ", g_reg.idOrdPro
         CALL combo_din2("fiddordpro", sql_stmt)
         CALL cleanProductor()

      ON CHANGE fIdDOrdPro
         CALL cleanProductor()  

      ON CHANGE fEmplEntrego 
        CALL cleanProductor() 
        IF g_reg.emplEntrego IS NULL THEN
           CALL msg("Debe ingresar emplEntrego")
           NEXT FIELD CURRENT
        END IF  

      ON CHANGE fdocpropio
         SELECT FIRST 1 docPropio FROM pemMBoleta WHERE docPropio = g_reg.docPropio
         IF SQLca.sqlcode = 0 THEN
            CALL msg("Número de documento ya existe")
            NEXT FIELD CURRENT
         END IF 
      
      ON CHANGE fTipoCaja 
        IF g_reg.tipoCaja IS NULL THEN
           CALL msg("Debe ingresar tipoCaja")
           NEXT FIELD CURRENT
        END IF 
      
   END INPUT 

   INPUT ARRAY reg_detArr FROM sDet2.*
      ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE) --, DELETE ROW = FALSE)

      BEFORE INPUT
        CALL fgl_dialog_setkeylabel("Append", "")
    
      --BEFORE ROW 
      BEFORE FIELD scantcajas
         IF arr_curr() <> arr_count() THEN 
            CALL DIALOG.setCurrentRow("sdet2", arr_count())
         END IF 
         IF g_reg.tipoBoleta = 1 THEN
            IF g_reg.idOrdPro IS NULL THEN
               CALL msg("Debe ingresar orden de producción")
               NEXT FIELD fIdOrdPro
            END IF 
            IF g_reg.idDOrdPro IS NULL THEN
               CALL msg("Debe ingresar estructura")
               NEXT FIELD fIdDOrdPro
            END IF 
            IF g_reg.emplEntrego IS NULL THEN
               CALL msg("Debe seleccionar empleado que entrego")
               NEXT FIELD fEmplEntrego
            END IF
            IF g_reg.docPropio IS NULL THEN
               CALL msg("Debe ingresar numero de documento de entrega")
               NEXT FIELD fdocpropio
            END IF
         ELSE 
            IF g_reg.idItem IS NULL THEN
               CALL msg("Debe ingresar Item")
               NEXT FIELD fIdItem
            END IF  
            IF g_reg.nomEntrego IS NULL THEN
               CALL msg("Debe ingresar nombre de quién entregó el producto")
               NEXT FIELD fNomEntrego
            END IF  
            
         END IF 

         IF g_reg.tipoCaja IS NULL THEN
            CALL msg("Debe ingresar tipoCaja")
            NEXT FIELD fTipoCaja
         END IF
         --Para numero de linea
         LET reg_detArr[arr_curr()].idLinea = arr_curr()
         DISPLAY reg_detArr[arr_curr()].idLInea TO sdet2[scr_line()].sidlinea

      ON CHANGE scantcajas
         LET reg_detArr[arr_curr()].pesoBruto = NULL 
         LET reg_detArr[arr_curr()].pesoTara = NULL 
         LET reg_detArr[arr_curr()].pesoNeto = NULL 
         DISPLAY reg_detArr[arr_curr()].pesoBruto TO sdet2[scr_line()].spesobruto
         DISPLAY reg_detArr[arr_curr()].pesoTara TO sdet2[scr_line()].spesotara
         DISPLAY reg_detArr[arr_curr()].pesoNeto TO sdet2[scr_line()].spesoneto 
      
      ON ACTION leerPeso
        -- Desactivando opcion de grabar
        --CALL Dialog.SetActionActive("accept",FALSE)
        
        IF reg_detArr[arr_curr()].cantCajas IS NULL THEN 
           CALL msg("Debe ingresar # de Cajas")
           NEXT FIELD scantcajas
        ELSE 
           LET reg_detArr[arr_curr()].pesoBruto = getPeso()
           CALL calculaPesos(1)
           
           LET preCantCajas = reg_detArr[arr_curr()].cantCajas
           NEXT FIELD NEXT 
        END IF

        -- Calculando pesos 
        --CALL inving001_CalculaPesos()

        -- Activando opcion de grabar
        --CALL Dialog.SetActionActive("accept",TRUE)
        {
            -- Desactivando opcion de grabar
    CALL Dialog.SetActionActive("accept",FALSE)

    -- Verificando si captura de peso es de bascula
    IF NOT pesomanual THEN 
     -- Obteniendo peso de bascula
     LET fn = "bascula"||w_mae_psj.numbas 
     CALL librut001_leerbascula(fn,fs,ft,"G")
     RETURNING result,peso
     IF NOT result THEN
        NEXT FIELD numbas 
     END IF

     -- Verificando pesos
     IF peso <=0 THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Peso de bascula invalido.",
        "stop")
        NEXT FIELD numbas 
     END IF 

     -- Verificando que peso obtenido de bascula no sea mayor que maximo
     -- peso permitido
     IF peso>maxpesobascula THEN
        CALL fgl_winmessage(
        " Atencion:",
        " Peso de bascula mayor a maximo peso permitido. \n"||
        " Peso bascula [ "||peso||" ] \n"||
        " Maximo peso permitido [ "||maxpesobascula||" ]",
        "stop")
        NEXT FIELD numbas 
     END IF 
    END IF 

    -- Calculando datos 
    -- Verificando tipo de movimiento
    IF (w_mae_psj.tipmov=1) THEN   -- Entrada
       -- Si no es transito 
       IF NOT w_mae_psj.transt THEN 
          -- Si captura de peso es manual 
          IF pesomanual THEN
             -- Habilitando peso bruto
             CALL Dialog.SetFieldActive("pbruto",TRUE)
             NEXT FIELD pbruto 
          ELSE 
             -- Obteniendo peso bruto de bascula 
             LET w_mae_psj.pbruto = peso 
             DISPLAY BY NAME w_mae_psj.pbruto 
          END IF 
       ELSE -- Si es transito 
          -- Si captura de peso es manual
          IF pesomanual THEN
             -- Si no hay stadia 
             IF NOT w_mae_psj.stadia THEN 
                -- Habilitando peso tara  
                CALL Dialog.SetFieldActive("pstara",TRUE)
                NEXT FIELD pstara 
             ELSE
                -- Verificando no peso
                -- Si ya se peso bruto y se va apesar cabezal 
                IF (w_mae_psj.nopeso=1) THEN 
                   -- Habilitando peso cabezal ingreso 
                   CALL Dialog.SetFieldActive("pingcb",TRUE)
                   NEXT FIELD pingcb
                END IF 

                -- Si ya se peso cabezal ingreso, pregunta que se desea pesar
                -- si tara o cabezal de salida
                IF (w_mae_psj.nopeso=2) THEN 
                    -- Registradno nuevo dato
                    LET quepeso = librut001_yesornot(
                                  "Confirmacion",
                                  "Que Peso Desea Registrar ?",
                                  "Tara",
                                  "Salida Cabezal",
                                  "question")
                      
                    CASE (quepeso)
                     WHEN 1 
                      -- Habilitando peso tara  
                      LET w_mae_bit.tppeso  = "TA" 
                      LET w_mae_psj.psalcb  = 0 
                      LET w_mae_psj.mpsalcb = 0 
                      CALL Dialog.SetFieldActive("pstara",TRUE)

                      -- Calculando pesos 
                      CALL inving001_CalculaPesos() 

                      NEXT FIELD pstara 
                     WHEN 0 
                      -- Habilitando peso cabezal salida
                      LET w_mae_bit.tppeso  = "SC" 
                      LET w_mae_psj.pstara  = 0 
                      LET w_mae_psj.mpstara = 0 
                      CALL Dialog.SetFieldActive("psalcb",TRUE)

                      -- Calculando pesos 
                      CALL inving001_CalculaPesos() 

                      NEXT FIELD psalcb
                    END CASE 
                END IF 

                -- Si ya se peso tara o cabezal de salida se registra peso faltante
                IF (w_mae_psj.nopeso=3) THEN
                   -- Verificando el peso 3 regitrado
                   LET xtppeso = librut003_BNumeroPeso(w_mae_psj.lnktra,w_mae_psj.nopeso) 
                   CASE (xtppeso)
                    WHEN "TA" -- Si ya existe peso tara pide peso de cabezal de salida 
                     -- Habilitando peso cabezal salida
                     LET w_mae_bit.tppeso = "SC" 
                     CALL Dialog.SetFieldActive("psalcb",TRUE)
                     NEXT FIELD psalcb
                    WHEN "SC" -- Si ya existe peso salida cabezal pide peso tara 
                     -- Habilitando peso tara  
                     LET w_mae_bit.tppeso = "TA" 
                     CALL Dialog.SetFieldActive("pstara",TRUE)
                     NEXT FIELD pstara 
                   END CASE 
                END IF
             END IF 
          ELSE 
             -- Si no hay stadia 
             IF NOT w_mae_psj.stadia THEN 
                -- Obteniendo peso tara de bascula 
                LET w_mae_psj.pstara = peso 
             ELSE
                -- Verificando no peso
                -- Si ya se peso bruto y se va a pesar cabezal 
                IF (w_mae_psj.nopeso=1) THEN 
                  -- Obteniendo peso cabezal ingreso de bascula
                  LET w_mae_psj.pingcb = peso 
                END IF

                -- Si ya se peso cabezal ingreso, pregunta que se desea pesar
                -- si tara o cabezal de salida
                IF (w_mae_psj.nopeso=2) THEN 
                    -- Registradno nuevo dato
                    LET quepeso = librut001_yesornot(
                                  "Confirmacion",
                                  "Que Peso Desea Registrar ?",
                                  "Tara",
                                  "Salida Cabezal",
                                  "question")
                      
                    CASE (quepeso)
                     WHEN 1 
                      -- Obteniendo peso tara  
                      LET w_mae_psj.pstara = peso 
                      LET w_mae_bit.canpes = w_mae_psj.pstara
                      LET w_mae_bit.tppeso  = "TA" 
                      LET w_mae_psj.psalcb  = 0 
                      LET w_mae_psj.mpsalcb = 0 
                     WHEN 0 
                      -- Obteniendo peso cabezal salida
                      LET w_mae_psj.psalcb = peso 
                      LET w_mae_bit.canpes = w_mae_psj.psalcb 
                      LET w_mae_bit.tppeso  = "SC" 
                      LET w_mae_psj.pstara  = 0 
                      LET w_mae_psj.mpstara = 0 
                    END CASE 
                END IF 

                -- Si ya se peso tara o cabezal de salida se registra peso faltante
                IF (w_mae_psj.nopeso=3) THEN
                   -- Verificando el peso 3 regitrado
                   LET xtppeso = librut003_BNumeroPeso(w_mae_psj.lnktra,w_mae_psj.nopeso) 
                   CASE (xtppeso)
                    WHEN "TA" -- Si ya existe peso tara pide peso de cabezal de salida 
                     -- Habilitando peso cabezal salida
                     LET w_mae_psj.psalcb = peso 
                     LET w_mae_bit.canpes = w_mae_psj.psalcb 
                     LET w_mae_bit.tppeso = "SC" 
                    WHEN "SC" -- Si ya existe peso salida cabezal pide peso tara 
                     -- Habilitando peso tara  
                     LET w_mae_psj.pstara = peso 
                     LET w_mae_bit.canpes = w_mae_psj.pstara
                     LET w_mae_bit.tppeso = "TA" 
                   END CASE 
                END IF
             END IF 
          END IF 

          -- Calculando pesos
          CALL inving001_CalculaPesos() 

          -- Si no hay estadia 
          IF NOT w_mae_psj.stadia THEN 
             -- Verificando peso neto
             IF w_mae_psj.psneto<=0 THEN 
                CALL fgl_winmessage(
                "Atencion:",
                "Peso neto invalido, VERIFICA.",
                "stop")
                NEXT FIELD numbas 
             END IF 
          ELSE
             -- Verificando si ya se peso el bruto y se va pesar peso de cabezal
             IF (w_mae_psj.nopeso=1) THEN 
                -- Verificando peso de cabezal 
                IF (w_mae_psj.pingcb>=w_mae_psj.pbruto) THEN 
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Peso cabezal no puede ser mayor a peso bruto, VERIFICA.",
                   "stop")
                   NEXT FIELD numbas 
                END IF 
             END IF 

             -- Verificando si es ultimo peso
             IF (w_mae_psj.nopeso=3) THEN 
                -- Verificando peso neto
                IF w_mae_psj.psneto<=0 THEN 
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Peso neto invalido, VERIFICA.",
                   "stop")
                   NEXT FIELD numbas 
                END IF 
             ELSE 
                -- Verificando peso neto
                IF w_mae_psj.psneto<0 THEN 
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Peso neto invalido, VERIFICA.",
                   "stop")
                   NEXT FIELD numbas 
                END IF 
             END IF
          END IF
       END IF 
    ELSE                         -- Salida
       -- Si no es transito 
       IF NOT w_mae_psj.transt THEN 
          -- Si captura de peso es manual 
          IF pesomanual THEN
             -- Habilitando peso tara  
             CALL Dialog.SetFieldActive("pstara",TRUE)
             NEXT FIELD pstara 
          ELSE 
             -- Obteniendo peso tara de bascula  
             LET w_mae_psj.pstara = peso
             DISPLAY BY NAME w_mae_psj.pstara
          END IF 
       ELSE
          -- Si captura de peso es manual
          IF pesomanual THEN
             -- Si no hay stadia 
             IF NOT w_mae_psj.stadia THEN 
                -- Habilitando peso bruto
                CALL Dialog.SetFieldActive("pbruto",TRUE)
                NEXT FIELD pbruto 
             ELSE
                -- Verificando no peso
                -- Si ya se peso tara y se va a pesar cabezal salida
                IF (w_mae_psj.nopeso=1) THEN 
                   -- Habilitando peso cabezal salida   
                   CALL Dialog.SetFieldActive("psalcb",TRUE)
                   NEXT FIELD psalcb 
                END IF 

                -- Si ya se peso cabezal salida, pregunta que se desea pesar
                -- si bruto o cabezal de ingreso
                IF (w_mae_psj.nopeso=2) THEN 
                    -- Registradno nuevo dato
                    LET quepeso = librut001_yesornot(
                                  "Confirmacion",
                                  "Que Peso Desea Registrar ?",
                                  "Bruto",
                                  "Ingreso Cabezal",
                                  "question")

                    CASE (quepeso)
                     WHEN 1 
                      -- Habilitando peso bruto
                      LET w_mae_bit.tppeso  = "BT" 
                      LET w_mae_psj.pingcb  = 0 
                      LET w_mae_psj.mpingcb = 0 
                      CALL Dialog.SetFieldActive("pbruto",TRUE)

                      -- Calculando pesos 
                      CALL inving001_CalculaPesos() 

                      NEXT FIELD pbruto 
                     WHEN 0 
                      -- Habilitando peso cabezal ingreso
                      LET w_mae_bit.tppeso  = "IC" 
                      LET w_mae_psj.pbruto  = 0 
                      LET w_mae_psj.mpbruto = 0 
                      CALL Dialog.SetFieldActive("pingcb",TRUE)

                      -- Calculando pesos 
                      CALL inving001_CalculaPesos() 

                      NEXT FIELD pingcb
                    END CASE 
                END IF 

                -- Si ya se peso bruto o cabezal de ingreso se registra peso faltante
                IF (w_mae_psj.nopeso=3) THEN
                   -- Verificando el peso 3 regitrado
                   LET xtppeso = librut003_BNumeroPeso(w_mae_psj.lnktra,w_mae_psj.nopeso) 
                   CASE (xtppeso)
                    WHEN "BT" -- Si ya existe peso bruto pide peso de cabezal de ingreso
                     -- Habilitando peso cabezal ingreso
                     LET w_mae_bit.tppeso = "IC" 
                     CALL Dialog.SetFieldActive("pingcb",TRUE)
                     NEXT FIELD pingcb
                    WHEN "IC" -- Si ya existe peso ingreso cabezal pide peso bruto
                     -- Habilitando peso bruto
                     LET w_mae_bit.tppeso = "BT" 
                     CALL Dialog.SetFieldActive("pbruto",TRUE)
                     NEXT FIELD pbruto 
                   END CASE 
                END IF
             END IF 
          ELSE 
             -- Si no hay stadia 
             IF NOT w_mae_psj.stadia THEN 
                -- Obteniendo peso bruto de bascula 
                LET w_mae_psj.pbruto = peso 
                DISPLAY BY NAME w_mae_psj.pbruto 
             ELSE
                -- Verificando no peso
                -- Si ya se peso tara y se va a pesar cabezal salida
                IF (w_mae_psj.nopeso=1) THEN 
                   -- Obteniendo peso cabezal salida de bascula
                   LET w_mae_psj.psalcb = peso 
                END IF 

                -- Si ya se peso cabezal salida, pregunta que se desea pesar
                -- si bruto o cabezal de ingreso
                IF (w_mae_psj.nopeso=2) THEN 
                    -- Registradno nuevo dato
                    LET quepeso = librut001_yesornot(
                                  "Confirmacion",
                                  "Que Peso Desea Registrar ?",
                                  "Bruto",
                                  "Ingreso Cabezal",
                                  "question")

                    CASE (quepeso)
                     WHEN 1 
                      -- Obteniendo peso bruto
                      LET w_mae_psj.pbruto  = peso 
                      LET w_mae_bit.canpes  = w_mae_psj.pbruto 
                      LET w_mae_bit.tppeso  = "BT" 
                      LET w_mae_psj.pingcb  = 0 
                      LET w_mae_psj.mpingcb = 0 
                     WHEN 0 
                      -- Obteniendo peso cabezal ingreso 
                      LET w_mae_psj.pingcb = peso 
                      LET w_mae_bit.canpes = w_mae_psj.pingcb 
                      LET w_mae_bit.tppeso  = "IC" 
                      LET w_mae_psj.pbruto  = 0 
                      LET w_mae_psj.mpbruto = 0 
                    END CASE 
                END IF 

                -- Si ya se peso bruto o cabezal de ingreso se registra peso faltante
                IF (w_mae_psj.nopeso=3) THEN
                   -- Verificando el peso 3 regitrado
                   LET xtppeso = librut003_BNumeroPeso(w_mae_psj.lnktra,w_mae_psj.nopeso) 
                   CASE (xtppeso)
                    WHEN "BT" -- Si ya existe peso bruto pide peso de cabezal de ingreso
                     -- Habilitando peso cabezal ingreso
                     LET w_mae_psj.pingcb = peso 
                     LET w_mae_bit.canpes = w_mae_psj.pingcb 
                     LET w_mae_bit.tppeso = "IC" 
                    WHEN "IC" -- Si ya existe peso ingreso cabezal pide peso bruto
                     -- Habilitando peso bruto 
                     LET w_mae_psj.pbruto = peso 
                     LET w_mae_bit.canpes = w_mae_psj.pbruto 
                     LET w_mae_bit.tppeso = "BT" 
                   END CASE 
                END IF
             END IF 
          END IF 

          -- Calculando pesos
          CALL inving001_CalculaPesos() 

          -- Si no hay estadia 
          IF NOT w_mae_psj.stadia THEN 
             -- Verificando peso neto
             IF w_mae_psj.psneto<=0 THEN 
                CALL fgl_winmessage(
                "Atencion:",
                "Peso neto invalido, VERIFICA.",
                "stop")
                NEXT FIELD numbas 
             END IF 
          ELSE 
             -- Verificando si ya se peso tara y se va pesar cabezal salida 
             IF (w_mae_psj.nopeso=1) THEN 
                -- Verificando peso salida de cabezal
                IF (w_mae_psj.psalcb>=w_mae_psj.pstara) THEN 
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Peso cabezal no puede ser mayor a peso tara, VERIFICA.",
                   "stop")
                   NEXT FIELD numbas 
                END IF 
             END IF 

             -- Verificando si es ultimo peso
             IF (w_mae_psj.nopeso=3) THEN 
                -- Verificando peso neto
                IF w_mae_psj.psneto<=0 THEN 
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Peso neto invalido, VERIFICA.",
                   "stop")
                   NEXT FIELD numbas 
                END IF 
             ELSE 
                -- Verificando peso neto
                IF w_mae_psj.psneto<0 THEN 
                   CALL fgl_winmessage(
                   "Atencion:",
                   "Peso neto invalido, VERIFICA.",
                   "stop")
                   NEXT FIELD numbas 
                END IF 
             END IF 
          END IF
       END IF 
    END IF 

    -- Calculando pesos 
    CALL inving001_CalculaPesos()

    -- Activando opcion de grabar
    CALL Dialog.SetActionActive("accept",TRUE)
        }
        
      AFTER INSERT
         IF reg_detArr[arr_curr()].cantCajas IS NULL 
            OR reg_detArr[arr_curr()].pesoBruto IS NULL
            OR reg_detArr[arr_curr()].pesoTara IS NULL
            OR reg_detArr[arr_curr()].pesoTara IS NULL THEN
            CANCEL INSERT
         END IF
         --Revisa que la linea no se duplique 
         {IF repLine(arr_curr()) THEN 
            CALL msg("Línea repetida")
            NEXT FIELD sIdFinca
         END IF 

      AFTER INSERT
         IF d_gru[idx1] IS NULL THEN
            CANCEL INSERT
         END IF

      ON ACTION DELETE
         LET idx1 = arr_curr()
         CALL DIALOG.deleteRow("sdet1", idx1)}
      
   END INPUT 

   INPUT --BY NAME  
       g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
       g_reg.tipoCaja, g_reg.tieneTarima
      --, g_reg.pesoTotal
      --g_reg.emplRecibio, g_reg.fecTran,
     FROM
      fproductor, fIdItem, fNomEntrego, fdocproductor,
      --fEmplRecibio, fFecTran, 
      fTipoCaja1, fTieneTarima1
      --, fPesoTotal
      ATTRIBUTES (WITHOUT DEFAULTS)
   --g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor,
      --g_reg.tipoCaja, g_reg.tieneTarima

      ON CHANGE fproductor
         CALL cleanPropia() 
            --LET sql_stmt = ' SELECT a.idItem, trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg ', 
                  --' FROM glbMItems a, glbmfam b, glbmtip c ',
                  --' WHERE a.idfamilia = b.idfamilia ', 
                  --' AND a.idtipo = c.idtipo '
         LET sqlTxt = ' SELECT idItem, trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg  ',
                      ' FROM glbMItems a, glbmfam b, glbmtip c ',
                      ' WHERE a.idfamilia = b.idfamilia ',
                      ' AND a.idtipo = c.idtipo ',
                      ' AND a.idItem in (select iditem from ande:itemsxempresa where id_commemp =', g_reg.productor, ')'
         CALL combo_din2("fiditem", sqlTxt) 

    ON CHANGE fIdItem
         CALL cleanPropia()  
         
      
      ON CHANGE fNomEntrego 
        CALL cleanPropia()
        IF g_reg.nomEntrego IS NULL THEN
           CALL msg("Debe ingresar nomEntrego")
           NEXT FIELD CURRENT
        END IF  
    END INPUT 

    INPUT ARRAY reg_detArr2 FROM sDet3.*
      ATTRIBUTES ( WITHOUT DEFAULTS, INSERT ROW = FALSE) --, DELETE ROW = FALSE)

      BEFORE INPUT
        CALL fgl_dialog_setkeylabel("Append", "")
    
      --BEFORE ROW 
      BEFORE FIELD scantcajas1
         IF arr_curr() <> arr_count() THEN 
            CALL DIALOG.setCurrentRow("sdet3", arr_count())
         END IF 
         IF g_reg.tipoBoleta = 1 THEN
            IF g_reg.idOrdPro IS NULL THEN
               CALL msg("Debe ingresar orden de producción")
               NEXT FIELD fIdOrdPro
            END IF 
            IF g_reg.idDOrdPro IS NULL THEN
               CALL msg("Debe ingresar estructura")
               NEXT FIELD fIdDOrdPro
            END IF 
            IF g_reg.emplEntrego IS NULL THEN
               CALL msg("Debe seleccionar empleado que entrego")
               NEXT FIELD fEmplEntrego
            END IF
            IF g_reg.docPropio IS NULL THEN
               CALL msg("Debe ingresar numero de documento de entrega")
               NEXT FIELD fdocpropio
            END IF
         ELSE 
            IF g_reg.idItem IS NULL THEN
               CALL msg("Debe ingresar Item")
               NEXT FIELD fIdItem
            END IF  
            IF g_reg.nomEntrego IS NULL THEN
               CALL msg("Debe ingresar nombre de quién entregó el producto")
               NEXT FIELD fNomEntrego
            END IF  
            
         END IF 

         IF g_reg.tipoCaja IS NULL THEN
            CALL msg("Debe ingresar tipoCaja")
            NEXT FIELD fTipoCaja1
         END IF
         --Para numero de linea
         LET reg_detArr2[arr_curr()].idLinea = arr_curr()
         DISPLAY reg_detArr2[arr_curr()].idLInea TO sdet3[scr_line()].sidlinea1

      ON CHANGE scantcajas1
         LET reg_detArr2[arr_curr()].pesoBruto = NULL 
         LET reg_detArr2[arr_curr()].pesoTara = NULL 
         LET reg_detArr2[arr_curr()].pesoNeto = NULL 
         DISPLAY reg_detArr2[arr_curr()].pesoBruto TO sdet3[scr_line()].spesobruto1
         DISPLAY reg_detArr2[arr_curr()].pesoTara TO sdet3[scr_line()].spesotara1
         DISPLAY reg_detArr2[arr_curr()].pesoNeto TO sdet3[scr_line()].spesoneto1 
      
      ON ACTION leerPeso
        IF reg_detArr2[arr_curr()].cantCajas IS NULL THEN 
           CALL msg("Debe ingresar # de Cajas")
           NEXT FIELD scantcajas1
        ELSE 
           LET reg_detArr2[arr_curr()].pesoBruto = getPeso()
           CALL calculaPesos(2)
           
           LET preCantCajas = reg_detArr2[arr_curr()].cantCajas
           NEXT FIELD NEXT 
        END IF
        
      AFTER INSERT
         IF reg_detArr2[arr_curr()].cantCajas IS NULL 
            OR reg_detArr2[arr_curr()].pesoBruto IS NULL
            OR reg_detArr2[arr_curr()].pesoTara IS NULL
            OR reg_detArr2[arr_curr()].pesoTara IS NULL THEN
            CANCEL INSERT
         END IF
         --Revisa que la linea no se duplique 
         {IF repLine(arr_curr()) THEN 
            CALL msg("Línea repetida")
            NEXT FIELD sIdFinca
         END IF 

      AFTER INSERT
         IF d_gru[idx1] IS NULL THEN
            CANCEL INSERT
         END IF

      ON ACTION DELETE
         LET idx1 = arr_curr()
         CALL DIALOG.deleteRow("sdet1", idx1)}
      
   END INPUT 
      
   ON ACTION ACCEPT
        
        IF g_reg.tipoBoleta = 1 THEN
           IF g_reg.idOrdPro IS NULL THEN
              CALL msg("Debe ingresar orden de producción")
              NEXT FIELD fIdOrdPro
           END IF 
           IF g_reg.idDOrdPro IS NULL THEN
              CALL msg("Debe ingresar estructura")
              NEXT FIELD fIdDOrdPro
           END IF 
           IF g_reg.emplEntrego IS NULL THEN
              CALL msg("Debe seleccionar empleado que entrego")
              NEXT FIELD fEmplEntrego
           END IF
           IF g_reg.docPropio IS NULL THEN
              CALL msg("Debe ingresar numero de documento de entrega")
              NEXT FIELD fdocpropio
           END IF
           --inicio
           IF reg_detArr.getLength() > 0 THEN 
              --Verifica si el usuario cambio la cantidad de cajas cambio luego de leer el peso
              IF reg_detArr[arr_curr()].pesoNeto IS NOT NULL THEN 
                 IF preCantCajas <>  reg_detArr[arr_curr()].cantCajas THEN 
                    CALL msg("Cambio cantidad de cajas, debe leer nuevamente el peso")
                    CONTINUE DIALOG
                 END IF
              ELSE 
                 IF reg_detArr[reg_detArr.getLength()].pesoNeto IS NULL THEN
                    CALL DIALOG.deleteRow("sdet2", reg_detArr.getLength())
                 END IF 
              END IF 
           ELSE 
              CALL msg("Debe ingresar # de cajas y leer el peso")
              CONTINUE DIALOG
           END IF  
           IF g_reg.tipoCaja IS NULL THEN
              CALL msg("Debe ingresar tipo Caja")
              NEXT FIELD fTipoCaja
           END IF 
           IF arr_curr() = 1 THEN  
              IF reg_detArr[arr_curr()].cantCajas IS NULL 
                  OR reg_detArr[arr_curr()].pesoBruto IS NULL
                  OR reg_detArr[arr_curr()].pesoTara IS NULL
                  OR reg_detArr[arr_curr()].pesoTara IS NULL THEN
                  CALL msg("Debe ingresar # de cajas y leer el peso")
                  CONTINUE DIALOG
               ELSE 
                  CALL calculaPesos(1)
               END IF
           END IF
           --fin 
        ELSE 
           IF g_reg.idItem IS NULL THEN
              CALL msg("Debe ingresar Item")
              NEXT FIELD fIdItem
           END IF  
           IF g_reg.nomEntrego IS NULL THEN
              CALL msg("Debe ingresar nombre de quién entregó el producto")
              NEXT FIELD fNomEntrego
           END IF 
           --inicio
           IF reg_detArr2.getLength() > 0 THEN 
              --Verifica si el usuario cambio la cantidad de cajas cambio luego de leer el peso
              IF reg_detArr2[arr_curr()].pesoNeto IS NOT NULL THEN 
                 IF preCantCajas <> reg_detArr2[arr_curr()].cantCajas THEN 
                    CALL msg("Cambio cantidad de cajas, debe leer nuevamente el peso")
                    CONTINUE DIALOG
                 END IF
              ELSE 
                 IF reg_detArr2[reg_detArr2.getLength()].pesoNeto IS NULL THEN
                    CALL DIALOG.deleteRow("sdet3", reg_detArr2.getLength())
                 END IF 
              END IF 
           ELSE 
              CALL msg("Debe ingresar # de cajas y leer el peso")
              CONTINUE DIALOG
           END IF  
           IF g_reg.tipoCaja IS NULL THEN
              CALL msg("Debe ingresar tipo Caja")
              NEXT FIELD fTipoCaja1
           END IF 
           IF arr_curr() = 1 THEN  
              IF reg_detArr2[arr_curr()].cantCajas IS NULL 
                  OR reg_detArr2[arr_curr()].pesoBruto IS NULL
                  OR reg_detArr2[arr_curr()].pesoTara IS NULL
                  OR reg_detArr2[arr_curr()].pesoTara IS NULL THEN
                  CALL msg("Debe ingresar # de cajas y leer el peso")
                  CONTINUE DIALOG
               ELSE 
                  CALL calculaPesos(2)
               END IF
           END IF
           --fin 
        END IF 
        
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      CALL displayDetArr() 
   END IF 
   RETURN resultado 
END FUNCTION

FUNCTION cleanProductor()
  LET g_reg.productor = NULL 
  LET g_reg.idItem = NULL 
  LET g_reg.nomEntrego = NULL 
  LET g_reg.docProductor = NULL 
  DISPLAY g_reg.productor, g_reg.idItem, g_reg.nomEntrego, g_reg.docProductor
       TO fproductor, fiditem, fnomentrego, fdocproductor
  --LET esPropia = 1
  LET g_reg.tipoBoleta = 1
END FUNCTION 

FUNCTION cleanPropia()
  LET g_reg.idOrdPro = NULL 
  LET g_reg.idDOrdPro = NULL 
  LET g_reg.emplEntrego = NULL 
  LET g_reg.docPropio = NULL
  DISPLAY g_reg.idOrdPro, g_reg.idDOrdPro, g_reg.emplEntrego, g_reg.docPropio
       TO fidordpro, fiddordpro, femplentrego, fdocpropio
  --LET esPropia = 0
  LET g_reg.tipoBoleta = 2
END FUNCTION 

FUNCTION getPeso()
  DEFINE pesoManual SMALLINT
  DEFINE fn, fs, ft STRING 
  DEFINE cPeso STRING 
  DEFINE result SMALLINT, --,opc,activo   
         peso   DEC(9,2)

  LET pesoManual = getValParam("LEER PESO")       
  --LET pesoManual = 1

  IF pesomanual = 1 THEN 
     RETURN 500
  ELSE 
     -- Obteniendo peso de bascula
     LET fn = "C:\\\\Basculas\\\\SipLafarge.exe"
     LET fs = "C:\\\\SCALEWORKDIR\\\\pesos.prn"
     LET ft = "/tmp/file.txt"
     
     --LET fn = "bascula" --||w_mae_psj.numbas 
     CALL librut001_leerbascula(fn,fs,ft,"G")
     RETURNING result,peso
     DISPLAY "result ", result, " peso ", peso 
     IF NOT result THEN
        DISPLAY "entre if"
        RETURN -1  
     ELSE 
        DISPLAY "entre al else"
        RETURN peso
     END IF
     
  END IF  
END FUNCTION 

FUNCTION calculaPesos(fArray) --fArray qye arreglo esta pesando 1 local 2 productor
DEFINE pesoTarima, pesoCaja, pesoCajaT DECIMAL(5,2)
DEFINE idx, i, fArray SMALLINT 

  LET idx = arr_curr()
  --obtiene peso de tarima
  IF g_reg.tieneTarima THEN
     LET pesoTarima = getValParam("RECEPCION DE FRUTA - PESO DE TARIMA")
  ELSE 
     LET pesoTarima = 0.0
  END IF  
  
  --obtiene peso segun tipo de caja
  SELECT pesoCatEmpaque INTO pesoCaja
  FROM pemMcatEmp
  WHERE idCatEmpaque = g_reg.tipoCaja
  
  --Calcula Tara (multiplica cantCajas * pesoCaja) + pesoTarima
  IF fArray = 1 THEN 
     LET pesoCajaT =  reg_detArr[idx].cantCajas * pesoCaja

     LET reg_detArr[idx].pesoTara = pesoTarima + pesoCajaT
     DISPLAY "Tara lleva: ", reg_detArr[idx].pesoTara
  ELSE
     LET pesoCajaT =  reg_detArr2[idx].cantCajas * pesoCaja

     LET reg_detArr2[idx].pesoTara = pesoTarima + pesoCajaT
     DISPLAY "Tara lleva: ", reg_detArr2[idx].pesoTara
  END IF 
  
  --Para peso Neto
  IF fArray = 1 THEN 
     LET reg_detArr[idx].pesoNeto = reg_detArr[idx].pesoBruto -
                                    reg_detArr[idx].pesoTara
     DISPLAY reg_detArr[arr_curr()].pesoBruto,
             reg_detArr[arr_curr()].pesoTara,
             reg_detArr[arr_curr()].pesoNeto
          TO sdet2[scr_line()].sPesoBruto,
             sdet2[scr_line()].sPesoTara,
             sdet2[scr_line()].sPesoNeto
    DISPLAY "Peso Neto lleva: ", reg_detArr[idx].pesoNeto
  ELSE 
     LET reg_detArr2[idx].pesoNeto = reg_detArr2[idx].pesoBruto -
                                     reg_detArr2[idx].pesoTara
     DISPLAY reg_detArr2[arr_curr()].pesoBruto,
             reg_detArr2[arr_curr()].pesoTara,
             reg_detArr2[arr_curr()].pesoNeto
          TO sdet3[scr_line()].sPesoBruto1,
             sdet3[scr_line()].sPesoTara1,
             sdet3[scr_line()].sPesoNeto1
    DISPLAY "Peso Neto lleva: ", reg_detArr2[idx].pesoNeto
  END IF 
          
  --Para pesoTotal
  IF idx = 1 THEN 
     IF fArray = 1 THEN 
        LET g_reg.pesoTotal = reg_detArr[idx].pesoNeto
     ELSE 
        LET g_reg.pesoTotal = reg_detArr2[idx].pesoNeto
     END IF 
  ELSE 
     IF fArray = 1 THEN 
        LET g_reg.pesoTotal = 0.0
        FOR i = idx TO reg_detArr.getLength()
            LET g_reg.pesoTotal = g_reg.pesoTotal + reg_detArr[i].pesoNeto 
        END FOR
    ELSE 
       LET g_reg.pesoTotal = 0.0
        FOR i = idx TO reg_detArr2.getLength()
            LET g_reg.pesoTotal = g_reg.pesoTotal + reg_detArr2[i].pesoNeto 
        END FOR
    END IF 
  END IF  
  IF fArray = 1 THEN 
     DISPLAY g_reg.pesoTotal TO fPesoTotal
  ELSE
     DISPLAY g_reg.pesoTotal TO fPesoTotal1
  END IF 
  --retorna total
  --RETURN taraTotal
END FUNCTION 

FUNCTION cleanAll()
   INITIALIZE g_reg.* TO NULL
   CALL reg_detArr.clear()
END FUNCTION



