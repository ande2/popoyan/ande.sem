################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : liq_func()
#                
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "liq_glob.4gl"

FUNCTION ObtSemanaliq(cnt)
DEFINE cnt SMALLINT
    RETURN gr_reg.semanaliq
END FUNCTION


FUNCTION ObtSemanaRec(cnt)
DEFINE cnt SMALLINT
    RETURN gr_reg.semanaliq
END FUNCTION

FUNCTION ObtPresentacion(cnt)
DEFINE cnt SMALLINT
DEFINE vdescripcion VARCHAR(100,1)
DEFINE vlnkItem INTEGER
DEFINE vpeso DECIMAL(6,2)

    LET vdescripcion = NULL
    LET vlnkItem = NULL
    LET vpeso = NULL

    SELECT itemsxgrupo.nombrefac, itemsxgrupo.lnkItem, itemsxgrupo.pesofac
    INTO vdescripcion, vlnkItem, vpeso
    FROM itemsxgrupo
    WHERE itemsxgrupo.iditemSAPfac = gr_det2[cnt].producto2
    
    RETURN vdescripcion, vlnkItem, vpeso
END FUNCTION

FUNCTION ObtPrecioCliente(cnt)
DEFINE cnt SMALLINT
DEFINE vpreciocliente DECIMAL(18,2)

    LET vpreciocliente = NULL
    
    SELECT dsemanas.pre_defcli
    INTO vpreciocliente
    FROM dsemanas,msemanas
    WHERE dsemanas.sem_liq = gr_det2[cnt].semanaliq2
    AND dsemanas.lnkitem = gr_det2[cnt].lnkItem
    AND msemanas.lnksem = dsemanas.lnksem
    AND msemanas.idcliente = gr_reg.idcliente

    LET vpreciocliente = vpreciocliente * gr_det2[cnt].cajassap2
    
    RETURN vpreciocliente
END FUNCTION

FUNCTION ObtPrecioClienteSinDescuentoPorLibra(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioclientePorLibra DECIMAL(18,2)

    LET vprecioclientePorLibra = NULL

    IF gr_det2[cnt].cajasing2 > 0 AND gr_det2[cnt].peso > 0 THEN
        LET vprecioclientePorLibra = gr_det2[cnt].preciocliente2 / gr_det2[cnt].cajasing2 / gr_det2[cnt].peso
    END IF
    
    RETURN vprecioclientePorLibra
END FUNCTION

FUNCTION ObtDescuentoEmpaque(cnt)
DEFINE cnt,i SMALLINT
DEFINE vdescuento DECIMAL(18,2)

    LET vdescuento = NULL

    FOR i = 1 TO gr_det1.getLength()

        IF gr_det1[i].fac_num = gr_det2[cnt].factura2 THEN
            IF gr_det1[i].tot_facturas > 0 THEN
                LET vdescuento = ((gr_det1[i].des_emp / gr_det1[i].tot_facturas) * gr_det2[cnt].totalsap2)
            END IF
        END IF

    END FOR
    
    IF vdescuento IS NULL THEN
        LET vdescuento = 0
    END IF
    
    RETURN vdescuento
END FUNCTION

FUNCTION ObtDescuentoReclamo(cnt)
DEFINE cnt, i SMALLINT
DEFINE vdescuento DECIMAL(18,2)

    LET vdescuento = NULL

    FOR i = 1 TO gr_det1.getLength()

        IF gr_det1[i].fac_num = gr_det2[cnt].factura2 THEN
            IF gr_det1[i].tot_facturas > 0 THEN
                LET vdescuento = ((gr_det1[i].des_rec / gr_det1[i].tot_facturas) * gr_det2[cnt].totalsap2)
            END IF
        END IF

    END FOR
    
    IF vdescuento IS NULL THEN
        LET vdescuento = 0
    END IF
    
    RETURN vdescuento
END FUNCTION

FUNCTION ObtPrecioConDescuento(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioConDescuento DECIMAL(18,2)

    LET vprecioConDescuento = NULL

    LET vprecioConDescuento = gr_det2[cnt].totalsap2 - gr_det2[cnt].descuentoempaque2 - gr_det2[cnt].descuentoreclamo2
    
    RETURN vprecioConDescuento
END FUNCTION

FUNCTION ObtPrecioConDescuentoDeCalidadPorLibra(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioConDescuento DECIMAL(18,2)

    LET vprecioConDescuento = NULL

    IF gr_det2[cnt].cajassap2 > 0 AND gr_det2[cnt].peso > 0 THEN
        LET vprecioConDescuento = gr_det2[cnt].preciocondescuento2 / gr_det2[cnt].cajassap2 / gr_det2[cnt].peso
    END IF
    
    RETURN vprecioConDescuento
END FUNCTION

FUNCTION ObtDifEnPrecio(cnt)
DEFINE cnt SMALLINT
DEFINE vdiferencia DECIMAL(6,2)

    LET vdiferencia = NULL
    LET vdiferencia = gr_det2[cnt].preciosap2 - gr_det2[cnt].preciocliente2

    RETURN vdiferencia

END FUNCTION

FUNCTION ObtDifTotalEnPrecio(cnt)
DEFINE cnt SMALLINT
DEFINE vdiferencia DECIMAL(6,2)

    LET vdiferencia = NULL
    LET vdiferencia = gr_det2[cnt].difenprecio2 * gr_det2[cnt].cajassap2

    RETURN vdiferencia

END FUNCTION




FUNCTION ObtDescripcion(cnt)
DEFINE cnt SMALLINT
DEFINE vdescripcion VARCHAR(100,1)
DEFINE vlnkitem INTEGER

    LET vdescripcion = NULL

    SELECT itemsxgrupo.nombrefac, itemsxgrupo.lnkitem
    INTO vdescripcion, vlnkitem
    FROM itemsxgrupo
    WHERE itemsxgrupo.iditemSAPfac = gr_det2[cnt].producto2

    RETURN vdescripcion, vlnkitem
END FUNCTION
