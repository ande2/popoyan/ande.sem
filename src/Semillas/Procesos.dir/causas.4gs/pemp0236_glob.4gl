SCHEMA db0001  
GLOBALS
  DEFINE gr_reg RECORD
    idBoletaIng     LIKE pemmboletacau.idBoletaIng,
    idBoletaCau     LIKE pemmboletacau.idboletacau,
    fecha           LIKE pemmboleta.fecha,
    estructura      STRING,
    productor       LIKE pemmboleta.productor , 
    iditem          LIKE pemmboleta.iditem,
    pesorecepcion   SMALLINT ,
    pesoaprovexp    SMALLINT,
    pesoaprovloc    SMALLINT,
    pesomediano     SMALLINT ,
    pesodevolucion  SMALLINT ,
    pesomerma       SMALLINT ,
    fectran         LIKE pemMBoletacau.fectran,
    usuOpero        LIKE pemMBoletacau.usuOpero
  END RECORD 

  DEFINE gr_det DYNAMIC ARRAY OF RECORD
   idboletacau    LIKE pemdboletacau.idboletacau, 
   idcausa        LIKE pemdboletacau.idcausa,  
   porcentaje     LIKE pemdboletacau.porcentaje,
   obsevaciones   LIKE pemdboletacau.observaciones
  END RECORD 

  DEFINE gProcesa   SMALLINT --Para saber si ejecutara 1=Carreta, 2=Cenma o 3=Devolución
  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
  DEFINE cbCalidad ui.ComboBox
  DEFINE gMsg STRING 
  DEFINE gAccion CHAR (1)
  DEFINE sumaPesos DECIMAL (9,0) --LIKE pemmboleta.pesototal
END GLOBALS
