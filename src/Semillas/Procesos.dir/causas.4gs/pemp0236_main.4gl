################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "pemp0236_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

    DEFINE f ui.Form
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	--IF n_param = 0 THEN
		--RETURN
	--ELSE
        --LET vDbname = arg_val(2)
        --DISPLAY "vDbname ", vDbname
		--CONNECT TO vDbname
        CONNECT TO "db0001"
	--END IF
    LET gProcesa = arg_val(1)
	LET prog_name2 = "pemp0236.log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	--INITIALIZE u_reg.* TO NULL
	--LET g_reg.* = u_reg.*

    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")

    CLOSE WINDOW SCREEN
    OPEN WINDOW w1 WITH FORM "pemp0236_form1"

    CALL fgl_settitle("ANDE - Ingreso de Causas de Rechazo")
    LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
 
    DISPLAY "INGRESO DE CAUSAS DE RECHAZO" TO pdescripcion  

    --CALL insert_init()
    --CALL update_init()
    --CALL delete_init()
    CALL creatmp()
    CALL combo_init()
	CALL main_menu()

END FUNCTION 

FUNCTION main_menu()  

    MENU "Causas de Rechazo"
      BEFORE MENU 
         CALL DIALOG.setActionActive("modificar",   FALSE)
         
       ON ACTION agregar
         CALL gr_det.clear()
         CLEAR FORM 
         DISPLAY "INGRESO DE CAUSAS DE RECHAZO" TO pdescripcion
         DISPLAY "Ingreso" TO gtit_enc
         LET gAccion = "I"
         CALL finput("I")
         DISPLAY "" TO gtit_enc

      ON ACTION modificar
         --CALL gr_det.clear()
         --CLEAR FORM 
         DISPLAY "INGRESO DE CAUSAS DE RECHAZO" TO pdescripcion
         DISPLAY "Modificar" TO gtit_enc
         LET gAccion = "U"
         CALL finput("U")
         DISPLAY "" TO gtit_enc
         
      ON ACTION buscar
         CALL gr_det.clear()
         CLEAR FORM 
         DISPLAY "INGRESO DE CAUSAS DE RECHAZO" TO pdescripcion
         DISPLAY "Buscar" TO gtit_enc
         LET gAccion = "B"
         IF fbusca() THEN
            CALL DIALOG.setActionActive("modificar",   TRUE )
         ELSE   
            CALL DIALOG.setActionActive("modificar",   FALSE) 
         END IF 
         DISPLAY "" TO gtit_enc
       
      ON ACTION salir
         EXIT MENU  
         
    END MENU 

END FUNCTION

FUNCTION combo_init()
DEFINE sqlTxt STRING 

   LET sqlTxt = ' SELECT c.idcausa, c.nombre FROM pemmcausa c, pemmboleta b, glbmitems i ',
                ' WHERE c.idtipo = i.idtipo AND i.iditem = b.iditem AND b.idboletaing = ', gr_reg.idboletaing,
                ' UNION ',  
                ' SELECT c.idcausa, c.nombre FROM pemmcausa c, glbmitems i, pemdordpro d, pemmboleta b ',
                ' WHERE c.idtipo = i.idtipo AND i.iditem = d.iditem AND d.idordpro = b.idordpro ', 
                ' AND d.iddordpro = b.iddordpro AND b.idboletaing = ', gr_reg.idboletaing
   DISPLAY 'sqlTxt', sqlTxt             
   CALL combo_din2("idcausa", sqlTxt)
   
   {--Para tipo caja
   LET sql_stmt = "SELECT idCatEmpaque, nomCatEmpaque FROM pemMCatEmp WHERE IdTipEmpaque = ", 
                   getValParam("RECEPCION DE FRUTA - ID CATEGORIA CAJAS")
   CALL combo_din2("tipocaja",sql_stmt)

   --Combo de calidad
   LET cbCalidad = ui.ComboBox.forName("calidad")
   IF cbCalidad IS NULL THEN
      ERROR "Campo no existe en la forma"
      EXIT PROGRAM
   END IF
   CALL cbCalidad.clear()
   CASE 
     WHEN gProcesa = 1 --carreta
       CALL cbCalidad.addItem(1,"Aprovechamiento")
       CALL cbCalidad.addItem(2,"Mediano")
     WHEN gProcesa = 2 --cenma
       CALL cbCalidad.addItem(1,"Aprovechamiento")
       CALL cbCalidad.addItem(2,"Mediano")
     WHEN gProcesa = 3 --devolucion
       CALL cbCalidad.addItem(3,"Devolución")
   END CASE} 
   
 
 

END FUNCTION 