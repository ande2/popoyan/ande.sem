GLOBALS "cliq_glob.4gl"


FUNCTION fbusca()
DEFINE sql_query STRING


  CALL finput("B")
  LET cuantos = 0 
  LET sql_query = "SELECT COUNT (*) FROM pemmconsolidadoliq WHERE 1=1"

  IF gr_reg.idconliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND idconliq = ",gr_reg.idconliq USING "<<<<<<<"
  END IF
  
  IF gr_reg.anio IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND cliqanio = ",gr_reg.anio USING "<<<<"
  END IF
  IF gr_reg.semanaliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND cliqsemana = ",gr_reg.semanaliq USING "<<<<"
  END IF

  PREPARE count_query1 FROM sql_query
  EXECUTE count_query1 INTO cuantos
  IF cuantos > 0 THEN 
LET sql_query = "SELECT a.idconliq, a.cliqanio, a.cliqsemana ",
                " FROM pemmconsolidadoliq a WHERE 1=1"

  IF gr_reg.idconliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.idconliq = ",gr_reg.idconliq USING "<<<<<<<"
  END IF
  
  IF gr_reg.anio IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.cliqanio = ",gr_reg.anio USING "<<<<"
  END IF
  IF gr_reg.semanaliq IS NOT NULL THEN
    LET sql_query = sql_query CLIPPED,
    " AND a.cliqsemana = ",gr_reg.semanaliq USING "<<<<"
  END IF
  PREPARE datos_query2 FROM sql_query
     DECLARE curBusca SCROLL CURSOR FOR datos_query2
       
  ELSE 
     RETURN FALSE   
  END IF 
  OPEN curBusca
  FETCH FIRST curBusca INTO gr_reg.idconliq, gr_reg.anio, gr_reg.semanaliq
  DISPLAY BY NAME gr_reg.*
  CALL dispdet1()
  CALL dispdet2()
  
  
  RETURN TRUE 
  
END FUNCTION 

FUNCTION sigAnt(fcur)
DEFINE fcur SMALLINT 
  CALL cleanAllDet()
  FETCH RELATIVE fcur curBusca INTO gr_reg.idconliq, gr_reg.anio, gr_reg.semanaliq
  DISPLAY BY NAME gr_reg.*
  CALL dispdet1()
  CALL dispdet2()
 
END FUNCTION 

FUNCTION dispdet1()
DEFINE cnt SMALLINT
 

    DECLARE cur05 CURSOR FOR
    SELECT a.idliq, a.idcliente, b.nombre, a.liqsemliq,1
    FROM pemrelmconliq c, pemmliquidacion a, pemmcli b
    WHERE c.idconliq= gr_reg.idconliq
    AND a.idliq = c.idliq
    AND b.idcliente = a.idcliente
    ORDER BY 1
    
    LET cnt = 1
    FOREACH cur05 INTO gr_det1[cnt].*
    
    LET cnt = cnt+ 1
    END FOREACH

    DISPLAY ARRAY gr_det1 TO sDet1.* ATTRIBUTES(COUNT=gr_det1.getLength())
        BEFORE DISPLAY 
        EXIT DISPLAY 
    END DISPLAY
    
END FUNCTION


FUNCTION dispdet2()
DEFINE cnt SMALLINT
 

    DECLARE cur06 CURSOR FOR
    SELECT a.lnkitem,
    b.pesofac,
    a.cliqcodigosap,
    b.nombrefac,
    a.cliqtotalcajas,
    a.cliqprecio,
    a.cliqtotalvalor,
    a.cliqdesemp,
    a.cliqdescal,
    a.cliqcostoporcaja,
    (a.cliqcostoporcaja*a.cliqtotalcajas),
    a.cliqflete,
    a.cliqcosdirecto,
    a.cliqpordes,
    a.cliqpreciocajaprod,
    a.cliqpreciolibraprod
    
    FROM pemdconsolidadoliq a, OUTER(itemsxgrupo b)
    WHERE a.idliq = gr_reg.idconliq
    AND b.lnkitem = a.lnkitem

    LET cnt = 1
    FOREACH cur06 INTO gr_det2[cnt].*
              LET cnt = cnt+ 1
    END FOREACH

    DISPLAY ARRAY gr_det2 TO sDet2.* ATTRIBUTES(COUNT=gr_det2.getLength())
        BEFORE DISPLAY 
        EXIT DISPLAY 
    END DISPLAY
    
END FUNCTION
