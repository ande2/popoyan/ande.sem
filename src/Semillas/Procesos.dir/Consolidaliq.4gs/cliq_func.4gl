################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : liq_func()
#                
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "cliq_glob.4gl"


FUNCTION ObtPresentacion(cnt)
DEFINE cnt SMALLINT
DEFINE vdescripcion VARCHAR(100,1)
DEFINE vlnkItem INTEGER
DEFINE vpeso DECIMAL(6,2)
DEFINE vfletecaja DECIMAL(6,2)
DEFINE vcostodircaja DECIMAL(6,2)

    LET vdescripcion = NULL
    LET vlnkItem = NULL
    LET vpeso = NULL
    LET vfletecaja =NULL
    LET vcostodircaja = NULL

    SELECT itemsxgrupo.nombrefac, itemsxgrupo.lnkItem, itemsxgrupo.pesofac, 
    itemsxgrupo.fletecaja, itemsxgrupo.costodirecto
    INTO vdescripcion, vlnkItem, vpeso, vfletecaja, vcostodircaja
    FROM itemsxgrupo
    WHERE itemsxgrupo.iditemSAPfac = gr_det2[cnt].producto

    IF vfletecaja IS NULL THEN
        LET vfletecaja = 0
    END IF
    IF vcostodircaja IS NULL THEN
        LET vcostodircaja = 0
    END IF
    
    LET vfletecaja = vfletecaja * gr_det2[cnt].cajasing
    LET vcostodircaja = vcostodircaja * gr_det2[cnt].cajasing
    
    RETURN vdescripcion, vlnkItem, vpeso, vfletecaja, vcostodircaja
END FUNCTION

FUNCTION ObtPrecioing(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioing DECIMAL(18,2)

    LET vprecioing = NULL

    IF gr_det2[cnt].cajasing > 0 THEN
        LET vprecioing = gr_det2[cnt].totaling / gr_det2[cnt].cajasing
    END IF
    
    RETURN vprecioing
END FUNCTION

FUNCTION ObtPrecioConDescuento(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioConDescuento DECIMAL(18,2)

    LET vprecioConDescuento = NULL

    IF gr_det2[cnt].cajasing > 0 THEN
        LET vprecioConDescuento = gr_det2[cnt].totalcondescuento / gr_det2[cnt].cajasing
    END IF 
    
    RETURN vprecioConDescuento
END FUNCTION



FUNCTION ObtDescuento6porc(cnt)
DEFINE cnt,i SMALLINT
DEFINE vdescuento DECIMAL(18,2)
DEFINE vpordescuento DECIMAL(6,2)
 
    LET vdescuento = NULL
    SELECT glb_paramtrs.valchr
    INTO vpordescuento
    FROM glb_paramtrs
    WHERE glb_paramtrs.numpar = 15
    AND glb_paramtrs.tippar = 1
    AND glb_paramtrs.nompar MATCHES "LIQUIDACION - % DESCUENTO ITEM"

    LET vdescuento = gr_det2[cnt].totalcondescuento * vpordescuento
    
    IF vdescuento IS NULL THEN
        LET vdescuento = 0
    END IF
    
    RETURN vdescuento
END FUNCTION

FUNCTION ObtPrecioxCaja(cnt)
DEFINE cnt, i SMALLINT
DEFINE vprexcaja DECIMAL(18,2)

    LET vprexcaja = NULL

    LET vprexcaja = gr_det2[cnt].totalcondescuento - gr_det2[cnt].totalflete - gr_det2[cnt].totalcostodirecto - gr_det2[cnt].total6porc
    LET vprexcaja = vprexcaja / gr_det2[cnt].cajasing 
    
    IF vprexcaja IS NULL THEN
        LET vprexcaja = 0
    END IF
    
    RETURN vprexcaja
END FUNCTION

FUNCTION ObtPrecioxLibra(cnt)
DEFINE cnt, i SMALLINT
DEFINE vprexlibra DECIMAL(18,2)

    LET vprexlibra = NULL

    IF gr_det2[cnt].peso > 0 THEN
        LET vprexlibra = gr_det2[cnt].precioporcaja / gr_det2[cnt].peso
    END IF 
    
    IF vprexlibra IS NULL THEN
        LET vprexlibra = 0
    END IF
    
    RETURN vprexlibra
END FUNCTION







FUNCTION ObtDescripcion(cnt)
DEFINE cnt SMALLINT
DEFINE vdescripcion VARCHAR(100,1)
DEFINE vlnkitem INTEGER

    LET vdescripcion = NULL

    SELECT itemsxgrupo.nombrefac, itemsxgrupo.lnkitem
    INTO vdescripcion, vlnkitem
    FROM itemsxgrupo
    WHERE itemsxgrupo.iditemSAPfac = gr_det2[cnt].producto

    RETURN vdescripcion, vlnkitem
END FUNCTION
