SCHEMA db0001  
GLOBALS
  DEFINE gr_reg RECORD
    idconliq INTEGER,
    anio        SMALLINT,
    semanaliq   SMALLINT
    
  END RECORD
  

  DEFINE gr_det1 DYNAMIC ARRAY OF RECORD
    idliq     INTEGER,
    idcliente INTEGER,
    nombre     VARCHAR(100,1),
    idsemliq   SMALLINT,
    liq_sel     SMALLINT
  END RECORD 

  DEFINE gr_det2 DYNAMIC ARRAY OF RECORD
   lnkItem          INTEGER,
   peso             DECIMAL(6,2),
   producto        VARCHAR(50,1),
   presentacion    VARCHAR(50,1),
   cajasing          SMALLINT,
   precioing       DECIMAL(18,2),
   totaling           DECIMAL(18,2),
   descuentoempaque DECIMAL(18,2),
   descuentoreclamo DECIMAL(18,2),
   preciocondescuento DECIMAL(6,2),
   totalcondescuento DECIMAL(18,2),
   totalflete DECIMAL(18,2),
   totalcostodirecto DECIMAL(18,2),
   total6porc  DECIMAL(18,2),
   precioporcaja       DECIMAL(18,2),
   precioporlibra       DECIMAL(18,2)
  END RECORD 

  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
  DEFINE cuantos SMALLINT 
END GLOBALS
