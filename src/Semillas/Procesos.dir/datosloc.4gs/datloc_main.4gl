################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "datloc_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING

    DEFINE f ui.Form
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	--IF n_param = 0 THEN
		--RETURN
	--ELSE
        --LET vDbname = arg_val(2)
        --DISPLAY "vDbname ", vDbname
		--CONNECT TO vDbname
        CONNECT TO "db0001"
	--END IF
    LET gProcesa = arg_val(1)
	LET prog_name2 = "datosloc.log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	--INITIALIZE u_reg.* TO NULL
	--LET g_reg.* = u_reg.*

    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")

    CLOSE WINDOW SCREEN
    OPEN WINDOW w1 WITH FORM "datloc_form1"

    CALL fgl_settitle("ANDE - Boleta de Ingreso de Fruta Local")
    LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
    CASE 
      WHEN gProcesa = 1 --La Carreta 
        DISPLAY "INGRESO DE DATOS LA CARRETA" TO pdescripcion
      WHEN gProcesa = 2 --CENMA 
        DISPLAY "INGRESO DE DATOS CENMA" TO pdescripcion
      WHEN gProcesa = 3 --Devolución 
        DISPLAY "INGRESO DE DATOS DEVOLUCIÓN" TO pdescripcion  
    END CASE 
    --CALL insert_init()
    --CALL update_init()
    --CALL delete_init()
    CALL creatmp()
    CALL combo_init()
	CALL main_menu()

END FUNCTION 

FUNCTION main_menu() 
  DEFINE result STRING 
  DEFINE msgTxt STRING  
  DEFINE vEstado, lEstadoDevolucion INTEGER 

    MENU "Local"
      BEFORE MENU 
         CALL DIALOG.setActionActive("modificar",   FALSE)
         HIDE OPTION "cerrar" --CALL DIALOG.setActionActive("cerrar", FALSE )
         HIDE OPTION "abrir"
         IF gProcesa = 3 THEN 
            SHOW OPTION "cerrar" --CALL DIALOG.setActionActive("cerrar", TRUE ) 
            SHOW OPTION "abrir"
         END IF --Devolucion 
         
       ON ACTION agregar
 
         DISPLAY "Ingreso" TO gtit_enc
         CALL finput("I")
         DISPLAY "" TO gtit_enc

      ON ACTION modificar
         DISPLAY "Modificar" TO gtit_enc
         CALL finput("U")
         DISPLAY "" TO gtit_enc
         
      ON ACTION buscar
         CALL gr_det.clear()
         CLEAR FORM 
         CASE 
           WHEN gProcesa = 1 --La Carreta 
              DISPLAY "INGRESO DE DATOS LA CARRETA" TO pdescripcion
           WHEN gProcesa = 2 --CENMA 
              DISPLAY "INGRESO DE DATOS CENMA" TO pdescripcion
           WHEN gProcesa = 3 --Devolución 
              DISPLAY "INGRESO DE DATOS DEVOLUCIÓN" TO pdescripcion  
         END CASE 
         DISPLAY "Buscar" TO gtit_enc
         IF fbusca() THEN
            SELECT a.estado INTO vEstado FROM pemmboleta a, pemmboletaloc b WHERE b.idboletaloc = gr_reg.idBoletaLoc AND b.idboletaing = a.idboletaing
            IF vEstado = 0 THEN  
               CALL DIALOG.setActionActive("modificar",   FALSE) 
            ELSE    
               CALL DIALOG.setActionActive("modificar",   TRUE )
            END IF    
            --IF gProcesa = 3 THEN CALL DIALOG.setActionActive("cerrar", TRUE ) END IF --Devolucion 
         ELSE   
            CALL DIALOG.setActionActive("modificar",   FALSE) 
            --IF gProcesa = 3 THEN CALL DIALOG.setActionActive("cerrar", FALSE ) END IF --Devolucion 
         END IF 
         DISPLAY "" TO gtit_enc
         
      ON ACTION cerrar
         IF gr_reg.idBoletaIng > 0 THEN
            SELECT estadoDevolucion INTO lEstadoDevolucion FROM pemmboleta WHERE idboletaing = gr_reg.idBoletaIng
            IF lEstadoDevolucion = 1 THEN 
               CALL msg("Boleta ya esta cerrada")
            ELSE 
               LET result = librut001_yesornot("Cerrar Local","Esta seguro de que esta recepción no tiene devolución?","Si","No","copy2") 
               DISPLAY result
               IF result = 1 THEN
                  UPDATE pemmboleta SET estadoDevolucion = 1, estado = 3 WHERE idBoletaIng = gr_reg.idBoletaIng
                  IF sqlca.sqlcode = 0 THEN 
                     CALL msg ("Actualización exitosa") 
                  ELSE 
                     LET msgTxt = "El error ", sqlca.sqlcode, " ocurrio al tratar de actualizar" 
                     CALL msg(msgTxt) 
                  END IF 
               ELSE    
                  CALL msg("Proceso cancelado por el usuario") 
               END IF
            END IF 
         ELSE   
            CALL msg("Error, antes debe Buscar") 
         END IF

      ON ACTION abrir
        IF gr_reg.idBoletaIng > 0 THEN
            SELECT estadoDevolucion INTO lEstadoDevolucion FROM pemmboleta WHERE idboletaing = gr_reg.idBoletaIng
            IF lEstadoDevolucion <> 1 THEN 
               CALL msg("Boleta ya esta abierta")
            ELSE 
               LET result = librut001_yesornot("Abrir Local","Desea abrir nuevamente esta boleta?","Si","No","copy2") 
               DISPLAY result
               IF result = 1 THEN
                  UPDATE pemmboleta SET estadoDevolucion = NULL, estado = 2 WHERE idBoletaIng = gr_reg.idBoletaIng
                  IF sqlca.sqlcode = 0 THEN 
                     CALL msg ("Actualización exitosa") 
                  ELSE 
                     LET msgTxt = "El error ", sqlca.sqlcode, " ocurrio al tratar de actualizar" 
                     CALL msg(msgTxt) 
                  END IF 
               ELSE    
                  CALL msg("Proceso cancelado por el usuario") 
               END IF
            END IF 
         ELSE   
            CALL msg("Error, antes debe Buscar") 
         END IF
            
      ON ACTION salir
         EXIT MENU  
         
    END MENU 

END FUNCTION

FUNCTION combo_init()
DEFINE sql_stmt STRING 

   CALL combo_din2("cmbcliente", "SELECT idcliente, nombre FROM pemmcli WHERE tipo = 1 ")
   
   --Para tipo caja
   LET sql_stmt = "SELECT idCatEmpaque, nomCatEmpaque FROM pemMCatEmp WHERE IdTipEmpaque = ", 
                   getValParam("RECEPCION DE FRUTA - ID CATEGORIA CAJAS")
   CALL combo_din2("tipocaja",sql_stmt)

   --Combo de calidad
   LET cbCalidad = ui.ComboBox.forName("calidad")
   IF cbCalidad IS NULL THEN
      ERROR "Campo no existe en la forma"
      EXIT PROGRAM
   END IF
   CALL cbCalidad.clear()
   CASE 
     WHEN gProcesa = 1 --carreta
       CALL cbCalidad.addItem(1,"Aprovechamiento")
       CALL cbCalidad.addItem(2,"Mediano")
     WHEN gProcesa = 2 --cenma
       CALL cbCalidad.addItem(1,"Aprovechamiento")
       CALL cbCalidad.addItem(2,"Mediano")
     WHEN gProcesa = 3 --devolucion
       CALL cbCalidad.addItem(3,"Devolución")
   END CASE 
   
 
 

END FUNCTION 