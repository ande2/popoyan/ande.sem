SCHEMA db0001  
GLOBALS
  DEFINE gr_reg RECORD
    idBoletaIng     LIKE pemmboletaloc.idBoletaIng,
    idBoletaLoc     LIKE pemmboletaloc.idboletaloc,
    idCliente       LIKE pemmboletaloc.idcliente,
    pesoCli         LIKE pemmboletaloc.pesocli,
    tieneTarima     LIKE pemmboletaloc.tienetarima,
    tipoCaja        LIKE pemmboletaloc.tipocaja,
    fectran     LIKE pemMBoletaExp.fectran,
    usuOpero    LIKE pemMBoletaExp.usuOpero
  END RECORD 

  DEFINE gr_det DYNAMIC ARRAY OF RECORD
   idboletaloc    LIKE pemdboletaloc.idboletaloc, 
   idlinea        LIKE pemdboletaloc.idlinea,  
   lnkItem        LIKE pemdboletaloc.lnkItem,
   nombre         LIKE itemsxgrupo.nombre,
   cantCajas      LIKE pemdboletaloc.cantcajas,
   pesoBruto      LIKE pemdboletaloc.pesoBruto,
   pesoTara       LIKE pemdboletaloc.pesoTara,
   pesoNeto       LIKE pemdboletaloc.pesoNeto,
   calidad        LIKE pemdboletaloc.calidad
  END RECORD 

  DEFINE gProcesa   SMALLINT --Para saber si ejecutara 1=Carreta, 2=Cenma o 3=Devolución
  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
  DEFINE cbCalidad ui.ComboBox
  DEFINE gMsg, sqlTxt STRING 
END GLOBALS
