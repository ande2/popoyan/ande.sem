################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : liq_func()
#                
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "pem0251_glob.4gl"

FUNCTION ObtSemanaliq(cnt)
DEFINE cnt SMALLINT
    RETURN gr_reg.semanaliq
END FUNCTION

FUNCTION ObtFechaRec(cnt)
DEFINE cnt SMALLINT
    RETURN gr_reg.fecharec
END FUNCTION

FUNCTION ObtSemanaRec(cnt)
DEFINE cnt SMALLINT
    RETURN gr_reg.semanaliq
END FUNCTION

FUNCTION ObtPresentacion(cnt)
DEFINE cnt SMALLINT
DEFINE vdescripcion VARCHAR(100,1)
DEFINE vlnkItem INTEGER
DEFINE vpeso DECIMAL(6,2)

    LET vdescripcion = NULL
    LET vlnkItem = NULL
    LET vpeso = NULL

    SELECT itemsxgrupo.nombrefac, itemsxgrupo.lnkItem, itemsxgrupo.pesofac
    INTO vdescripcion, vlnkItem, vpeso
    FROM itemsxgrupo
    WHERE itemsxgrupo.iditemSAPfac = gr_det2[cnt].producto2
    
    RETURN vdescripcion, vlnkItem, vpeso
END FUNCTION

FUNCTION ObtPrecioCliente(cnt)
DEFINE cnt SMALLINT
DEFINE vpreciocliente DECIMAL(18,2)

    LET vpreciocliente = NULL
    
    SELECT dsemanas.pre_defcli
    INTO vpreciocliente
    FROM dsemanas,msemanas
    WHERE dsemanas.sem_liq = gr_det2[cnt].semanaliq2
    AND dsemanas.lnkitem = gr_det2[cnt].lnkItem
    AND msemanas.lnksem = dsemanas.lnksem
    AND msemanas.idcliente = gr_reg.idcliente

    LET vpreciocliente = vpreciocliente * gr_det2[cnt].cajas2
    
    RETURN vpreciocliente
END FUNCTION

FUNCTION ObtPrecioClienteSinDescuentoPorLibra(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioclientePorLibra DECIMAL(18,2)

    LET vprecioclientePorLibra = NULL

    IF gr_det2[cnt].cajas2 > 0 AND gr_det2[cnt].peso > 0 THEN
        LET vprecioclientePorLibra = gr_det2[cnt].preciocliente2 / gr_det2[cnt].cajas2 / gr_det2[cnt].peso
    END IF
    
    RETURN vprecioclientePorLibra
END FUNCTION

FUNCTION ObtDescuentoEmpaque(cnt)
DEFINE cnt SMALLINT
DEFINE vdescuento DECIMAL(18,2)

    LET vdescuento = NULL

    IF gr_reg.tot_facturas > 0 THEN
        LET vdescuento = ((gr_reg.des_emp / gr_reg.tot_facturas) * gr_det2[cnt].total2)
    END IF

    IF vdescuento IS NULL THEN
        LET vdescuento = 0
    END IF
    
    RETURN vdescuento
END FUNCTION

FUNCTION ObtDescuentoReclamo(cnt)
DEFINE cnt SMALLINT
DEFINE vdescuento DECIMAL(18,2)

    LET vdescuento = NULL

    IF gr_reg.tot_facturas > 0 THEN
        LET vdescuento = ((gr_reg.des_rec / gr_reg.tot_facturas) * gr_det2[cnt].total2)
    END IF

    IF vdescuento IS NULL THEN
        LET vdescuento = 0
    END IF
    
    RETURN vdescuento
END FUNCTION

FUNCTION ObtPrecioConDescuento(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioConDescuento DECIMAL(18,2)

    LET vprecioConDescuento = NULL

    LET vprecioConDescuento = gr_det2[cnt].total2 - gr_det2[cnt].descuentoempaque2 - gr_det2[cnt].descuentoreclamo2
    
    RETURN vprecioConDescuento
END FUNCTION

FUNCTION ObtPrecioConDescuentoDeCalidadPorLibra(cnt)
DEFINE cnt SMALLINT
DEFINE vprecioConDescuento DECIMAL(18,2)

    LET vprecioConDescuento = NULL

    IF gr_det2[cnt].cajas2 > 0 AND gr_det2[cnt].peso > 0 THEN
        LET vprecioConDescuento = gr_det2[cnt].preciocondescuento2 / gr_det2[cnt].cajas2 / gr_det2[cnt].peso
    END IF
    
    RETURN vprecioConDescuento
END FUNCTION

FUNCTION ObtDifEnPrecio(cnt)
DEFINE cnt SMALLINT
DEFINE vdiferencia DECIMAL(6,2)

    LET vdiferencia = NULL
    LET vdiferencia = gr_det2[cnt].preciosap2 - gr_det2[cnt].preciocliente2

    RETURN vdiferencia

END FUNCTION

FUNCTION ObtDifTotalEnPrecio(cnt)
DEFINE cnt SMALLINT
DEFINE vdiferencia DECIMAL(6,2)

    LET vdiferencia = NULL
    LET vdiferencia = gr_det2[cnt].difenprecio2 * gr_det2[cnt].cajas2

    RETURN vdiferencia

END FUNCTION

FUNCTION ObtNotaCNotaD(cnt)
DEFINE cnt SMALLINT
DEFINE vnotacnotad DECIMAL(6,2)

    LET vnotacnotad = NULL
    LET vnotacnotad = gr_det2[cnt].preciocondescuento2 - gr_det2[cnt].total2

    RETURN vnotacnotad

END FUNCTION

FUNCTION ObtFlete(cnt)
DEFINE cnt SMALLINT
DEFINE vflete DECIMAL(6,2)

    LET vflete = NULL

    SELECT pemmdesitem.desvalor
    INTO vflete
    FROM pemmdesitem
    WHERE pemmdesitem.iddescuento = 1 --Flete

    LET vflete = vflete * gr_det2[cnt].cajas2

    IF vflete IS NULL THEN
        LET vflete = 0
    END IF

    RETURN vflete

END FUNCTION

FUNCTION ObtCostoDirecto(cnt)
DEFINE cnt SMALLINT
DEFINE vcosto DECIMAL(6,2)

    LET vcosto = NULL

    --**** PENDIENTE DE DEFINIR DE DONDE SE TOMARA EL COSTO DIRECTO
    
    IF vcosto IS NULL THEN
        LET vcosto = 0
    END IF

    RETURN vcosto

END FUNCTION

FUNCTION ObtPorcentaje(cnt)
DEFINE cnt SMALLINT
DEFINE vporcentaje DECIMAL(6,2)

    LET vporcentaje = NULL

    --**** PENDIENTE DE DEFINIR DE DONDE SE TOMARA EL PORCENTAJE
    
    IF vporcentaje IS NULL THEN
        LET vporcentaje = 0
    END IF

    RETURN vporcentaje

END FUNCTION

FUNCTION ObtTotalDespuesDeDescuentos(cnt)
DEFINE cnt SMALLINT
DEFINE vtotal DECIMAL(6,2)

    LET vtotal = NULL

    LET vtotal = gr_det2[cnt].preciocondescuento2 - (gr_det2[cnt].flete2 + gr_det2[cnt].costodirecto2 + gr_det2[cnt].porcentaje2)

    RETURN vtotal

END FUNCTION

FUNCTION ObtPrecioCajaProductor(cnt)
DEFINE cnt SMALLINT
DEFINE vprecio DECIMAL(6,2)

    LET vprecio = NULL

    IF gr_det2[cnt].cajas2 > 0 THEN
        LET vprecio = gr_det2[cnt].totdespuesdescuentos2 / gr_det2[cnt].cajas2
    END IF

    RETURN vprecio

END FUNCTION

FUNCTION ObtPrecioLibraProductor(cnt)
DEFINE cnt SMALLINT
DEFINE vprecio DECIMAL(6,2)

    LET vprecio = NULL

    IF gr_det2[cnt].peso > 0 THEN
        LET vprecio = gr_det2[cnt].preciocajaproductor2 / gr_det2[cnt].peso
    END IF

    RETURN vprecio

END FUNCTION
