################################################################################
# Funcion     : %M%
# id_commdep  : Catalogo de Valvulas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idBoletaIng   LIKE pemMBoleta.idBoletaIng,
      fecha         LIKE pemMBoleta.fecha,
      esDirecto     LIKE pemMBoleta.esdirecto,
      tipoBoleta    LIKE pemMBoleta.tipoboleta,
      estado        LIKE pemMBoleta.estado,
      idOrdPro      LIKE pemMBoleta.idOrdPro,
      idDOrdPro     LIKE pemMBoleta.iddordpro,
      emplEntrego   LIKE pemMBoleta.emplEntrego,
      docPropioSerie LIKE pemmboleta.docpropioserie,
      docPropio     LIKE pemMBoleta.docpropio,
      productor     LIKE pemMBoleta.productor,
      idItem        LIKE pemMBoleta.idItem, 
      nomEntrego    LIKE pemMBoleta.nomEntrego,
      docProductor  LIKE pemMBoleta.docproductor,
      tipoCaja      LIKE pemMBoleta.tipoCaja,
      tieneTarima   LIKE pemMBoleta.tieneTarima,
      pesoTotal     LIKE pemMBoleta.pesoTotal,
      emplRecibio   LIKE pemMBoleta.emplRecibio,
      fecTran       LIKE pemMBoleta.fecTran
      
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet

TYPE 
   tDetArr RECORD
      idBoletaIng   LIKE pemDBoleta.idBoletaIng,
      idLinea       LIKE pemDBoleta.idLinea,
      cantCajas     LIKE pemDBoleta.cantCajas,
      pesoBruto     LIKE pemDBoleta.pesoBruto,
      pesoTara      LIKE pemDBoleta.pesoTara,
      pesoNeto      LIKE pemDBoleta.pesoNeto
   END RECORD 

DEFINE reg_detArr, reg_detArr2 DYNAMIC ARRAY OF tDetArr

DEFINE g_regArr, u_regArr tDetArr
   
DEFINE vDbname     STRING,   
       condicion   STRING --Condicion de la clausula Where 
CONSTANT    prog_name = "pemp0207"

DEFINE d_cajaEmp DYNAMIC ARRAY OF RECORD
  idCatEmpaque  LIKE    pemDCajEmp.idcatempaque,
  prestamo      LIKE    pemDCajEmp.prestamo,
  devolucion    LIKE    pemDCajEmp.devolucion
  
END RECORD 
END GLOBALS