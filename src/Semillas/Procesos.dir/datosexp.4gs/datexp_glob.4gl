SCHEMA db0001  
GLOBALS
  DEFINE gr_reg RECORD
    idBoletaIng     INTEGER,
    --cliente         INTEGER,
    idBoletaExp     LIKE pemMBoletaExp.idBoletaExp,
    estadoExportacion LIKE pemmboleta.estadoexportacion
    --idLinea     LIKE pemMBoletaExp.idLinea,
    --productorEstructura LIKE pemMBoletaExp.productorEstructura,
    --producto    LIKE pemMBoletaExp.producto,
    --cliente     LIKE pemMBoletaExp.cliente,
    --fectran     LIKE pemMBoletaExp.fectran,
    --usuOpero    LIKE pemMBoletaExp.usuOpero
  END RECORD

  DEFINE gr_cli DYNAMIC ARRAY OF RECORD
    idboletaexp LIKE pemcboletaexp.idboletaexp,
    idcliente   LIKE pemcboletaexp.idcliente,
    nombre      LIKE pemmcli.nombre,
    pesocli     DECIMAL(8,1) 
  END RECORD 

  DEFINE gr_det DYNAMIC ARRAY OF RECORD
   idboletaexp    LIKE pemdboletaexp.idboletaexp, 
   idcliente      LIKE pemdboletaexp.idcliente,
   lnkItem        LIKE pemdboletaexp.lnkItem,
   nombre         LIKE itemsxgrupo.nombre,
   peso           DECIMAL(8,1),
   cantcajas      LIKE pemdboletaexp.cantcajas,
   pesoTotal      LIKE pemdboletaexp.pesototal
   
  END RECORD 

  
  
  DEFINE resultado BOOLEAN
  DEFINE ltipo, i, j, idx, idx2, det2, insdet2 SMALLINT 
  DEFINE lidordpro, liddordpro, lproductor, litem, vpesototal INTEGER 
  DEFINE vEstructura, vProductor, vItem VARCHAR (100) 
  DEFINE vdescripcion, lcondi VARCHAR (100)
  DEFINE lPesoCaja, lPesoTotal, lSumPeso, lPesoSaldo, vpeso LIKE pemmboleta.pesototal
  DEFINE lEstadoExportacion LIKE pemmboleta.estadoexportacion
  DEFINE cuantos SMALLINT 
END GLOBALS
