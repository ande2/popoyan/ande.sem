################################################################################
# Funcion     : %M%
# nombre : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      tipo        CHAR (1), 
      idregistro  INTEGER ,
      nombre      STRING 
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "comm0103"

DEFINE gr_cajas DYNAMIC ARRAY OF RECORD 
   id_commemp  LIKE commemp.id_commemp,
   idItem       LIKE glbmitems.iditem,
   desItem      LIKE glbmitems.desitemlg
END RECORD 
END GLOBALS