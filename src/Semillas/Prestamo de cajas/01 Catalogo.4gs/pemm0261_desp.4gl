################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "catm0222_glob.4gl"


FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creación de la consulta
      CONSTRUCT condicion 
         ON id_commemp, codigo, nit, nombre, iniciales, direccion, telefono, espropia, estado, dbname
         FROM id_commemp, codigo, nit, nombre, iniciales, direccion, telefono, espropia, estado, dbname

         ON ACTION ACCEPT
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 
      FOR i = 1 TO condicion.getLength()
          IF condicion.subString(i,i+7) = "espropia" THEN 
             LET vcon = condicion.subString(i+9,i+9)
             IF vcon = 1 THEN 
                LET condicion = condicion.subString(i,i+8),
                                "'t'",
                                condicion.subString(i+10,condicion.getLength())
             ELSE
                LET condicion = condicion.subString(i,i+8),
                                "'f'",
                                condicion.subString(i+10,condicion.getLength())
             END IF 
         END IF 
      END FOR   
   ELSE
      --LET condicion = " 1=1 "
      LET condicion = " espropia='t' "
   END IF
   --Armar la consulta
   LET consulta = 
      "SELECT id_commemp, codigo, nit, nombre, iniciales, direccion, telefono, ",
      " espropia, estado, dbname ",
      " FROM commemp ",
      " WHERE id_commemp > 0 AND estado = 1 AND ", condicion,
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH

   
   RETURN x --Cantidad de registros
END FUNCTION
