DATABASE db0001
DEFINE VIdBoletaIng INTEGER 
MAIN
  MENU 
  ON ACTION cargar
      --Se llama en causas de rechazo, antes de actualizar el estado de la boleta
      PROMPT "Boleta " FOR VIdBoletaIng
      CALL GeneraInformacionDatosSap(VIdBoletaIng)
  ON ACTION enviar

  ON ACTION salir
    EXIT MENU 
  END MENU   
END MAIN 