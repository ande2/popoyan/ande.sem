GLOBALS "carga_glob.4gl"

FUNCTION GeneraInformacionDatosSap(VIdBoletaIng)
DEFINE VIdBoletaIng INTEGER
DEFINE reg_dbolexp RECORD LIKE pemdboletaexp.*
DEFINE reg_dbolloc RECORD LIKE pemdboletaloc.*
DEFINE vsem_cal SMALLINT
DEFINE vpesototalexp INTEGER
DEFINE vpesototalloc INTEGER

DEFINE reg_mdatosSAP RECORD
   idboletaing          INTEGER,     
   tipdoc               SMALLINT,    
   ipservidorbd         varchar(255),
   userservirdorbd      varchar(255),
   passuserservidorbd   varchar(255),
   ipservirdorsap       varchar(255),
   nombrebd             varchar(255),
   usuariosap           varchar(255),
   contrasenasap        varchar(255),
   numordpro            varchar(255),
   fechaconta           DATE,        
   serrecpro            varchar(255),
   sersalmer            varchar(255),
   serentmer            varchar(255),
   comentariosdoc       varchar(255),   
   codigosocioneg       varchar(255),    
   nombresocioneg       varchar(255),    
   numreciboprosap      varchar(255),    
   numsalidainvsap      varchar(255),    
   numentradainvsap     varchar(255),    
   numentmercexpsap     varchar(255),    
   numentmerclocsap     varchar(255),    
   estado               smallint        
END RECORD

DEFINE reg_ddatosSAP RECORD
   idboletaing          INTEGER,          
   numordpro            varchar(255),     
   tipoart              SMALLINT,         
   codart               varchar(255),     
   descart              varchar(255),     
   costoart             decimal(18,2),    
   cantart              SMALLINT,         
   codalmart            varchar(255),     
   cuentaafe            varchar(255)     
END RECORD

   -- Registro de Informacion

   -- ENCABEZADO DATOS SAP
   DISPLAY "BOLETA ID: ",VIdBoletaIng
   
   INITIALIZE reg_mdatosSAP.* TO NULL

   LET reg_mdatosSAP.idboletaing = VIdBoletaIng

   -- Datos fijos
   LET reg_mdatosSAP.ipservidorbd = getValParam("SAP IP SERVIDOR BD")
   --LET reg_mdatosSAP.ipservidorbd = '192.168.0.2'        
   LET reg_mdatosSAP.userservirdorbd = getValParam("SAP USER SERVIDOR BD")      
   --LET reg_mdatosSAP.userservirdorbd = 'sa'     
   LET reg_mdatosSAP.passuserservidorbd = getValParam("SAP PASS USER SERVIDOR BD") 
   --LET reg_mdatosSAP.passuserservidorbd = 'Kmaleon2' 
   LET reg_mdatosSAP.ipservirdorsap = getValParam("SAP IP SERVIRDOR SAP")      
   --LET reg_mdatosSAP.ipservirdorsap = '192.168.0.2'      
   LET reg_mdatosSAP.nombrebd = getValParam("SAP NOMBRE BD")            
   --LET reg_mdatosSAP.nombrebd = 'WPruebas Semillas'            
   LET reg_mdatosSAP.usuariosap = getValParam("SAP USUARIO SAP")         
   --LET reg_mdatosSAP.usuariosap = 'manager'         
   LET reg_mdatosSAP.contrasenasap = getValParam("SAP CONTRASENA SAP")
   --LET reg_mdatosSAP.contrasenasap = '0313231719'  

   -- 1 = FINCA, 2 = PRODUCTOR
   SELECT tb1.tipoboleta
     INTO reg_mdatosSAP.tipdoc
     FROM pemmboleta tb1
    WHERE tb1.idboletaing = VIdBoletaIng

   IF reg_mdatosSAP.tipdoc = 1 THEN -- FINCA
      -- Numero de Orden de Produccion
      SELECT tb1.numordpro
        INTO reg_mdatosSAP.numordpro
        FROM pemmordpro tb1, pemmboleta tb2
       WHERE tb1.idordpro = tb2.idordpro
         AND tb2.idboletaing = VIdBoletaIng

      -- Serie recibo de produccion, salida mercaderia, entrada mercaderia
      {SELECT tb1.serrecpro, tb1.sersalmer, tb1.serentmer
        INTO reg_mdatosSAP.serrecpro, reg_mdatosSAP.sersalmer, reg_mdatosSAP.serentmer
        FROM pemmordpro tb1, pemmboleta tb2
       WHERE tb1.idordpro = tb2.idordpro
         AND tb2.idboletaing = VIdBoletaIng  }
      LET reg_mdatosSAP.serrecpro = getValParam("SAP SERIE RECIBO PRODUCCION")
      LET reg_mdatosSAP.sersalmer = getValParam("SAP SERIE SALIDA ALMACEN")
      LET reg_mdatosSAP.serentmer = getValParam("SAP SERIE ENTRADA ALMACEN")   
   ELSE
       -- Codigo y Nombre del socio negocio
      SELECT UNIQUE tb1.codigo, tb1.nombre
        INTO reg_mdatosSAP.codigosocioneg, reg_mdatosSAP.nombresocioneg
        FROM commemp tb1, pemmboleta tb2
       WHERE tb1.id_commemp = tb2.productor
         AND tb2.idboletaing = VIdBoletaIng
       
   END IF 
   
   -- Fecha de Contabilizacion
   SELECT tb1.fecha
     INTO reg_mdatosSAP.fechaconta
     FROM pemmboleta tb1
    WHERE tb1.idboletaing = VIdBoletaIng
   {SELECT MAX(DATE(tb1.fectran))
     INTO reg_mdatosSAP.fechaconta
     FROM pemmboletaexp tb1
    WHERE tb1.idboletaing = VIdBoletaIng

   -- Si no existe en la boleta exportacion busca en la local
   IF reg_mdatosSAP.fechaconta IS NULL THEN
      SELECT MAX(DATE(tb1.fectran))
        INTO reg_mdatosSAP.fechaconta
        FROM pemmboletaloc tb1
       WHERE tb1.idboletaing = VIdBoletaIng
   END IF}

   -- Comentario para el tipo de documento
   SELECT tb1.sem_cal
     INTO vsem_cal
     FROM dsemanacal tb1
    WHERE reg_mdatosSAP.fechaconta >= tb1.fec_ini
      AND reg_mdatosSAP.fechaconta <= tb1.fec_fin  
    
   LET reg_mdatosSAP.comentariosdoc =  'REC-',VIdBoletaIng USING '<<<<<<<&','_SM_',vsem_cal USING '<<<<<<&'

   -- Inicializando datos que devolvera la interface
   LET reg_mdatosSAP.numreciboprosap = NULL    
   LET reg_mdatosSAP.numsalidainvsap = NULL
   LET reg_mdatosSAP.numentradainvsap = NULL    
   LET reg_mdatosSAP.numentmercexpsap = NULL    
   LET reg_mdatosSAP.numentmerclocsap = NULL

   --Para estado
   LET reg_mdatosSAP.estado = 0     

   --Elimina datos si ya existen
   DELETE FROM pemddatossap WHERE idboletaing = reg_mdatossap.idboletaing
   DELETE FROM pemmdatossap WHERE idboletaing = reg_mdatossap.idboletaing

   --Inserta en encabezado
   INSERT INTO pemmdatossap VALUES(reg_mdatosSAP.*)

   -- DETALLE DATOS SAP - Detalle de Items
   --====================DATOS PARA EL RECIBO Y SALIDA ================================ 
   -- Detalle FINCA Recibo y Salida
   IF reg_mdatosSAP.tipdoc = 1 THEN -- FINCA
      --Selecciona los datos del Item para el recibo de producción y la salida de mercancias
      LET reg_ddatossap.idboletaing = VIdBoletaIng
      LET reg_ddatossap.numordpro = reg_mdatosSAP.numordpro 
      LET reg_ddatossap.tipoart = 1 --Por ser finca es un solo item para recibo y salida

      -- Codigo Item SAP, nombre Item, cuenta contable, costo Std de Produccion
      SELECT tb2.iditemsap, tb2.nomordpro, tb2.cueafe, tb2.costostdprod
        INTO reg_ddatosSAP.codart, reg_ddatosSAP.descart, reg_ddatosSAP.cuentaafe,
             reg_ddatosSAP.costoart
        FROM pemmboleta tb1, pemmordpro tb2
       WHERE tb1.idboletaing = VIdBoletaIng
         AND tb2.idordpro = tb1.idordpro

      {-- Costo de Produccion
      SELECT tb2.costostdprod
        INTO reg_ddatossap.costoart
        FROM pemmboleta tb1, pemmordpro tb2
       WHERE tb1.idboletaing = VIdBoletaIng
         AND tb2.idordpro = tb1.idordpro}

      -- Cantidad item suma detalle de exportacion y local
      LET vpesototalexp = 0
      SELECT SUM(tb1.pesototal)
        INTO vpesototalexp
        FROM pemmboletaexp tb1
       WHERE tb1.idboletaing = VIdBoletaIng

      IF vpesototalexp IS NULL THEN
         LET vpesototalexp = 0
      END IF

      LET vpesototalloc = 0
      SELECT SUM(tb1.pesocli)
        INTO vpesototalloc
        FROM pemmboletaloc tb1
       WHERE tb1.idcliente <> 8 --descarta devolucion
         AND tb1.idboletaing = VIdBoletaIng

      IF vpesototalloc IS NULL THEN
         LET vpesototalloc = 0
      END IF
      
      LET reg_ddatossap.cantart = vpesototalexp + vpesototalloc

      {SELECT tb1.almart, tb1.cueafe
        INTO reg_ddatosSAP.codalmart, reg_ddatosSAP.cuentaafe 
        FROM pemmordpro tb1, pemmboleta tb2
       WHERE tb1.idordpro = tb2.idordpro
         AND tb2.idboletaing = VIdBoletaIng}

      --Almacen de recibo de producción
      LET reg_ddatosSAP.codalmart = getValParam("SAP ALMACEN RECIBO PRODUCCION")
      
      INSERT INTO pemddatossap VALUES(reg_ddatossap.*)
      
   END IF
   --========== FIN DATOS RECIBO Y SALIDA ======================================


   --========== PARA ENTRADA DE MERCANCIAS =====================================
   -- Detalle boleta Exportacion
   INITIALIZE reg_dbolexp.*, reg_ddatossap.* TO NULL
   DECLARE cursor_boleta_exp CURSOR FOR
      SELECT tb1.* 
        FROM pemdboletaexp tb1, pemmboletaexp tb2
       WHERE tb1.idboletaexp = tb2.idboletaexp
         AND tb2.idboletaing = VIdBoletaIng

   FOREACH cursor_boleta_exp INTO reg_dbolexp.*

      LET reg_ddatosSAP.idboletaing = VIdBoletaIng
      LET reg_ddatosSAP.numordpro = reg_mdatosSAP.numordpro
      
      IF reg_mdatosSAP.tipdoc = 1 THEN -- FINCA
         LET reg_ddatosSAP.tipoart = 0 -- Para cualquier tipo
      ELSE
         LET reg_ddatosSAP.tipoart = 1 -- Expotacion
      END IF

      -- Codigo SAP del articulo
      SELECT tb1.iditemsap, tb1.nombre
        INTO reg_ddatosSAP.codart, reg_ddatosSAP.descart
        FROM itemsxgrupo tb1
       WHERE tb1.lnkitem = reg_dbolexp.lnkitem

      SELECT MAX(tb1.cos_stdcom)
        INTO reg_ddatosSAP.costoart
        FROM dsemanas tb1, msemanas tb2
       WHERE tb2.idcliente = reg_dbolexp.idcliente
         AND tb2.fec_ini >= reg_mdatosSAP.fechaconta
         AND tb2.fec_fin <= reg_mdatosSAP.fechaconta
         AND tb1.lnksem = tb2.lnksem
         AND tb1.lnkitem = reg_dbolexp.lnkitem
           
      LET reg_ddatosSAP.cantart = reg_dbolexp.pesototal

      IF reg_mdatosSAP.tipdoc = 1 THEN -- FINCA
         SELECT tb1.almart
           --INTO reg_ddatosSAP.codalmart, reg_ddatosSAP.cuentaafe
           INTO reg_ddatosSAP.codalmart
           FROM pemmordpro tb1, pemmboleta tb2
          WHERE tb1.idordpro = tb2.idordpro
            AND tb2.idboletaing = VIdBoletaIng
      ELSE -- PRODUCTOR
         SELECT UNIQUE tb1.almart
           INTO reg_ddatosSAP.codalmart
           FROM commemp tb1, pemmboleta tb2
          WHERE tb1.id_commemp = tb2.productor
            AND tb2.idboletaing = VIdBoletaIng
      END IF

      INSERT INTO pemddatossap VALUES(reg_ddatossap.*)

      INITIALIZE reg_dbolexp.*, reg_ddatossap.* TO NULL

   END FOREACH
   CLOSE cursor_boleta_exp
   FREE cursor_boleta_exp

   INITIALIZE reg_dbolloc.* TO NULL
   INITIALIZE reg_ddatosSAP.* TO NULL
   
   -- Detalle boleta Local
   DECLARE cursor_boleta_loc CURSOR FOR
      SELECT tb1.* 
        FROM pemdboletaloc tb1, pemmboletaloc tb2
       WHERE tb1.idboletaloc = tb2.idboletaloc
         AND tb2.idCliente <> 8 --Descarta devolución
         AND tb2.idboletaing = VIdBoletaIng

   FOREACH cursor_boleta_loc INTO reg_dbolloc.*

      LET reg_ddatosSAP.idboletaing = VIdBoletaIng
      LET reg_ddatosSAP.numordpro = reg_mdatosSAP.numordpro
      LET reg_ddatosSAP.tipoart = 0 -- LOCAL

      -- Codigo SAP del articulo
      SELECT tb1.iditemsap, tb1.nombre
        INTO reg_ddatosSAP.codart, reg_ddatosSAP.descart
        FROM itemsxgrupo tb1
       WHERE tb1.lnkitem = reg_dbolloc.lnkitem

      SELECT MAX(tb1.cos_stdcom)
        INTO reg_ddatosSAP.costoart
        FROM dsemanas tb1, msemanas tb2, pemmboletaloc tb3
       WHERE tb3.idboletaing = VIdBoletaIng
         AND tb2.idcliente = tb3.idcliente
         AND tb2.fec_ini >= reg_mdatosSAP.fechaconta
         AND tb2.fec_fin <= reg_mdatosSAP.fechaconta
         AND tb1.lnksem = tb2.lnksem
         AND tb1.lnkitem = reg_dbolloc.lnkitem
      
      LET reg_ddatosSAP.cantart = reg_dbolloc.pesoneto

      IF reg_mdatosSAP.tipdoc = 1 THEN -- FINCA
         SELECT tb1.almart
           INTO reg_ddatosSAP.codalmart
           FROM pemmordpro tb1, pemmboleta tb2
          WHERE tb1.idordpro = tb2.idordpro
            AND tb2.idboletaing = VIdBoletaIng
      ELSE -- PRODUCTOR            
         SELECT UNIQUE tb1.almart
           INTO reg_ddatosSAP.codalmart
           FROM commemp tb1, pemmboleta tb2
          WHERE tb1.id_commemp = tb2.productor
            AND tb2.idboletaing = VIdBoletaIng
      END IF

      INSERT INTO pemddatossap VALUES(reg_ddatossap.*)

      INITIALIZE reg_dbolloc.*, reg_ddatossap.* TO NULL
      
   END FOREACH
   CLOSE cursor_boleta_loc
   FREE cursor_boleta_loc

END FUNCTION
