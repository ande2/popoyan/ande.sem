################################################################################
# Funcion     : %M%
# Descripcion : Modulo Interfaz ANDE JAVA SAP
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Luis de Leon  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS
    DEFINE 
       dbname      STRING,
        n_param 	SMALLINT,
        prog_name   STRING,
        prog_name2 	STRING,
        GFacturaMax INTEGER


    -- Registro de Encabezado datos SAP
    DEFINE reg_facturasSAP RECORD
         correlativo       SMALLINT,
         serie             VARCHAR(30,1),
         numero_factura    VARCHAR(30,1),
         fecha             VARCHAR(25,1),
         codigo_item       VARCHAR(30,1),
         cantidad          SMALLINT,
         unidad_medida     VARCHAR(30,1),
         precio            DECIMAL(18,2),
         descuento_linea   DECIMAL(18,2),
         total_factura     DECIMAL(18,2),
         descuento_documento DECIMAL(18,2)
   END RECORD

END GLOBALS

MAIN
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

   LET GFacturaMax = NULL
	LET n_param = num_args()

	IF n_param = 0 THEN
		RETURN
	ELSE
      LET dbname = arg_val(1)
		CONNECT TO dbname
      LET GFacturaMax = arg_val(2)
	END IF

   IF GFacturaMax IS NULL THEN
      DISPLAY "NO SE RECIBIO PARAMETRO DE FACTURA, PROCESO CANCELADO"
      RETURN
   END IF
   
   LET prog_name = "pemp0251"
	LET prog_name2 = prog_name||".log"   
    
	CALL STARTLOG(prog_name2)
	CALL main_initFAC()
END MAIN

FUNCTION main_initFAC()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE reg_facturasSAP.* TO NULL

    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles")

	LET nom_forma = prog_name CLIPPED, "_form"

   CALL CreaTablaTemporalFAC()
	CALL main_menuFAC()
    
END FUNCTION                                                                    

FUNCTION main_menuFAC()                               

   CALL FuncInterfazJavaSAPFAC()
   
END FUNCTION

FUNCTION FuncInterfazJavaSAPFAC()
DEFINE VCursorQuery STRING
DEFINE VCondicion1 STRING
DEFINE VStringCMD STRING
DEFINE VResCMD STRING
DEFINE VComandoJava VARCHAR(255,1)
DEFINE VTipoOpe SMALLINT

   -- Selecciona el comando JAVA con el PATH de ejecucion
   LET VComandoJava = NULL
   SELECT glb_paramtrs.valchr
     INTO VComandoJava
     FROM glb_paramtrs
    WHERE glb_paramtrs.numpar = 15
      AND glb_paramtrs.tippar = 1

   LET VComandoJava = '"C:\\\\Program Files (x86)\\\\Java\\\\jre7\\\\bin\\\\java" -jar C:\\\\tmp\\\\dist\\\\sc_sap_facturas.jar',
   ' 192.168.0.2 "WPruebas Semillas" sa Kmaleon2'

   DISPLAY "COMANDO JAVA: ",VComandoJava CLIPPED
   
   -- Arma el string con los datos a trasladar 
   LET VStringCMD = VComandoJava CLIPPED," ",GFacturaMax USING "<<<<<<<&"," ",'"|"' 
         
   CALL ui.interface.frontcall("standard","shellexec",[VStringCMD],[VResCMD])
   DISPLAY "Resultado ",VResCMD
   DISPLAY "String: ",VStringCMD

   SLEEP 5
   CALL LeeResultadoSAPFAC()

   BEGIN WORK
   
   COMMIT WORK
   
END FUNCTION


FUNCTION CreaTablaTemporalFAC()

   
      
END FUNCTION

FUNCTION LeeResultadoSAPFAC()
DEFINE PathWin , PathUnix STRING

   LET PathWin = "C:\\ANDE\\FILES\\facturas_sap.txt"
   LET PathUnix = FGL_GETENV("HOME")
   LET PathUnix = PathUnix CLIPPED,"/facturas_sap.txt"
   DISPLAY "PATHWIN: ",PathWin
   DISPLAY "PATHUNIX: ",PathUnix

   CALL librut001_getfile(PathWin,PathUnix)

   -- Genera la informacion a la tabla  
   LOAD FROM PathUnix 
   INSERT INTO tmpfacturas
   
   UNLOAD TO "facs.txt"
   SELECT * FROM tmpfacturas
   
END FUNCTION
