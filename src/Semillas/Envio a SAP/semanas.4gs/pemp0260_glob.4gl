################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Familias
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      lnksem1   INTEGER,
      idcliente1 INTEGER,
      nombre1   VARCHAR(50,1),
      ano1      SMALLINT,
      sem_cal1  SMALLINT ,
      rango_ini1 DATE ,
      rango_fin1 DATE ,
      periodoActivo1 SMALLINT ,
      fec_ini1   DATE,
      fec_fin1   DATE ,
      sem_com1  SMALLINT,
      sem_fac1  SMALLINT,
      sem_liq1  SMALLINT
      
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg2, g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "pemp0260"
END GLOBALS