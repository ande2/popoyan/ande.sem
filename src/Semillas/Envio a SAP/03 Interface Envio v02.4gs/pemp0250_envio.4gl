################################################################################
# Funcion     : %M%
# Descripcion : Modulo Interfaz ANDE JAVA SAP
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Luis de Leon  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS 'pemp0250_glob.4gl'

DEFINE 
    dbname      STRING,
    n_param 	SMALLINT,
    prog_name   STRING,
    prog_name2 	STRING
        

    -- Registro de resultados de retorno SAP
    DEFINE result RECORD
      numReciboPro      VARCHAR(255,1),
      numSalidaInv      VARCHAR(255,1) ,
      numEntradaInv     VARCHAR(255,1) ,
      numEntMerExp      VARCHAR(255,1) ,
      numEntMerLoc      VARCHAR(255,1) ,
      ctaConSalida      VARCHAR(255,1) ,
      ctaConEntrada     VARCHAR(255,1) 
    END RECORD 

    -- Registro de Encabezado datos SAP
    DEFINE reg_mdatosSAP RECORD
      idBoletaIng          INTEGER,
      tipDoc               SMALLINT,
      ipServidorBD         VARCHAR(255,1),
      userServirdorBD      VARCHAR(255,1),
      passuserServidorBD   VARCHAR(255,1),
      ipServirdorSAP       VARCHAR(255,1),
      nombreBD             VARCHAR(255,1),
      usuarioSAP           VARCHAR(255,1),
      contrasenaSAP        VARCHAR(255,1),
      numordpro            VARCHAR(255,1),
      fechaconta           DATE,
      serrecpro            VARCHAR(255,1),
      sersalmer            VARCHAR(255,1),
      serentmer            VARCHAR(255,1),
      serentpro            VARCHAR(255,1),
      almreciboprosap      VARCHAR (255,1),
      almtransferenciasap  VARCHAR (255,1),
      comentariosdoc       VARCHAR(255,1),
      codigosocioneg       VARCHAR(255,1),
      nombresocioneg       VARCHAR(255,1), 
      numReciboProSAP      VARCHAR(255,1),
      numSalidaInvSAP      VARCHAR(255,1),
      numEntradaInvSAP     VARCHAR(255,1),
      numEntMercExpSAP     VARCHAR(255,1),
      numEntMercLocSAP     VARCHAR(255,1),
      estado               SMALLINT ,
      estadofca            SMALLINT ,
      estadopro            SMALLINT ,    
      obs                  VARCHAR  (3000)     
    END RECORD

    -- Registro de Detalle datos SAP
    DEFINE reg_ddatosSAP RECORD
      idBoletaIng          INTEGER,
      numordpro            VARCHAR(255,1),
      tipoart              SMALLINT,
      codart               VARCHAR(255,1),
      descart              VARCHAR(255,1),
      costoart             DECIMAL(18,2),
      cantart              SMALLINT,
      codalmart            VARCHAR(255,1),
      cuentaafe            VARCHAR(255,1)
    END RECORD
    
--END GLOBALS

FUNCTION FuncInterfazJavaSAP(GIdBoletaIng)
DEFINE GIdBoletaIng INTEGER
DEFINE VCursorQuery STRING
DEFINE VCondicion1 STRING
DEFINE VStringCMD STRING
DEFINE VResCMD STRING
DEFINE VComandoJava VARCHAR(255,1)
DEFINE VTipoOpe SMALLINT
DEFINE vEstado SMALLINT
DEFINE vCuentaCon VARCHAR(255,1) 

   -- Selecciona el comando JAVA con el PATH de ejecucion
{   LET VComandoJava = NULL
   SELECT glb_paramtrs.valchr
     INTO VComandoJava
     FROM glb_paramtrs
    WHERE glb_paramtrs.numpar = 15
      AND glb_paramtrs.tippar = 1}

   --LET VComandoJava = '"C:\\\\Program Files (x86)\\\\Java\\\\jre7\\\\bin\\\\java" -jar C:\\\\temp\\\\dist\\\\sc_conector_sap.jar'
   --LET VComandoJava = '"C:\\\\Program Files (x86)\\\\Java\\\\jre7\\\\bin\\\\java" -jar C:\\\\ANDE\\\\sap\\\\sc_conector_sap.jar'

{   -- Selecciona el comando JAVA con el PATH de ejecucion
   LET VComandoJava = 'java -jar C:\\\\ANDE\\\\sap\\\\sc_conector_sap.jar'

   DISPLAY "COMANDO JAVA: ",VComandoJava CLIPPED}
   
   -- Cursor para el Encabezado de los Datos SAP
   LET VCursorQuery = "SELECT tb1.idboletaing, tb1.tipdoc, tb1.ipservidorbd, tb1.userservirdorbd, tb1.passuserservidorbd, ",
   " tb1.ipservirdorsap, tb1.nombrebd, tb1.usuariosap, tb1.contrasenasap, tb1.numordpro, tb1.fechaconta, ",
   " tb1.serrecpro, tb1.sersalmer, tb1.serentmer, tb1.serentpro, tb1.almreciboprosap, ",
   " tb1.almtransferenciasap, tb1.comentariosdoc, tb1.codigosocioneg, ",
   " tb1.nombresocioneg, tb1.numreciboprosap, tb1.numsalidainvsap, tb1.numentradainvsap, tb1.numentmercexpsap, ",
   " tb1.numentmerclocsap, tb1.estado, tb1.estadofca, tb1.estadopro, tb1.obs ",
                      "  FROM pemmDatosSap tb1 ",
                      " WHERE tb1.estado = 0 "
                      
   LET VCondicion1 = NULL
   IF GIdBoletaIng = 0 THEN
      LET VCondicion1 = NULL
   ELSE
      LET VCondicion1 = "AND tb1.IdBoletaIng = ",GIdBoletaIng
   END IF

   LET VCursorQuery = VCursorQuery," ",VCondicion1

   DISPLAY "QUERY: ",VCursorQuery CLIPPED
   
   LET VStringCMD = NULL

   --BEGIN WORK

   DECLARE CursorQuerySAP CURSOR FROM VCursorQuery
   FOREACH CursorQuerySAP INTO reg_mdatosSAP.idboletaing, reg_mdatosSAP.tipdoc, 
      reg_mdatosSAP.ipservidorbd, 
      reg_mdatosSAP.userservirdorbd, reg_mdatosSAP.passuserservidorbd,
      reg_mdatosSAP.ipservirdorsap, reg_mdatosSAP.nombrebd, reg_mdatosSAP.usuariosap, 
      reg_mdatosSAP.contrasenasap, reg_mdatosSAP.numordpro, reg_mdatosSAP.fechaconta,
      reg_mdatosSAP.serrecpro, reg_mdatosSAP.sersalmer, reg_mdatosSAP.serentmer, 
      reg_mdatosSAP.serentpro, reg_mdatosSAP.almreciboprosap, reg_mdatosSAP.almtransferenciasap,
      reg_mdatosSAP.comentariosdoc, reg_mdatosSAP.codigosocioneg,
      reg_mdatosSAP.nombresocioneg, reg_mdatosSAP.numreciboprosap, reg_mdatosSAP.numsalidainvsap, 
      reg_mdatosSAP.numentradainvsap, reg_mdatosSAP.numentmercexpsap, reg_mdatosSAP.numentmerclocsap, 
      reg_mdatosSAP.estado, reg_mdatosSAP.estadofca, reg_mdatosSAP.estadopro, reg_mdatosSAP.obs
   
      IF NOT validaEnvioEnc(reg_mdatosSAP.idboletaing) THEN RETURN END IF 
      
      IF reg_mdatosSAP.tipDoc = 2 THEN -- PRODUCTOR
         LET VTipoOpe = 3 -- Entradas de mercancía local y extranjero
      ELSE -- FINCA
         -- Selecciona el tipo de Operacion segun documentos
         CASE
            WHEN reg_mdatosSAP.numReciboProSAP IS NULL AND
                 reg_mdatosSAP.numSalidaInvSAP IS NULL  -- Ahora número de transferencia
                 --reg_mdatosSAP.numEntradaInvSAP IS NULL -- Ya no aplica 
                 LET VTipoOpe = 2 -- Proceso completo (recibo de producción y transferencia de mercadería)
                                  
            WHEN reg_mdatosSAP.numReciboProSAP IS NOT NULL AND
                 reg_mdatosSAP.numSalidaInvSAP IS NULL -- Ahora se refiere al numero de transferencia 
                 LET VTipoOpe = 4 -- Proceso parcial (solo transferencia de mercadería)

            OTHERWISE
               LET VTipoOpe = 0
         END CASE
      END IF

      -- Se empieza a armar el string con los datos a trasladar 

      -- Selecciona el comando JAVA con el PATH de ejecucion
      IF reg_mdatosSAP.tipDoc = 2 THEN -- PRODUCTOR
         LET VComandoJava = 'java -jar C:\\\\ANDE\\\\sap\\\\sc_conector_sap.jar'  --Entrada $'s y Entrada Q's
         LET VComandoJava = VComandoJava CLIPPED," ",VTipoOpe USING '<<<<<<&' 
      ELSE -- FINCA
         LET VComandoJava = 'java -jar C:\\\\ANDE\\\\sap\\\\sc_conector_sap2.jar' --Recibo y Transferencia
      END IF  
      DISPLAY "COMANDO JAVA: ",VComandoJava CLIPPED
      
      -- Datos de conexión
      LET VStringCMD = VComandoJava CLIPPED," ", --VTipoOpe USING '<<<<<<&'," ",
         reg_mdatosSAP.ipServidorBD CLIPPED," ",      
         reg_mdatosSAP.userServirdorBD CLIPPED," ",   
         reg_mdatosSAP.passuserServidorBD CLIPPED," ",
         reg_mdatosSAP.ipServirdorSAP CLIPPED," ",    
         '"',reg_mdatosSAP.nombreBD CLIPPED,'"'," ",          
         reg_mdatosSAP.usuarioSAP CLIPPED," ",        
         reg_mdatosSAP.contrasenaSAP CLIPPED," "  
         
      CASE 
         WHEN VTipoOpe = 2 OR VTipoOpe = 4
            LET VStringCMD = VStringCMD CLIPPED," ",
            reg_mdatosSAP.fechaconta USING 'DD-MM-YYYY' CLIPPED," ",
            reg_mdatosSAP.numordpro CLIPPED," ",         
            reg_mdatosSAP.serrecpro CLIPPED," ",         
            reg_mdatosSAP.sersalmer CLIPPED," ", 
            --FuncNumeroArticulosExp(reg_mdatosSAP.idBoletaIng) USING '<<<<<<&'," ",
            reg_mdatosSAP.comentariosdoc CLIPPED," ",
            reg_mdatosSAP.almreciboprosap CLIPPED," ", 
            reg_mdatosSAP.almtransferenciasap CLIPPED," "   
         WHEN VTipoOpe = 3
            SELECT FIRST 1 cuentaafe INTO vCuentaCon FROM pemddatossap WHERE idboletaing = reg_mdatosSAP.idBoletaIng
            LET VStringCMD = VStringCMD CLIPPED," ",
            reg_mdatosSAP.fechaconta USING 'DD-MM-YYYY' CLIPPED," ",
            --reg_mdatosSAP.serrecpro CLIPPED, " ",        
            reg_mdatosSAP.serentpro CLIPPED," ",
            reg_mdatosSAP.codigosocioneg CLIPPED," ",    
            '"',reg_mdatosSAP.nombresocioneg CLIPPED,'"'," ",    
            '"',reg_mdatosSAP.comentariosdoc CLIPPED,'"'," ",
            '"',reg_mdatosSAP.comentariosdoc CLIPPED,'"'," ",  
            FuncNumeroArticulosExp(reg_mdatosSAP.idBoletaIng) USING '<<<<<<&'," ",
            vCuentaCon CLIPPED," ",
            vCuentaCon CLIPPED," "
      END CASE

      -- Para el detalle de items      
      DECLARE CursorQueryDetSAP CURSOR FOR
         SELECT tb1.* 
           FROM pemdDatosSap tb1
          WHERE tb1.IdBoletaIng = reg_mdatosSAP.idBoletaIng
         ORDER BY tb1.tipoart --DESC -- 0 = LOCAL, 1 = EXPO

      FOREACH CursorQueryDetSAP INTO reg_ddatosSAP.*
         --IF NOT validaEnvioDet() THEN RETURN END IF
         CASE 
            WHEN VTipoOpe = 2 OR VTipoOpe = 4 
               IF reg_ddatosSAP.tipoart = 1 THEN -- EXPORTACION
                  LET VStringCMD = VStringCMD CLIPPED," ",
                     reg_ddatosSAP.codart CLIPPED," ",
                     '"',reg_ddatosSAP.descart CLIPPED,'"'," ",
                     reg_ddatosSAP.costoart USING '<<<<<<<<<<<<<<<&.&&'," ",
                     reg_ddatosSAP.cantart USING '<<<<<<&' {," ",
                     reg_ddatosSAP.codalmart CLIPPED," ",
                     reg_ddatosSAP.cuentaafe CLIPPED}
               ELSE -- LOCAL
                  LET VStringCMD = VStringCMD CLIPPED," ",
                     reg_ddatosSAP.codart CLIPPED," ",
                     '"',reg_ddatosSAP.descart CLIPPED,'"'," ",
                     reg_ddatosSAP.costoart USING '<<<<<<<<<<<<<<<&.&&'," ",
                     reg_ddatosSAP.cantart USING '<<<<<<&'," ",
                     reg_ddatosSAP.codalmart CLIPPED," ",
                     reg_ddatosSAP.cuentaafe CLIPPED
               END IF
            WHEN VTipoOpe = 3
               LET VStringCMD = VStringCMD CLIPPED," ",
                  reg_ddatosSAP.codart CLIPPED," ",
                  '"',reg_ddatosSAP.descart CLIPPED,'"'," ",
                  reg_ddatosSAP.costoart USING '<<<<<<<<<<<<<<<&.&&'," ",
                  reg_ddatosSAP.cantart USING '<<<<<<&'," ",
                  reg_ddatosSAP.codalmart CLIPPED," "
                  --reg_ddatosSAP.cuentaafe CLIPPED
         END CASE
         
      END FOREACH
      CLOSE CursorQueryDetSAP
      FREE CursorQueryDetSAP

      CALL ui.interface.frontcall("standard","shellexec",[VStringCMD],[VResCMD])
      DISPLAY "Resultado ",VResCMD
      DISPLAY "String: ",VStringCMD

      --actualizar pemmdatossap estadoenvio = en proceso

      --======= Fin envio datos ========================

      
   END FOREACH
   DISPLAY "Al salir del foreach"
   CLOSE CursorQuerySAP
   FREE CursorQuerySAP
   
   --COMMIT WORK

   --UNLOAD TO 'archivo.txt'
   --SELECT * FROM pemmDatosSap
   
END FUNCTION

FUNCTION leeResultadoEnvio(vidBol,ope)
DEFINE ope TINYINT --0=manual (despliega error si hay), 1= automatico (no despliega error)
DEFINE gr_reg RECORD LIKE pemmdatossap.*
DEFINE vIdBol LIKE pemmboleta.idboletaing
DEFINE VTipoOpe SMALLINT 
DEFINE VBandera SMALLINT

--DEFINE VContador, vTipDoc, vEstado, vEstadoFca, vEstadoPro SMALLINT

   --==========Leer resulados ========================
   LET gr_reg.idboletaing = vIdBol

   --1. Averigua si la boleta existe
   SELECT tipdoc, comentariosdoc, numReciboProSap, numSalidaInvSap, numEntradaInvSap, estado 
   INTO gr_reg.tipdoc, gr_reg.comentariosdoc, gr_reg.numreciboprosap,
   gr_reg.numsalidainvsap, gr_reg.numentradainvsap, gr_reg.estado
   FROM pemmdatossap 
   WHERE idboletaing = gr_reg.idboletaing

   IF sqlca.sqlcode = 100 THEN CALL msg("Error no existe boleta") RETURN END IF 

   --2. Averiguar en que estado esta la boleta, dejando VTipoOpe
     -- 2 = Recibo, Salida, Entrada
     -- 3 = Entrada Expo y Entrada Local
     -- 4 = Salida, Entrada
     -- 5 = Entrada

   IF gr_reg.tipdoc = 1 THEN --finca  
      IF gr_reg.numreciboprosap IS NULL OR gr_reg.numreciboprosap = 0 THEN
         LET vTipoOpe = 2 
      ELSE 
         IF gr_reg.numsalidainvsap IS NULL OR gr_reg.numsalidainvsap = 0 THEN
            LET vTipoOpe = 4  
         END IF   
      END IF
   ELSE --Productor
      LET vTipoOpe = 3     
   END IF 
   
   --3. Lee documentos
   CALL LeeResultadoSAP(VTipoOpe, gr_reg.comentariosdoc) RETURNING VBandera
   DISPLAY "VTipoOpe, gr_reg.comentariosdoc,VBandera ", VTipoOpe, gr_reg.comentariosdoc,VBandera
   IF ope = 0 THEN 
      IF VBandera = 0 THEN
         CALL msg("Error al leer numeros de documentos SAP")
         RETURN 
      END IF     
   END IF 

   --4. Actualizacion de pemmdatossap con numeros de Documentos
   CASE VTipoOpe
      WHEN 2
         UPDATE pemmDatosSap
            SET numReciboProSAP = result.numReciboPro,
                numSalidaInvSAP = result.numSalidaInv,
                numEntradaInvSAP = result.numEntradaInv
          WHERE pemmDatosSap.IdBoletaIng = gr_reg.idBoletaIng
      
      WHEN 3
         UPDATE pemmDatosSap
            SET numEntMercExpSAP = result.numEntMerExp,
                numEntMercLocSAP = result.numEntMerLoc
          WHERE pemmDatosSap.IdBoletaIng = gr_reg.idBoletaIng
      
      WHEN 4
         UPDATE pemmDatosSap
            SET numSalidaInvSAP = result.numSalidaInv,
                numEntradaInvSAP = result.numEntradaInv
          WHERE pemmDatosSap.IdBoletaIng = gr_reg.idBoletaIng
      
      WHEN 5
         UPDATE pemmDatosSap
            SET numEntradaInvSAP = result.numEntradaInv
          WHERE pemmDatosSap.IdBoletaIng = gr_reg.idBoletaIng
      
   END CASE
   {DISPLAY "Entre al foreach"
   END FOREACH
   DISPLAY "Al salir del foreach"
   CLOSE CursorQuerySAP
   FREE CursorQuerySAP

   COMMIT WORK}


{IF vTipDoc = 1 THEN --finca
   SELECT estadofca INTO vEstadoFca FROM pemmdatossap WHERE idboletaing = vIdBol
ELSE 
   SELECT estadoPro INTO vEstadoPro FROM pemmdatossap WHERE idboletaing = vIdBol
END IF }
--0. Averiguar comenariosdoc
 
  --IF sqlca.sqlcode = 100 THEN CALL msg("Error boleta no ha sido enviada") RETURN END IF 
      --SLEEP 30
      --LET VBandera = 0
      --LET VContador = 0
      --WHILE VBandera = FALSE
         --SLEEP 5
            --LET VContador = VContador + 1
            --DISPLAY "CONTADOR: ",VContador
            {IF VContador = 40 THEN
               EXIT WHILE
            END IF}
         {ELSE
            EXIT WHILE}
         --END IF
      --END WHILE
      

END FUNCTION 
{
FUNCTION CreaTablaTemporal()

   CREATE TEMP TABLE pemmDatosSap
   (
      idBoletaIng          INTEGER,
      tipDoc               SMALLINT,
      ipServidorBD         VARCHAR(255,1),
      userServirdorBD      VARCHAR(255,1),
      passuserServidorBD   VARCHAR(255,1),
      ipServirdorSAP       VARCHAR(255,1),
      nombreBD             VARCHAR(255,1),
      usuarioSAP           VARCHAR(255,1),
      contrasenaSAP        VARCHAR(255,1),
      numordpro            VARCHAR(255,1),
      fechaconta           DATE,
      serrecpro            VARCHAR(255,1),
      sersalmer            VARCHAR(255,1),
      serentmer            VARCHAR(255,1),
      comentariosdoc       VARCHAR(255,1),
      codigosocioneg       SMALLINT,
      nombresocioneg       VARCHAR(255,1),
      numReciboProSAP      VARCHAR(255,1),
      numSalidaInvSAP      VARCHAR(255,1),
      numEntradaInvSAP     VARCHAR(255,1),
      numEntMercExpSAP     VARCHAR(255,1),
      numEntMercLocSAP     VARCHAR(255,1),
      estado               SMALLINT
   ) WITH NO LOG

   INSERT INTO pemmDatosSap
   VALUES(1001, 2, "192.168.0.2", "sa", "Kmaleon2", "192.168.0.2", "WPruebas Semillas", "manager", "0313231719", "5453",
   "21-04-2014", "195", "196", "195","RC-T100",NULL,NULL,NULL,NULL,NULL,NULL,NULL,0)

   CREATE TEMP TABLE pemdDatosSap
   (
      idBoletaIng          INTEGER,
      numordpro            VARCHAR(255,1),
      tipoart              SMALLINT,
      codart               VARCHAR(255,1),
      descart              VARCHAR(255,1),
      costoart             DECIMAL(18,2),
      cantart              SMALLINT,
      codalmart            VARCHAR(255,1),
      cuentaafe            VARCHAR(255,1)
   ) WITH NO LOG

   INSERT INTO pemdDatosSap
   VALUES(1001, "5453", 1, "140.042.005", "CHILE BLOCKY (LIBRA) FINCAS CM", 2.22, 200, "SR-0400", "_SYS00000001152")  
   INSERT INTO pemdDatosSap
   VALUES(1001, "5453", 0, "305.240.003", "TOMATE CHERRY (LB) EXPORTACIÓN", 5.85, 100, "PH-0200",NULL)  
   
END FUNCTION
}

FUNCTION FuncNumeroArticulosExp(VIdBoletaIng)
DEFINE VIdBoletaIng INTEGER
DEFINE VCuentaReg SMALLINT

   LET VCuentaReg = 0
   
   SELECT COUNT(*)
     INTO VCuentaReg
     FROM pemdDatosSap
    WHERE pemdDatosSap.IdBoletaIng = VIdBoletaIng
      AND pemdDatosSap.tipoart = 2

   IF VCuentaReg IS NULL THEN
      LET VCuentaReg = 0
   END IF
   
   RETURN VCuentaReg

END FUNCTION

FUNCTION FuncNumeroArticulosLoc(VIdBoletaIng)
DEFINE VIdBoletaIng INTEGER
DEFINE VCuentaReg SMALLINT

   LET VCuentaReg = 0
   
   SELECT COUNT(*)
     INTO VCuentaReg
     FROM pemdDatosSap
    WHERE pemdDatosSap.IdBoletaIng = VIdBoletaIng
      AND pemdDatosSap.tipoart = 3

   IF VCuentaReg IS NULL THEN
      LET VCuentaReg = 0
   END IF
   
   RETURN VCuentaReg

END FUNCTION

FUNCTION LeeResultadoSAP(VTipoOpe, VComentBoleta)
DEFINE VTipoOpe SMALLINT
DEFINE VComentBoleta VARCHAR(255,1)
DEFINE PathWin , PathUnix STRING
DEFINE texto STRING
DEFINE accion DYNAMIC ARRAY OF CHAR(3)
DEFINE i INTEGER
DEFINE VBandera SMALLINT
DEFINE VStringCMD, VResCMD STRING

   DISPLAY "LEER RESULTADOS.........",VComentBoleta CLIPPED,"OPE: ",VTipoOpe
   
	CALL accion.clear()
   INITIALIZE result.* TO NULL
   LET texto = NULL
   LET VBandera = 0

	CASE VTipoOpe
		WHEN 2
			LET accion[1] = "-01"
			LET accion[2] = "-07"
			--LET accion[3] = "-03"
		WHEN 3 --Productores
         --hay items de exportación?
         IF FuncNumeroArticulosExp(g_reg.idboletaing) > 0 THEN 
            LET accion[4] = "-04"
         END IF
         --hay items de locales? 
			IF FuncNumeroArticulosLoc(g_reg.idboletaing) > 0 THEN
            LET accion[5] = "-05"
         END IF   
		WHEN 4 
            LET accion[2] = "-07" 
            --LET accion[3] = "-03"
		{WHEN 5
         LET accion[3] = "-03"}
		OTHERWISE
			LET accion = NULL
	END CASE

   DISPLAY "ACCION LENGHT: ",accion.getLength()
   
	FOR i = 1 TO accion.getLength()
		IF accion[i] IS NULL THEN
			CONTINUE FOR
		END IF
		LET PathWin = "C:\\ANDE\\FILES\\",VComentBoleta CLIPPED,accion[i] CLIPPED
		LET PathUnix = FGL_GETENV("HOME")
		LET PathUnix = PathUnix CLIPPED,"/",VComentBoleta CLIPPED,accion[i] CLIPPED
      DISPLAY "PATHWIN: ",PathWin
      DISPLAY "PATHUNIX: ",PathUnix

      WHENEVER ERROR CONTINUE
		CALL librut001_getfile(PathWin,PathUnix)
      WHENEVER ERROR STOP

      WHENEVER ERROR CONTINUE
		LET texto = librut001_readfile(PathUnix,1)
      WHENEVER ERROR STOP

		DISPLAY "texto: ",texto
      CASE 
         WHEN VTipoOpe = 2  OR VTipoOpe = 4 
            IF accion[i] = '-07' THEN
               IF LENGTH(texto) > 0 THEN
                  LET VBandera = 1
               END IF
            END IF
         WHEN VTipoOpe = 3
            IF accion[i] = '-04' THEN
               IF LENGTH(texto) > 0 THEN
                  LET VBandera = 1
               END IF
            END IF
            IF accion[i] = '-05' THEN
               IF LENGTH(texto) > 0 THEN
                  LET VBandera = 1
               END IF
            END IF
      END CASE
            
		CASE i
			WHEN 1
				LET result.numReciboPro = texto
			WHEN 2
				LET result.numSalidaInv = texto
			WHEN 3
				LET result.numEntradaInv = texto
			WHEN 4
				LET result.numEntMerExp = texto
			WHEN 5
				LET result.numEntMerLoc = texto
		END CASE
      
      --CALL box_valdato(texto||' '||i)
      
      --LET VStringCMD = 'del ',PathWin
      --CALL ui.interface.frontcall("standard","shellexec",[VStringCMD],[VResCMD])

	END FOR	

   DISPLAY 'VBandera: ',VBandera
   
   RETURN VBandera
   
END FUNCTION

FUNCTION validaEnvioEnc(lidboletaing)
DEFINE vObs VARCHAR (5000) 
DEFINE ok TINYINT 
DEFINE totItems, fexp, floc, idx1 SMALLINT 
DEFINE lidboletaing INTEGER 
DEFINE val_dsap DYNAMIC ARRAY OF RECORD
  idboletaing  LIKE pemddatossap.idboletaing,
  numordpro  LIKE pemddatossap.numordpro,
  tipoart  LIKE pemddatossap.tipoart,
  codart  LIKE pemddatossap.codart,
  descart  LIKE pemddatossap.descart,
  costoart  LIKE pemddatossap.costoart,
  cantart  LIKE pemddatossap.cantart,
  codalmart  LIKE pemddatossap.codalmart,
  cuentaafe  LIKE pemddatossap.cuentaafe
END RECORD 

DEFINE lreg_mdatosSAP RECORD
      idBoletaIng          INTEGER,
      tipDoc               SMALLINT,
      ipServidorBD         VARCHAR(255,1),
      userServirdorBD      VARCHAR(255,1),
      passuserServidorBD   VARCHAR(255,1),
      ipServirdorSAP       VARCHAR(255,1),
      nombreBD             VARCHAR(255,1),
      usuarioSAP           VARCHAR(255,1),
      contrasenaSAP        VARCHAR(255,1),
      numordpro            VARCHAR(255,1),
      fechaconta           DATE,
      serrecpro            VARCHAR(255,1),
      sersalmer            VARCHAR(255,1),
      serentmer            VARCHAR(255,1),
      serentpro            VARCHAR(255,1),
      comentariosdoc       VARCHAR(255,1),
      codigosocioneg       VARCHAR(255,1),
      nombresocioneg       VARCHAR(255,1), 
      numReciboProSAP      VARCHAR(255,1),
      numSalidaInvSAP      VARCHAR(255,1),
      numEntradaInvSAP     VARCHAR(255,1),
      numEntMercExpSAP     VARCHAR(255,1),
      numEntMercLocSAP     VARCHAR(255,1),
      estado               SMALLINT ,
      estadofca            SMALLINT ,
      estadopro            SMALLINT ,    
      obs                  VARCHAR  (3000)     
    END RECORD
    DEFINE fItemsLoc, fItemsExp TINYINT 


--DECLARE newcur CURSOR FOR 
  SELECT idBoletaIng,tipDoc,ipServidorBD,userServirdorBD,passuserServidorBD,
      ipServirdorSAP,nombreBD,usuarioSAP,contrasenaSAP,numordpro,fechaconta,
      serrecpro,sersalmer,serentmer,serentpro,comentariosdoc,codigosocioneg,
      nombresocioneg,numReciboProSAP,numSalidaInvSAP,numEntradaInvSAP,
      numEntMercExpSAP,numEntMercLocSAP,estado,estadofca,estadopro,obs
  INTO lreg_mdatosSAP.idBoletaIng,lreg_mdatosSAP.tipDoc,lreg_mdatosSAP.ipServidorBD,
  lreg_mdatosSAP.userServirdorBD,lreg_mdatosSAP.passuserServidorBD,
      lreg_mdatosSAP.ipServirdorSAP,lreg_mdatosSAP.nombreBD,lreg_mdatosSAP.usuarioSAP,
      lreg_mdatosSAP.contrasenaSAP,lreg_mdatosSAP.numordpro,lreg_mdatosSAP.fechaconta,
      lreg_mdatosSAP.serrecpro,lreg_mdatosSAP.sersalmer,lreg_mdatosSAP.serentmer,
      lreg_mdatosSAP.serentpro,lreg_mdatosSAP.comentariosdoc,lreg_mdatosSAP.codigosocioneg,
      lreg_mdatosSAP.nombresocioneg,lreg_mdatosSAP.numReciboProSAP,lreg_mdatosSAP.numSalidaInvSAP,
      lreg_mdatosSAP.numEntradaInvSAP,lreg_mdatosSAP.numEntMercExpSAP,lreg_mdatosSAP.numEntMercLocSAP,
      lreg_mdatosSAP.estado,lreg_mdatosSAP.estadofca,lreg_mdatosSAP.estadopro,lreg_mdatosSAP.obs    
  FROM pemmdatossap 
  WHERE idboletaing = lidboletaing  

   --Verifica si la boleta ya esta completa
   --Finca
   LET fItemsExp = 0
   LET fItemsLoc = 0
   
   IF lreg_mdatosSAP.tipdoc = 1 THEN --Finca
       
      IF lreg_mdatosSAP.numReciboProSAP > 0 AND lreg_mdatosSAP.numSalidaInvSAP > 0 AND lreg_mdatosSAP.numEntradaInvSAP > 0 THEN --ya esta   
         IF lreg_mdatosSAP.obs <> "Finalizada" THEN
            LET lreg_mdatosSAP.obs = "Finalizada" 
            UPDATE pemmdatossap SET obs = lreg_mdatosSAP.obs WHERE idboletaing = lreg_mdatosSAP.idBoletaIng
            RETURN FALSE 
         ELSE    
            RETURN FALSE   
         END IF
          
      END IF
   ELSE
      IF FuncNumeroArticulosExp(lreg_mdatosSAP.idboletaing) > 0 THEN    
         IF lreg_mdatosSAP.numEntMercExpSAP > 0 THEN
            LET fItemsExp = 1
         END IF
      ELSE  
         LET fItemsExp = 1  
      END IF
      IF FuncNumeroArticulosLoc(lreg_mdatosSAP.idboletaing) > 0 THEN    
         IF lreg_mdatosSAP.numEntMercLocSAP > 0 THEN
            LET fItemsLoc = 1
         END IF
      ELSE  
         LET fItemsLoc = 1  
      END IF
   END IF 
   IF fItemsExp = 1 AND fItemsLoc = 1 THEN 
      IF lreg_mdatosSAP.obs <> "Finalizada" THEN
            LET lreg_mdatosSAP.obs = "Finalizada" 
            UPDATE pemmdatossap SET obs = lreg_mdatosSAP.obs WHERE idboletaing = lreg_mdatosSAP.idBoletaIng
            RETURN FALSE 
         END IF
   END IF 

    
--Si aun no esta completa, valida info para enviarla

LET ok = 0 
LET vObs = "Error(es) en: "

--Validacion de parametros de conexión
IF lreg_mdatosSAP.ipServidorBD IS NULL 
   OR lreg_mdatosSAP.userServirdorBD IS NULL
   OR lreg_mdatosSAP.passuserServidorBD IS NULL 
   OR lreg_mdatosSAP.ipServirdorSAP IS NULL 
   OR lreg_mdatosSAP.nombreBD IS NULL
   OR lreg_mdatosSAP.usuarioSAP IS NULL
   OR lreg_mdatosSAP.contrasenaSAP IS NULL THEN 
   LET vObs = vObs, " - Parametros de conexión" 
   LET ok=1
END IF

--Validaciones Generales
IF lreg_mdatosSAP.fechaconta IS NULL THEN LET vObs = vObs, " - Fecha de contabilizacion" LET ok=1 END IF
IF lreg_mdatosSAP.comentariosdoc IS NULL THEN LET vObs = vObs, " - Comentario de documento" LET ok=1 END IF

--Validaciones especificas (finca/productor)
IF lreg_mdatosSAP.tipdoc IS NULL THEN LET vObs = vObs, " - Tipo de Documento" LET ok=1 END IF
--Validaciones para finca
IF lreg_mdatosSAP.tipdoc = 1 THEN --Finca
   IF lreg_mdatosSAP.numordpro IS NULL THEN LET vObs = vObs, " - Orden de Produccion" LET ok=1 END IF
   IF lreg_mdatosSAP.serrecpro IS NULL THEN LET vObs = vObs, " - Serie Recibo de Produccion" LET ok=1 END IF
   IF lreg_mdatosSAP.sersalmer IS NULL THEN LET vObs = vObs, " - Serie Salida de Mercancia" LET ok=1 END IF
   IF lreg_mdatosSAP.serentmer IS NULL THEN LET vObs = vObs, " - Serie Entrada de Mercancia" LET ok=1 END IF
   
--valiaciones para productores   
ELSE 
   IF lreg_mdatosSAP.serentpro IS NULL THEN LET vObs = vObs, " - Serie Entrada de Productores" LET ok=1 END IF 
   IF lreg_mdatosSAP.codigosocioneg IS NULL THEN LET vObs = vObs, " - Codigo socio de negocio" LET ok=1 END IF 
   IF lreg_mdatosSAP.nombresocioneg IS NULL THEN LET vObs = vObs, " - Nombre socio de negocio" LET ok=1 END IF 
END IF 

--Validaciones en detalle de productos
DECLARE dsapcur CURSOR FOR 
  SELECT idboletaing, numordpro, tipoart, codart, descart, costoart, cantart, codalmart, cuentaafe
    FROM pemddatossap 
   WHERE idboletaing = lreg_mdatosSAP.idBoletaIng

LET idx1 = 1
FOREACH dsapcur INTO val_dsap[idx1].idboletaing,val_dsap[idx1].numordpro,
  val_dsap[idx1].tipoart, val_dsap[idx1].codart, val_dsap[idx1].descart,
  val_dsap[idx1].costoart, val_dsap[idx1].cantart, val_dsap[idx1].codalmart,
  val_dsap[idx1].cuentaafe

  IF val_dsap[idx1].idboletaing IS NULL THEN LET vObs = vObs, " - ID Boleta detalle" LET ok=1 END IF
  IF val_dsap[idx1].tipoart IS NULL THEN LET vObs = vObs, " - Tipo de articulo" LET ok=1 END IF 

  IF lreg_mdatosSAP.tipDoc = 1 THEN --finca
     IF val_dsap[idx1].numordpro IS NULL THEN LET vObs = vObs, " - Numero de orden de produccion" LET ok=1 END IF
  END IF 
  
  IF val_dsap[idx1].codart IS NULL THEN LET vObs = vObs, " - Codigo de articulo" LET ok=1 END IF
  IF val_dsap[idx1].descart IS NULL THEN LET vObs = vObs, " - Descripcion del articulo" LET ok=1 END IF
  IF val_dsap[idx1].costoart IS NULL THEN LET vObs = vObs, " - Precios por semana" LET ok=1 END IF
  IF val_dsap[idx1].cantart IS NULL THEN LET vObs = vObs, " - Cantidad" LET ok=1 END IF
  IF val_dsap[idx1].codalmart IS NULL THEN LET vObs = vObs, " - Almacen" LET ok=1 END IF
  IF val_dsap[idx1].cuentaafe IS NULL THEN LET vObs = vObs, " - Cuenta contable" LET ok=1 END IF
  IF ok = 1 THEN EXIT FOREACH END IF  
  LET idx1 = idx1 + 1  
END FOREACH   

IF ok = 1 THEN  
  UPDATE pemmdatossap SET obs = vObs WHERE idboletaing = lreg_mdatosSAP.idBoletaIng    
  RETURN FALSE 
END IF 

   LET reg_mdatosSAP.obs = "Lista para enviar ..." 
   UPDATE pemmdatossap SET obs = reg_mdatosSAP.obs WHERE idboletaing = lreg_mdatosSAP.idBoletaIng
   RETURN TRUE  
END FUNCTION

{FUNCTION validaEnvioDet()
--reg_ddatosSAP
RETURN TRUE 
END FUNCTION} 
