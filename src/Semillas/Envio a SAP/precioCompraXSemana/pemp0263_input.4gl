################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "pemp0263_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form
DEFINE vdescripcion VARCHAR(50,1)
DEFINE msgTxt STRING 

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      INITIALIZE g_reg.* TO NULL
      --LET g_reg.lnksem1 = 0
      --LET g_reg.ano1 = YEAR(TODAY)
      --DISPLAY BY NAME g_reg.*
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT g_reg.idSem, g_reg.categoria, g_reg.tipo 
      FROM  cmbidsem, cmbcateg, cmbtipo
      
       --lnksem1, g_reg.idcliente1, g_reg.ano1, g_reg.rango_ini1, g_reg.rango_fin1, 
       --g_reg.sem_cal1, g_reg.fec_ini1, g_reg.fec_fin1, g_reg.sem_com1, g_reg.sem_fac1, 
       --g_reg.sem_liq1  
      ATTRIBUTES (WITHOUT DEFAULTS)

      {ON ACTION buscar
          LABEL opcionBusca:
          
          CALL picklist_2("Clientes", "Codigo","Descripción","idcliente", "nombre", "pemmcli", "1=1", "1", 0)
            RETURNING g_reg.idcliente1, vdescripcion, INT_FLAG
          --Que no sea nulo
           IF g_reg.idcliente1 IS NULL THEN
              CALL msg("Debe ingresar un cliente valido")
              NEXT FIELD idcliente1
            ELSE
                LET g_reg.nombre1=vdescripcion
                DISPLAY BY NAME g_reg.nombre1
           END IF}

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",TRUE)

      {BEFORE FIELD lnksem1
        NEXT FIELD NEXT}
  

      {ON CHANGE idcliente1
        SELECT nombre 
        INTO vdescripcion 
        FROM pemmcli 
        WHERE idcliente = g_reg.idcliente1
        IF sqlca.sqlcode = NOTFOUND THEN
           CALL msg("Cliente no existe")
           NEXT FIELD CURRENT
        ELSE
            LET g_reg.nombre1 = vdescripcion
            DISPLAY BY NAME g_reg.nombre1
        END IF }
          
      {AFTER FIELD nombre
        LET g_reg.nombre = g_reg.nombre CLIPPED  
        SELECT nombre FROM commemp
        WHERE nombre = g_reg.nombre 
        IF sqlca.sqlcode = 0 THEN
           CALL msg("Nombre de empresa ya existe")
           NEXT FIELD CURRENT 
        END IF }
        

      {AFTER FIELD dbname 
        IF g_reg.nombre IS NOT NULL THEN
           SELECT id_commemp FROM commemp WHERE commemp.dbname = g_reg.dbname
           IF sqlca.sqlcode = 0 THEN 
              CALL msg("Nombre de base de datos ya existe")
              NEXT FIELD CURRENT
           END IF 
        END IF}
 
 
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
      
   END INPUT 

   ON ACTION ACCEPT
      {LET msgTxt = "ID semana ", g_reg.idSem
      CALL msg(msgTxt)}
      CALL ingresaDetalle()
      IF int_flag THEN 
         LET resultado = FALSE 
      ELSE    
         LET resultado = TRUE
      END IF    
      EXIT DIALOG
        {IF g_reg.idcliente1 IS NULL THEN
           CALL msg("Debe ingresar cliente")
           NEXT FIELD idcliente1
        END IF  

        IF g_reg.ano1 IS NULL THEN
           CALL msg("Debe ingresar año")
           NEXT FIELD ano1
        END IF  

        IF g_reg.rango_ini1 IS NULL THEN
           CALL msg("Debe ingresar rango inicial de periodo")
           NEXT FIELD rango_ini1
        END IF 
        
        IF g_reg.rango_fin1 IS NULL THEN
           CALL msg("Debe ingresar rango final de periodo")
           NEXT FIELD rango_fin1
        END IF

        --IF g_reg.sem_cal1 IS NULL THEN
           --CALL msg("Debe ingresar numero de semana calendario al que corresponde este periodo")
           --NEXT FIELD sem_cal1
        --END IF 

        IF g_reg.fec_ini1 IS NULL THEN
           CALL msg("Debe ingresar fecha inicial")
           NEXT FIELD fec_ini1
        END IF

        IF g_reg.fec_fin1 IS NULL THEN
           CALL msg("Debe ingresar fecha inicial")
           NEXT FIELD fec_fin1
        END IF 
        
        IF g_reg.sem_com1 IS NULL THEN
           CALL msg("Debe ingresar una semana de compra")
           NEXT FIELD sem_com1
        END IF 

        IF g_reg.sem_fac1 IS NULL THEN
           CALL msg("Debe ingresar una semana de facturacion")
           NEXT FIELD sem_fac1
        END IF

        IF g_reg.sem_liq1 IS NULL THEN
           CALL msg("Debe ingresar una semana de liquidacion")
           NEXT FIELD sem_liq1
        END IF}

        

      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF


      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      {IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE }
   
     ON ACTION CANCEL
        LET resultado = FALSE 
        EXIT dialog
   END DIALOG
   
   {IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF }
   RETURN resultado 
   
END FUNCTION

FUNCTION ingresaDetalle()
  DEFINE idx,idx2 SMALLINT 
  DEFINE lflete LIKE itemsxgrupo.fletecaja
  DEFINE lcostodirecto LIKE itemsxgrupo.costodirecto
  DEFINE lporcentaje LIKE itemsxgrupo.porcentaje
  DEFINE lpesofac LIKE itemsxgrupo.pesofac
  DEFINE lsubtotal, valorIva, porcIva DECIMAL (6,4)
  
  INPUT ARRAY reg_det FROM sDet.* ATTRIBUTES (WITHOUT DEFAULTS, APPEND ROW = FALSE , DELETE ROW = FALSE , INSERT ROW = FALSE  )

    BEFORE INPUT 
      --CALL reg_det.clear()
      CALL llenaItems(g_reg.idSem, g_reg.categoria, g_reg.tipo)

    ON CHANGE precioBruto
      LET idx = arr_curr() 
      LET idx2 = scr_line()
      SELECT fleteCaja, costodirecto, porcentaje, pesoFac
        INTO lflete, lcostodirecto, lporcentaje, lpesofac
        FROM itemsxgrupo
       WHERE lnkItem = reg_det[idx].lnkItem

     --Descontando flete y costo directo
     IF g_reg.categoria = 2 THEN --local se resta el IVA
        LET porcIva = 1 + (getValParam("PORCENTAJE IVA") / 100) 
        LET valorIva = reg_det[idx].precioBruto / porcIva * getValParam("PORCENTAJE IVA") / 100
     ELSE    
        LET valorIva = 0
     END IF   
     
     LET lsubtotal = reg_det[idx].precioBruto - valorIva - (lflete + lcostodirecto)
     
     --para porcentaje 
     LET reg_det[idx].precioNeto = lsubtotal - ((lporcentaje/100)*reg_det[idx].precioBruto)

     --Para pesoLibra
     LET reg_det[idx].precioLibra = reg_det[idx].precioNeto / lpesofac
     
     DISPLAY reg_det[idx].precioNeto TO sDet[idx2].precioneto
     DISPLAY reg_det[idx].precioLibra TO sDet[idx2].preciolibra 
         
       {ON ACTION buscar
          LET idx2 = arr_curr()
          CALL picklist_2("Clientes", "ID Cliente","Nombre","idCliente", "nombre", "pemmcli", "tipo=1", "2", 0)
            RETURNING gr_cli[idx2].idcliente, gr_cli[idx2].nombre, INT_FLAG
            
       ON CHANGE idcliente 
         LET idx2 = arr_curr()
         
         --Cliente no repetido
         IF clirep(idx2) THEN
            CALL msg("Error: Cliente ya ingresado, ingrese de nuevo")
            NEXT FIELD idcliente 
         END IF 
         
         SELECT nombre INTO gr_cli[idx2].nombre FROM pemmcli WHERE idcliente = gr_cli[idx2].idcliente
         IF sqlca.sqlcode <> 0 THEN
            CALL msg("Error: ID cliente no exite, ingrese de nuevo")
            NEXT FIELD idcliente 
         END IF 
         
       AFTER ROW 
          LET idx2 = arr_curr()
        
          IF NOT cliExDet2(gr_cli[idx2].idcliente) AND gr_cli[idx2].nombre IS NOT NULL THEN  
             GOTO labelIS
          ELSE 
             CALL mostrarDet2(gr_cli[idx2].idcliente)
          END IF  
         
       BEFORE FIELD idcliente
         LET idx2 = arr_curr()
         IF idx2 > 1 THEN 
            IF (gr_cli[idx2].idcliente IS NULL ) AND --OR length(gr_cli[idx2].idcliente) < 1) AND
               (gr_cli[idx2-1].idcliente IS NULL) THEN -- OR length(gr_cli[idx2-1].idcliente)) THEN 
               CALL gr_cli.deleteElement(idx2)
            END IF 
         END IF 
         
       BEFORE ROW 
         LET idx2 = arr_curr()  
         IF gr_cli[idx2].idcliente IS NOT NULL THEN
            IF NOT cliExDet2(gr_cli[idx2].idcliente) THEN
               CALL cleandet2()  
               GOTO labelIS
            ELSE 
               CALL mostrarDet2(gr_cli[idx2].idcliente)
            END IF
         ELSE 
            CALL cleandet2()  
         END IF 
          
       --BEFORE INSERT 
         --LET idx2 = arr_curr() 
         --IF idx2 > 1 THEN 
            --IF gr_cli[idx-1].idcliente IS NULL THEN CANCEL INSERT END IF 
         --END IF 
         --
         
       ON CHANGE pesocli
         CALL calculaPesoSaldo()}
         
     END INPUT 

END FUNCTION 

FUNCTION llenaItems(lidSem, lcategoria, ltipo)
DEFINE lIdSem, lCategoria, lTipo INTEGER 
DEFINE x SMALLINT 

  DECLARE curDet CURSOR FOR --definir cursor con consulta de BD
    SELECT i.lnkitem, i.iditemsapfac, i.nombrefac 
      FROM itemsxgrupo i, glbmitems g, glbmtip m
      WHERE i.iditem  = g.iditem 
      AND g.idfamilia = m.idfamilia
      AND g.idtipo    = m.idtipo
      AND i.nombrefac IS NOT NULL 
      AND categoria   = lCategoria --Exportacion
      AND g.idtipo    = lTipo --Cherry

  CALL reg_det.clear()
  LET x = 0
  --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.lnkItem, rDet.idItemSapFac, rDet.nombrefac
      LET rDet.idSem = lidSem 
      SELECT precioBruto, precioNeto, precioLibra
      INTO rDet.precioBruto, rDet.precioNeto, rDet.precioLibra
      FROM precioxsem
      WHERE idSem = lidSem
      AND lnkItem = rDet.lnkItem
      LET x = x + 1 
      LET reg_det[x].* = rDet.*  
   END FOREACH
   --RETURN x --Cantidad de registros      
      
END FUNCTION 
