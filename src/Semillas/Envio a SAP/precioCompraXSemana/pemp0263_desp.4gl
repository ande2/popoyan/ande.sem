################################################################################
# Funcion     : %M%
# param_id : Funcion para consulta de umdores  
# Funciones   : umd_qry_init()    -> Prepara la seleccion
#               query_umd()       -> Ingreso de criterios de busqueda
#               fetch_umd()       -> Navega sobre seleccion de datos
#               clean_up()        -> Limpia el cursor de trabajo
#               display_umd()     -> Despliega el registro en pantalla
#               inicia_null()     -> inicia param_ides a nulos
#               display_estado()  -> Despliega el estado del umdor
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        param_id de la modificacion
#
################################################################################

GLOBALS "pemp0263_glob.4gl"


FUNCTION consulta(flagConsulta)
DEFINE flagConsulta BOOLEAN 
DEFINE 
   rDet tDet,
   x,i,vcon INTEGER,
   consulta STRING  

   IF flagConsulta THEN 
      CALL encabezado("Consulta")
      --CALL info_usuario()
      --Creación de la consulta
      CONSTRUCT condicion 
         ON idSem, categoria, tipo
         FROM cmbidsem, cmbcateg, cmbtipo 

         {AFTER  cmbidsem
           LET g_reg.idSem = GET_FLDBUF(cmbidsem)
           DISPLAY "Semana ", g_reg.idSem}

         ON ACTION ACCEPT
            LET g_reg.idSem = GET_FLDBUF(cmbidsem)
            IF g_reg.idSem IS NULL THEN CALL msg("Seleccione semana") NEXT FIELD cmbidsem END IF 
            LET g_reg.categoria = GET_FLDBUF(cmbcateg)
            IF g_reg.categoria IS NULL THEN CALL msg("Seleccione categoria") NEXT FIELD cmbcateg END IF
            LET g_reg.tipo = GET_FLDBUF(cmbtipo)
            IF g_reg.tipo IS NULL THEN CALL msg("Seleccione tipo") NEXT FIELD cmbtipo END IF
            EXIT CONSTRUCT 
         ON ACTION CANCEL 
            CALL reg_det.clear()
            RETURN 0
             
      END CONSTRUCT 
      
   ELSE
      LET condicion = " 1=1 "
   END IF
   CALL muestraItems(g_reg.idSem, g_reg.categoria, g_reg.tipo)
   --Armar la consulta
   {LET consulta = 
      "SELECT idSem, lnkItem, precioBruto, precioNeto, precioLibra  ",
      " FROM precioxsem ",
      " WHERE ", condicion,
      " AND idSem = ", g_reg.idSem,
      " ORDER BY 1"
   
   --definir cursor con consulta de BD
   DECLARE curDet CURSOR FROM consulta
   CALL reg_det.clear()
   LET x = 0
   --Llenar el arreglo con el resultado de la consulta
   FOREACH curDet INTO rDet.*
      LET x = x + 1 
      LET reg_det [x].* = rDet.*  
   END FOREACH}
   RETURN x --Cantidad de registros
END FUNCTION

FUNCTION muestraItems(lsem,lcateg,ltipo)
 DEFINE lsem, lcateg, ltipo, x INTEGER 

 DISPLAY "entre muestraItems con la semana ", lsem
 DECLARE muetraItemsCursor CURSOR FOR 
 SELECT idSem, lnkItem, idCateg, idTipo, precioBruto, precioNeto, precioLibra
   FROM precioxsem
  WHERE idSem = lsem
    AND idCateg = lcateg
    AND idTipo  = ltipo

  CALL reg_det.clear()
  LET x = 0
  --Llenar el arreglo con el resultado de la consulta
   FOREACH muetraItemsCursor INTO rDet.idSem, rDet.lnkItem, --rDet.idItemSapFac, rDet.nombrefac
      rDet.idCateg, rDet.idTipo, rDet.precioBruto, rDet.precioNeto, rDet.precioLibra
      --LET rDet.idSem = lidSem 
      SELECT i.iditemsapfac, i.nombrefac 
      INTO rDet.idItemSapFac, rDet.nombrefac
      FROM itemsxgrupo i
      WHERE i.lnkitem = rDet.lnkItem
      LET x = x + 1 
      LET reg_det[x].* = rDet.*  
      DISPLAY ARRAY reg_det TO sDet.*
         BEFORE DISPLAY  EXIT DISPLAY 
      END DISPLAY 
   END FOREACH
   --RETURN x --Cantidad de registros   
  
END FUNCTION 
