################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un registro nuevo 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "glbm0009_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO glbMItems ( ",
        " iditem, idFamilia, idTipo, desItemCt, desItemMd, desItemLg, ",
        " variedad, calidad, mercado ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
  DEFINE i SMALLINT 
   LET g_reg.lidItem = 0

   TRY 
      EXECUTE st_insertar USING 
            g_reg.lidItem, g_reg.lidFamilia, g_reg.lidtipo, g_reg.desItemCt, 
            g_reg.desItemMd, g_reg.ldesItemLg, g_reg.variedad, g_reg.calidad,
            g_reg.mercado

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.lidItem = SQLCA.sqlerrd[2]
      TRY 
        FOR i = 1 TO gr_itemsExp.getLength()
           IF gr_itemsExp[i].idItemSAP IS NULL AND gr_itemsExp[i].nombre IS NULL THEN 
              EXIT FOR 
           END IF
           SELECT MAX (lnkitem) + 1 INTO gr_itemsExp[i].lnkItem FROM itemsxgrupo  
           INSERT INTO itemsxgrupo(lnkItem, idItem, idItemSAP, categoria, nombre, 
                       peso, iditemfac, iditemSAPfac, nombrefac, pesofac, 
                       fleteCaja, costoDirecto, porcentaje) 
               VALUES (gr_itemsExp[i].lnkItem,g_reg.lidItem, gr_itemsExp[i].idItemSAP, 1, 
                       gr_itemsExp[i].nombre, gr_itemsExp[i].peso, 
                       g_reg.lidItem, --gr_itemsExp[i].idItemfac, 
                       gr_itemsExp[i].idItemSAP,
                       gr_itemsExp[i].nombrefac, gr_itemsExp[i].pesofac,
                       gr_itemsExp[i].fleteCaja, gr_itemsExp[i].costoDirecto,
                       gr_itemsExp[i].porcentaje)
        END FOR 
        TRY 
          
          FOR i = 1 TO gr_itemsLoc.getLength()
             IF gr_itemsLoc[i].idItemSAP IS NULL AND gr_itemsLoc[i].nombre IS NULL THEN 
                EXIT FOR 
             END IF 
             SELECT MAX (lnkitem) + 1 INTO gr_itemsLoc[i].lnkItem FROM itemsxgrupo
             INSERT INTO itemsxgrupo(lnkItem, idItem, idItemSAP, categoria, nombre, 
                         peso, iditemfac, iditemSAPfac, nombrefac, pesofac,
                         fleteCaja, costoDirecto, porcentaje) 
                 VALUES (gr_itemsLoc[i].lnkItem,g_reg.lidItem, gr_itemsLoc[i].idItemSAP, 2, 
                         gr_itemsLoc[i].nombre, gr_itemsLoc[i].peso,
                         g_reg.lidItem, --gr_itemsExp[i].idItemfac, 
                         gr_itemsLoc[i].idItemSAP,
                         gr_itemsLoc[i].nombrefac, gr_itemsLoc[i].pesofac,
                         gr_itemsLoc[i].fleteCaja, gr_itemsLoc[i].costoDirecto,
                         gr_itemsLoc[i].porcentaje)
          END FOR 
          
        CATCH 
          CALL msgError(sqlca.sqlcode,"Grabar Items Locales")
          RETURN FALSE 
        END TRY  
      CATCH 
        CALL msgError(sqlca.sqlcode,"Grabar Items de Exportación")
        RETURN FALSE 
      END TRY  
        
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 