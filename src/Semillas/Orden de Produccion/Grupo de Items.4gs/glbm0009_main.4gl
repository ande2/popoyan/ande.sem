################################################################################
# Funcion     : %M%
# Descripcion : Modulo Principal
# Funciones   : main_init(dbname)
#               main_menu()                               
#               dummy() 
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia  
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "glbm0009_glob.4gl"
MAIN
	DEFINE 
		n_param 	SMALLINT,
		prog_name2 	STRING
		
	DEFER INTERRUPT

	OPTIONS
		INPUT  WRAP,
		HELP KEY CONTROL-W,
		COMMENT LINE OFF,
		PROMPT LINE LAST - 2,
		MESSAGE LINE LAST - 1,
		ERROR LINE LAST

	LET n_param = num_args()

	IF n_param = 0 THEN
		RETURN
	ELSE
        LET dbname = arg_val(1)
		CONNECT TO dbname
	END IF

	LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
	CALL STARTLOG(prog_name2)
	CALL main_init()
END MAIN

FUNCTION main_init()
DEFINE 
	nom_forma 	STRING,
    w           ui.Window,
    f           ui.Form
	  
	INITIALIZE u_reg.* TO NULL
	LET g_reg.* = u_reg.*

   --CALL trae_usuario() returning g_usuario.*
   --CALL trae_empresa(dbname) returning g_empresa.*
   --CALL trae_programa(prog_name) returning g_programa.*
   --IF g_programa.prog_id = 0 OR 
      --g_programa.prog_id IS NULL THEN
      --CALL box_valdato("Programa no existente, contacte a sistemas")
      --RETURN 
   --END IF 
    CALL ui.Interface.loadActionDefaults("actiondefaults")
    CALL ui.Interface.loadStyles("styles_sc")
   --CALL ui.Interface.loadToolbar("toolbar")

	LET nom_forma = prog_name CLIPPED, "_form"
    --LET nom_forma = "catm0202_form"
    CLOSE WINDOW SCREEN 
	OPEN WINDOW w1 WITH FORM nom_forma
    CALL fgl_settitle("ANDE - Catálogo de Items")

	LET w = ui.WINDOW.getcurrent()
	LET f = w.getForm()
	--CALL fgl_settitle(g_programa.prog_des)
    
	CALL insert_init()
    CALL update_init()
    CALL delete_init()
    CALL combo_init()
	CALL main_menu()
END FUNCTION                                                                    

FUNCTION main_menu()                               
DEFINE  
    cuantos, 
    id, ids	    SMALLINT,
    vopciones   CHAR(255), 
    cnt 		SMALLINT,  
    tol ARRAY[15] OF RECORD
      acc       CHAR(15),
      des_cod   SMALLINT
    END RECORD,
    aui, tb, tbi, tbs om.DomNode

    LET cnt 		= 1
    DISPLAY "REGISTRO DE ITEMS" TO gtit_enc
    
	DISPLAY ARRAY reg_det TO sDet.*
      ATTRIBUTE(ACCEPT=FALSE,CANCEL=FALSE,UNBUFFERED)
   
      BEFORE DISPLAY
         LET cuantos = consulta(false)
      
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            DISPLAY BY NAME g_reg.*
            CALL displayItemsxGrupo()
         END IF 
         {
         LET cuantos = consulta(false)
         IF cuantos > 0 THEN 
            CALL dialog.setCurrentRow("sdet",1)
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
            CALL displayItemsxGrupo()
         END IF 
         CALL encabezado("")}
         --CALL info_usuario()
         
      BEFORE ROW 
         LET id = arr_curr()
         IF id > 0 THEN 
            LET g_reg.* = reg_det[id].*
            DISPLAY BY NAME g_reg.*
            CALL displayItemsxGrupo()
         END IF 

      --ON KEY (CONTROL-W) 
         
      ON ACTION buscar
         
         LET cuantos = consulta(true)
         IF cuantos > 0 THEN 
            LET g_reg.* = reg_det[1].*
            DISPLAY BY NAME g_reg.*
            CALL displayItemsxGrupo()
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
         
      --ON ACTION primero
         --CALL fgl_set_arr_curr( 1 )
      --ON ACTION ultimo
         --CALL fgl_set_arr_curr( arr_count() )
      --ON ACTION anterior 
         --CALL fgl_set_arr_curr( arr_curr() - 1 )
      --ON ACTION siguiente 
         --CALL fgl_set_arr_curr( arr_curr() + 1 )
      ON ACTION agregar
         IF ingreso() THEN 
            LET cuantos = consulta(FALSE)
            CALL fgl_set_arr_curr( arr_count() + 1 )
            --Refrescar Pantalla
            DISPLAY ARRAY reg_det TO sDet.*
               BEFORE DISPLAY  EXIT DISPLAY 
            END DISPLAY 
            --LET g_reg.* = reg_det[arr_curr()].*
            --DISPLAY BY NAME g_reg.*
            --CALL displayItemsxGrupo()
            CALL displayItemsxGrupo()
         END IF
         CALL encabezado("")
         --CALL info_usuario()
      ON ACTION modificar
         LET id = arr_curr()
         LET ids = scr_line()
         IF id > 0 THEN 
            IF modifica() THEN
               LET reg_det[id].* = g_reg.*
               DISPLAY reg_det[id].* TO sDet[ids].*
               CALL displayItemsxGrupo()
            END IF   
         END IF 
         CALL encabezado("")
         --CALL info_usuario()
      ON ACTION anular
         LET id = arr_curr()
         LET ids = scr_line()         
         IF id > 0 THEN 
            IF anular() THEN
               --Si es anulacion
               --LET reg_det[id].* = g_reg.*
               --DISPLAY reg_det[id].* TO sDet[ids].*
               --Si es eliminacion
               CALL DIALOG.deleteRow("sdet", id)
               IF id = arr_count() THEN 
                  LET id = id - 1
               END IF 
               IF id > 0 THEN 
                  LET g_reg.* = reg_det[id].*
               ELSE 
                  INITIALIZE g_reg.* TO NULL
               END IF 
               DISPLAY BY NAME g_reg.*
               CALL displayItemsxGrupo()
            END IF   
         END IF 
      
      {ON ACTION reporte
         CALL repPuesto()}
      ON ACTION salir
         EXIT DISPLAY 
   END DISPLAY 
END FUNCTION

FUNCTION combo_init()
   CALL combo_din2("lidfamilia","SELECT * FROM glbmfam")
   CALL combo_din2("lidtipo","SELECT * FROM glbmtip")
   --CALL combo_din2("id_commpue","SELECT * FROM commpue WHERE id_commpue > 0")
   --CALL combo_din2("jefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   --CALL combo_din2("cmbjefe",'SELECT id_commempl, TRIM(nombre) || " "|| TRIM(apellido) FROM commempl WHERE id_commempl > 0')
   CALL combo_din2("cmbidfamilia","SELECT * FROM glbmfam")
   CALL combo_din2("cmbidtipo","SELECT * FROM glbmtip")
END FUNCTION 

FUNCTION encabezado(gtit_enc)
DEFINE gtit_enc STRING 

   DISPLAY BY NAME  gtit_enc
END FUNCTION 

FUNCTION displayItemsxGrupo()
  DEFINE i SMALLINT 

  --Items de exportacion
  CALL gr_itemsExp.clear()
  DECLARE cur21 CURSOR FOR 
    SELECT lnkItem, idItem, idItemSAP, nombre, peso, iditemsapfac, nombrefac, pesofac,
           fleteCaja, costoDirecto, porcentaje
      FROM itemsxgrupo
     WHERE idItem = g_reg.lidItem
     AND categoria = 1

  LET i = 1
  FOREACH cur21 INTO gr_itemsExp[i].lnkItem, gr_itemsExp[i].idItem, gr_itemsExp[i].idItemSAP,
                     gr_itemsExp[i].nombre, gr_itemsExp[i].peso, gr_itemsExp[i].idItemSAPfac,
                     gr_itemsExp[i].nombrefac, gr_itemsExp[i].pesofac, 
                     gr_itemsExp[i].fleteCaja, gr_itemsExp[i].costoDirecto,
                     gr_itemsExp[i].porcentaje
    LET i = i + 1
  END FOREACH 
  DISPLAY "i lleva ", i
  CALL set_count(i) 
  DISPLAY ARRAY gr_itemsExp TO sDet2.*
    BEFORE DISPLAY          
       EXIT DISPLAY 
  END DISPLAY 

  --Items locales
  CALL gr_itemsLoc.clear()
  DECLARE cur22 CURSOR FOR 
    SELECT lnkItem, idItem, idItemSAP, nombre, peso, iditemsapfac, nombrefac, pesofac,
      fleteCaja, costoDirecto, porcentaje      
      FROM itemsxgrupo
     WHERE idItem = g_reg.lidItem
     AND categoria = 2

  LET i = 1
  FOREACH cur22 INTO gr_itemsLoc[i].lnkItem, gr_itemsLoc[i].idItem, gr_itemsLoc[i].idItemSAP,
                     gr_itemsLoc[i].nombre, gr_itemsLoc[i].peso, gr_itemsLoc[i].idItemSAPfac,
                     gr_itemsLoc[i].nombrefac, gr_itemsLoc[i].pesofac,
                     gr_itemsLoc[i].fleteCaja, gr_itemsLoc[i].costoDirecto,
                     gr_itemsLoc[i].porcentaje
    LET i = i + 1
  END FOREACH 
  DISPLAY ARRAY gr_itemsLoc TO sDet3.*
    BEFORE DISPLAY          
       EXIT DISPLAY 
  END DISPLAY 
     
END FUNCTION 