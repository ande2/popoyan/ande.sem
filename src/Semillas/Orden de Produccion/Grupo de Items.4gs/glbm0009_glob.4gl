################################################################################
# Funcion     : %M%
# id_commdep  : Catalogo de Items
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
DEFINE gr_itemsExp DYNAMIC ARRAY OF RECORD
   lnkItem      LIKE itemsxgrupo.lnkitem,
   idItem       LIKE itemsxgrupo.iditem,
   idItemSAP    LIKE itemsxgrupo.iditemsap,
   nombre       LIKE itemsxgrupo.nombre,
   peso         LIKE itemsxgrupo.peso,
   idItemfac       LIKE itemsxgrupo.iditemfac,
   idItemSAPfac    LIKE itemsxgrupo.iditemsapfac,
   nombrefac       LIKE itemsxgrupo.nombrefac,
   pesofac         LIKE itemsxgrupo.pesofac,
   fleteCaja      LIKE itemsxgrupo.fletecaja,
   costoDirecto   LIKE itemsxgrupo.costodirecto,
   porcentaje     LIKE itemsxgrupo.porcentaje
END RECORD 
DEFINE gr_itemsLoc DYNAMIC ARRAY OF RECORD
   lnkItem      LIKE itemsxgrupo.lnkitem,
   idItem       LIKE itemsxgrupo.iditem,
   idItemSAP    LIKE itemsxgrupo.iditemsap,
   nombre       LIKE itemsxgrupo.nombre,
   peso         LIKE itemsxgrupo.peso,
   idItemfac       LIKE itemsxgrupo.iditemfac,
   idItemSAPfac    LIKE itemsxgrupo.iditemsapfac,
   nombrefac       LIKE itemsxgrupo.nombrefac,
   pesofac         LIKE itemsxgrupo.pesofac,
   fleteCaja      LIKE itemsxgrupo.fletecaja,
   costoDirecto   LIKE itemsxgrupo.costodirecto,
   porcentaje     LIKE itemsxgrupo.porcentaje
END RECORD
TYPE 
   tDet RECORD 
      lidItem     LIKE glbmitems.idItem,
      lidFamilia  LIKE glbmitems.idFamilia,
      lidTipo     LIKE glbmitems.idTipo,
      desItemCt  LIKE glbmitems.desItemCt,
      desItemMd  LIKE glbmitems.desitemmd,
      ldesItemLg  LIKE glbmitems.desitemlg,
      variedad   LIKE glbmitems.variedad,
      calidad    LIKE glbmitems.calidad,
      mercado    LIKE glbmitems.mercado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "glbm0009"
END GLOBALS