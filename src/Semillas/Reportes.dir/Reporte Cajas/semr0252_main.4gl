DATABASE db0001

DEFINE fecIni, fecFin DATE 
DEFINE gr_reg RECORD
   fecha          LIKE pemdcajemp.fecha, 
   idboletaing    LIKE pemdcajemp.idboletaing,
   idordpro       LIKE pemdcajemp.idordpro, 
   id_commemp     LIKE pemdcajemp.id_commemp, 
   idcatempaque   LIKE pemdcajemp.idcatempaque, 
   prestamo       LIKE pemdcajemp.prestamo, 
   devolucion     LIKE pemdcajemp.devolucion
END RECORD

--DEFINE vCalidad SMALLINT 

MAIN
  DEFINE prog_name2 STRING 

  DEFER INTERRUPT 

  LET prog_name2 = "semr0252.log"   -- El progr_name es definido como constante en el arch. globals
    
  CALL STARTLOG(prog_name2)
  CALL main_init()

END MAIN 

FUNCTION main_init()

  CALL ui.Interface.loadActionDefaults("actiondefaults")
  CALL ui.Interface.loadStyles("styles_sc")

  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "semr0252_form1"

  CALL fgl_settitle("ANDE - Movimientos de Cajas - Detallado")
  
MENU "Movimiento de Cajas"
  BEFORE MENU 
    CALL pideparam()
    EXIT MENU 
    
  ON ACTION generar
     CALL pideParam()
  ON ACTION salir
    EXIT MENU 
END MENU 

END FUNCTION 

FUNCTION pideParam()
  DEFINE sqlTxt STRING 
  --DEFINE vestructura, cproductor, citem, citem1, vdes VARCHAR (100)
  DEFINE vestructura VARCHAR (100)
  --DEFINE vBol, vordpro, vdordpro, vproductor, vitem INTEGER
  
  DEFINE myHandler om.SaxDocumentHandler
  
  INPUT BY NAME fecIni, fecFin
    {BEFORE INPUT 
      LET fecIni = "01032016"
      LET fecFin = "30032016"
      EXIT INPUT }
    AFTER FIELD fecIni
      IF fecIni IS NULL THEN
         CALL msg("Ingrese fecha de inicio")
         NEXT FIELD fecIni
      END IF

   AFTER FIELD fecFin
      IF fecFin IS NULL THEN
         CALL msg("Ingrese fecha final")
         NEXT FIELD fecFin
      END IF      
  END INPUT 
  
  IF INT_FLAG THEN 
     CALL msg("Reporte cancelado por el usuario")
     RETURN 
  END IF 
   --SET EXPLAIN ON 
   --Cajas
   LET sqlTxt = " SELECT fecha, idboletaing, idordpro, id_commemp, idcatempaque, prestamo, devolucion  ",
               " FROM pemdcajemp ",
               " WHERE fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'"
  DISPLAY "sqlTxt ", sqlTxt 
  PREPARE ex_sql1 FROM sqlTxt
  DECLARE cur01 CURSOR FOR ex_sql1
  LET myHandler = gral_reporte_all("unapagina","horizontal","XLS",132,"con_r_08")
  START REPORT repMovCajas 
  
  FOREACH cur01 INTO gr_reg.fecha, gr_reg.idboletaing, gr_reg.idordpro, 
          gr_reg.id_commemp, gr_reg.idcatempaque, gr_reg.prestamo, gr_reg.devolucion

    OUTPUT TO REPORT repMovCajas(gr_reg.*, vEstructura)
    
  END FOREACH   

  FINISH REPORT repMovCajas  
  CALL fgl_report_stopGraphicalCompatibilityMode()
END FUNCTION 

REPORT repMovCajas(lr_reg, lEstructura )
DEFINE lr_reg  RECORD
   fecha          LIKE pemdcajemp.fecha, 
   idboletaing    LIKE pemdcajemp.idboletaing,
   idordpro       LIKE pemdcajemp.idordpro, 
   id_commemp     LIKE pemdcajemp.id_commemp, 
   idcatempaque   LIKE pemdcajemp.idcatempaque, 
   prestamo       LIKE pemdcajemp.prestamo, 
   devolucion     LIKE pemdcajemp.devolucion
END RECORD
--DEFINE lproductor LIKE pemmboleta.productor
--DEFINE lpesototal LIKE pemmboleta.pesototal
--DEFINE lfecha     LIKE pemmboleta.fecha
--DEFINE lcantcajas INT 
--DEFINE cproductor LIKE commemp.nombre
DEFINE lEstructura VARCHAR (100)

DEFINE lFinca, lTipoMov, lNomCaja     VARCHAR (100)
DEFINE lCantidad  INTEGER 
 
FORMAT 

FIRST PAGE HEADER 
  PRINT COLUMN 01, "MOVIMIENTOS DE CAJAS - POR FECHA"
  PRINT COLUMN 01, "Fecha inicial: ", fecini
  PRINT COLUMN 01, "Fecha final:   ", fecfin
  PRINT COLUMN 01, "."
  
  PRINT COLUMN 01, "Fecha",
        COLUMN 10, "Documento", 
        COLUMN 20, "Productor", 
        COLUMN 60, "Movimiento",
        COLUMN 80, "Tipo de Caja",
        COLUMN 130, "Cantidad" --,
        --COLUMN 180, "Cantidad(D)"
        
ON EVERY ROW 
   --Para finca
   LET lFinca =  NULL 
   SELECT t1.fincanomct
      INTO lFinca 
      FROM pemmfinca t1, pemdordpro t2, pemmboleta t3
      WHERE t1.idfinca = t2.idfinca
      AND t2.idordpro = t3.idordpro
      AND t2.iddordpro = t3.iddordpro
      AND t3.idboletaing = lr_reg.idboletaing
   IF sqlca.sqlcode = 0 THEN    
      LET lFinca = "VALLE ", lfinca CLIPPED
   END IF 

   --Para productor
   SELECT trim(t1.nombre)
      INTO lFinca 
      FROM commemp t1, pemmboleta t2  
      WHERE t1.id_commemp = t2.productor
      AND t2.idboletaing = lr_reg.idboletaing

   --Para movimiento
   LET lTipoMov = NULL 
   IF lr_reg.prestamo > 0 THEN
      LET lTipoMov = "Prestamo"
   ELSE 
      IF lr_reg.devolucion > 0 THEN
         LET lTipoMov = "Devolucion"
      ELSE 
         LET lTipoMov = ""
      END IF 
   END IF 

   --Para nombre caja
   SELECT nomcatempaque 
      INTO lNomCaja
      FROM pemmcatemp where idcatempaque = lr_reg.idcatempaque

   --para cantidad
   --LET lTipoMov = NULL 
   IF lr_reg.prestamo > 0 THEN
      LET lCantidad = lr_reg.prestamo
   ELSE 
      IF lr_reg.devolucion > 0 THEN
         LET lCantidad = lr_reg.devolucion * -1
      ELSE 
         LET lCantidad = NULL 
      END IF 
   END IF 
  PRINT COLUMN 01, lr_reg.fecha , 
        COLUMN 10, lr_reg.idboletaing USING "###,##&", 
        COLUMN 20, lfinca , --lr_reg.idordpro, 
        COLUMN 60, lTipoMov,
        COLUMN 80, lNomCaja , --lr_reg.idcatempaque,
        COLUMN 130, lCantidad USING "###,##&" --lr_reg.prestamo,
        --COLUMN 180, lr_reg.devolucion
END REPORT 
