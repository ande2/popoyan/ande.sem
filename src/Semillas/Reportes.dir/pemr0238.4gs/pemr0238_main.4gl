IMPORT util 

DATABASE db0001

DEFINE vfecIni, vfecFin DATE 
DEFINE vidboletaing INTEGER 
DEFINE vTipoBoleta LIKE pemmboleta.tipoboleta
DEFINE vcoditem SMALLINT 
DEFINE vnomItem VARCHAR(50)
DEFINE vTipoCaja VARCHAR(50)
DEFINE pos, vSumCajas SMALLINT 



DEFINE sqlTxt STRING
DEFINE vestructura, cproductor, citem, citem1, vdes VARCHAR (100)
DEFINE vBol, vordpro, vdordpro, vproductor, vitem INTEGER

DEFINE gr_reg RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  fectran     LIKE pemmboleta.fectran,
  docpropioserie LIKE pemmboleta.docpropioserie,
  docpropio   LIKE pemmboleta.docpropio,
  pesoTotal   LIKE pemmboleta.pesototal  
END RECORD
DEFINE vdescripcion VARCHAR (100)

MAIN
  DEFINE prog_name2 STRING 

  DEFER INTERRUPT 

  LET prog_name2 = "pemr0238.log"   -- El progr_name es definido como constante en el arch. globals
    
  CALL STARTLOG(prog_name2)
  CALL creatmp()
  CALL main_init()

END MAIN 

FUNCTION main_init()

  CALL ui.Interface.loadActionDefaults("actiondefaults")
  CALL ui.Interface.loadStyles("styles_sc")

  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "pemr0238_form1"

  CALL fgl_settitle("ANDE - Reporte de general de boletas")
  
MENU "Boletas"
  {BEFORE MENU 
    LET vidboletaing = 90
    CALL reporte1()}
  ON ACTION generar
     IF pideParam() THEN
     --LET vidBoletaIng = 90
        CALL reporte1() 
     END IF 
     
  ON ACTION salir
    EXIT MENU 
END MENU 

END FUNCTION 

FUNCTION pideParam()
   
  INPUT vidboletaing, vfecini, vfecfin
    FROM  idboletaing, fecIni, fecFin

    BEFORE INPUT 
      LET INT_FLAG = FALSE 
    
    AFTER FIELD idboletaing
       IF vidBoletaIng IS NOT NULL THEN 
          SELECT idBoletaing FROM pemmboleta WHERE idboletaing = vidboletaing
          IF sqlca.sqlcode <> 0 THEN CALL msg("Boleta no existe") NEXT FIELD idboletaing END IF
       ELSE
          CALL msg("Debe seleccionar boleta")   
          NEXT FIELD idboletaing
       END IF 

    ON ACTION buscar
          CALL llenatmp()
          CALL picklist_2("Boletas", "Boleta","Descripción","idorden", "descripcion", "ordenes", "1=1", "1", 1)
            RETURNING vidBoletaIng, vDescripcion, INT_FLAG
          IF INT_FLAG THEN 
             LET INT_FLAG = FALSE 
             LET vidBoletaIng = NULL 
           END IF 
           DISPLAY vidboletaing TO idboletaing   
          {--Que no sea nulo
           IF gr_reg.idBoletaing IS NULL THEN
              CALL msg("Debe ingresar orden de producción")
              NEXT FIELD idboletaing
           ELSE 
              DISPLAY BY NAME gr_reg.idBoletaIng   
           END IF

           LET sqlTxt = " SELECT tipoboleta, idordpro, iddordpro, productor, ",
                        " iditem, pesoTotal ",
                        " FROM pemmboleta WHERE idboletaing = ", gr_reg.idBoletaIng

           PREPARE ex_st1 FROM sqlTxt
           EXECUTE ex_st1 INTO ltipo, lidordpro, liddordpro, lproductor, 
                               litem, lPesoTotal 
           --Si boleta no existe
           IF sqlca.sqlcode = 100 THEN 
              CALL msg(msgTxt)
              NEXT FIELD idboletaing
           END IF 

           --Para datos de finca/productor
           IF ltipo = 1 THEN
              --Para estructura
              SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg 
              INTO vEstructura
              FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
              WHERE a.idfinca = b.idfinca 
              AND a.idestructura = c.idEstructura 
              AND a.idValvula = d.idValvula 
              AND a.idItem = e.idItem 
              AND a.idOrdPro = lidordpro 
              AND a.iddordpro = liddordpro

              DISPLAY vEstructura TO estructura
              --limpia productor
              LET vProductor = NULL 
              LET vItem = NULL 
              DISPLAY vProductor TO productor
              DISPLAY vItem TO producto
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemdordpro c, glbmtip b, pemmboleta d
              WHERE d.idBoletaIng = gr_reg.idBoletaIng
              AND  d.idOrdPro = c.idOrdPro
              AND  d.idDOrdPro = c.idDOrdPro
              AND  c.idItem = a.idItem
              AND  a.idtipo = b.idtipo  
              DISPLAY vItem TO producto
           ELSE 
              --Para productor
              SELECT trim(nombre)
              INTO vProductor 
              FROM commemp 
              WHERE id_commemp = lproductor
              DISPLAY vProductor TO productor
              
              --Para producto / item
              SELECT TRIM (nombre) || " " || a.desItemLg
              INTO vItem 
              FROM glbMItems a, pemmboleta c, glbmtip b
              WHERE c.idBoletaIng = gr_reg.idBoletaIng
              AND a.idtipo = b.idtipo  
              --AND  c.idOrdPro = b.idOrdPro
              --AND  c.idDOrdPro = b.idDOrdPro
              AND  a.idItem = c.idItem
              DISPLAY vItem TO producto

              --Limpia estructura
              LET vEstructura = NULL 
              DISPLAY vEstructura TO estructura
           END IF 

           --Para pesos
           DISPLAY lPesoTotal TO fpesototal
           CALL calculaPesoSaldo() 
           DISPLAY lPesoSaldo TO fPesoSaldo
           IF accion = "B" THEN
              RETURN 
           END IF   }
  END INPUT 
  IF INT_FLAG THEN 
     CALL msg("Reporte cancelado por el usuario")
     RETURN FALSE 
  END IF 
  RETURN TRUE 
END FUNCTION 

FUNCTION reporte1()

  DEFINE myHandler om.SaxDocumentHandler
             
  --Boletas
  LET sqlTxt = " SELECT idboletaing, tipoBoleta, idOrdPro, idDOrdPro, productor, ",
               " fectran, docpropioserie, docpropio, pesototal ",
               " FROM pemmboleta ",
               " WHERE 1=1 "
  IF vidboletaing IS NOT NULL THEN 
     LET sqlTxt = sqlTxt || " AND idboletaing = ", vidboletaing
  END IF 
  DISPLAY "sqlTxt ", sqlTxt
               --" estado in (1,2) AND fecha BETWEEN '", fecIni, "'",
               --" AND '", fecFin, "'"
  PREPARE ex_sql1 FROM sqlTxt
  DECLARE cur01 CURSOR FOR ex_sql1

  LET myHandler = gral_reporte("carta","vertical","PDF",80,"ANDE_Rep_238")
               
  START REPORT repBolIng 
  
  FOREACH cur01 INTO gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     gr_reg.idDOrdPro, gr_reg.productor, gr_reg.fectran,
                     gr_reg.docpropioserie, gr_reg.docpropio, gr_reg.pesoTotal
    DISPLAY "Boleta ", gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     gr_reg.idDOrdPro, gr_reg.productor, gr_reg.fectran,
                     gr_reg.docpropioserie, gr_reg.docpropio, gr_reg.pesoTotal 
    
    --Para datos de finca/productor
    IF gr_reg.tipoBoleta = 1 THEN
       --Para estructura
       SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || nombre || '-' || desItemLg 
       INTO vEstructura
       FROM pemdordpro a, pemmfinca b, mestructura c, glbmtip f, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
       AND a.idestructura = c.idEstructura 
       AND a.idValvula = d.idValvula 
       AND a.idItem = e.idItem 
       AND a.idOrdPro = gr_reg.idOrdPro 
       AND a.iddordpro = gr_reg.idDOrdPro
       AND e.idtipo = f.idtipo

       DISPLAY "vestructura ", vEstructura 
       --limpia productor
       LET vProductor = NULL 
       LET vItem = NULL 
       DISPLAY "vProductor ", vProductor
       DISPLAY "vItem ", vItem 
    ELSE 
       --Para productor
       SELECT trim(nombre)
       INTO cProductor 
       FROM commemp 
       WHERE id_commemp = gr_reg.productor
       DISPLAY "vProductor ", cProductor
              
       --Para producto / item
       SELECT nombre
       INTO cItem1
       FROM pemmboleta a, glbmitems b, glbmtip c
       WHERE c.idtipo = b.idtipo
       AND b.iditem = a.iditem
       AND a.idboletaing = gr_reg.idboletaing
       
       SELECT FIRST 1 desItemLg
       INTO cItem 
       FROM glbMItems a, pemmboleta c
       WHERE c.idItem = a.idItem 

       DISPLAY "vItem ", cItem

       --Limpia estructura
       LET vEstructura = cItem1 CLIPPED, "-", cItem 
       DISPLAY "vEstrctura ", vEstructura
    END IF 
    OUTPUT TO REPORT repBolIng(gr_reg.*, vEstructura)
  END FOREACH   

  FINISH REPORT repBolIng  

  CALL fgl_report_stopGraphicalCompatibilityMode()
  
END FUNCTION 

REPORT repBolIng(lr_reg, lEstructura )
DEFINE lr_reg  RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  fectran     LIKE pemmboleta.fectran,
  docpropioserie LIKE pemmboleta.docpropioserie,
  docpropio   LIKE pemmboleta.docpropio,
  pesoTotal   LIKE pemmboleta.pesototal  
   
END RECORD
DEFINE lproductor LIKE pemmboleta.productor
DEFINE lpesototal LIKE pemmboleta.pesototal
DEFINE lfecha     LIKE pemmboleta.fecha
DEFINE lcantcajas INT 
DEFINE cproductor LIKE commemp.nombre
DEFINE lEstructura VARCHAR (100)
DEFINE pline STRING 
DEFINE lvar, lvar2, lvar3 VARCHAR (100)
DEFINE vlnkItem, vcantCajas, vpesoTotal, vpesomerma, vSumPeso INTEGER 
DEFINE vEmplEntrego, vEmplRecibio VARCHAR(50)
DEFINE vBoleta INTEGER 
DEFINE vPorc SMALLINT 
DEFINE vPorcSaldo, vTest SMALLINT 
DEFINE fPorc TINYINT 
DEFINE vEstado LIKE pemmboleta.estado

OUTPUT 
  PAGE LENGTH 75 
  
FORMAT 
--PAGE HEADER
ON EVERY ROW 

  LET lvar = getValParam("RECEPCIÓN DE FRUTA - CODIGO")
  PRINT COLUMN 075, "Código: ", lvar
  LET lvar = getValParam("RECEPCION DE FRUTA - VERSION")
  PRINT COLUMN 075, "Versión: ", lvar
  LET lvar = getValParam("RECEPCION DE FRUTA - FECHA DE ELABORACION")
  PRINT COLUMN 005, "SEMILLAS DEL CAMPO, S.A.",
        COLUMN 035, "REGISTRO DE RECEPCIÓN DE FRUTA",
        COLUMN 075, lvar
  LET lvar = getValParam("RECEPCION DE FRUTA - PAGINAS")      
  PRINT 
        --COLUMN 043, "FRUTA",
        COLUMN 075, "Páginas: ", lvar
  SKIP 1 LINE
  LET pline = "=========="  
  PRINT COLUMN 005, pline CLIPPED ,
                    pline CLIPPED ,
                    pline CLIPPED ,
                    pline CLIPPED ,
                    pline CLIPPED ,
                    pline CLIPPED ,
                    pline CLIPPED ,
                    pline CLIPPED ,
                    "======" 
  PRINT COLUMN 075, "No.", lr_reg.idboletaing USING "&&&&&&" 
  SELECT estado INTO vEstado FROM pemmboleta WHERE idboletaing = lr_reg.idboletaing
  CASE 
    WHEN vEstado = -1 PRINT COLUMN 35, "-------------ANULADA------------"
    --WHEN vEstado = 0 PRINT ""
    WHEN vEstado > 0 PRINT COLUMN 35,  "------------EN PROCESO----------"
  END CASE                   
  SKIP 1 LINE 

  
  
 -- ON EVERY ROW
    --Para datos de finca/productor
    SELECT tipoBoleta INTO vTipoBoleta FROM pemmboleta WHERE idboletaing = vidboletaing
  
    IF vTipoBoleta = 1 THEN
       --Para estructura
       SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,''), nombre || '-' || desItemLg 
       INTO vEstructura, cItem
       FROM pemdordpro a, pemmfinca b, mestructura c, glbmtip f, OUTER(pemMValvula d), glbMItems e, pemmboleta g 
       WHERE g.idboletaing = vidboletaing
       AND   g.idOrdPro = a.idOrdPro
       AND   g.idDOrdPro = a.idDOrdPro
       AND a.idfinca = b.idfinca 
       AND a.idestructura = c.idEstructura 
       AND a.idValvula = d.idValvula 
       AND a.idItem = e.idItem 
       AND e.idtipo = f.idtipo

       DISPLAY "vestructura ", vEstructura 
       --limpia productor
       --LET vProductor = NULL 
       --LET vItem = NULL 
       --DISPLAY "vProductor ", vProductor
       --DISPLAY "vItem ", vItem 
    ELSE 
       --Para productor
       SELECT trim(nombre)
       INTO cProductor 
       FROM commemp a, pemmboleta b
       WHERE b.idboletaing = vidboletaing 
       AND b.productor = a.id_commemp
       DISPLAY "vProductor ", cProductor
              
       --Para producto / item
       SELECT nombre
       INTO cItem1
       FROM pemmboleta a, glbmitems b, glbmtip c
       WHERE c.idtipo = b.idtipo
       AND b.iditem = a.iditem
       AND a.idboletaing = gr_reg.idboletaing

       SELECT FIRST 1 desItemLg
       INTO cItem 
       FROM glbMItems a, pemmboleta c
       WHERE c.idItem = a.idItem 

       DISPLAY "vItem ", cItem

       --Limpia estructura
       LET vEstructura = cItem1 CLIPPED, "-", cItem 
    END IF 
       --DISPLAY "vEstrctura ", vEstructura
  SELECT fecha, pesoTotal INTO lfecha, vlnkItem FROM pemmboleta WHERE idboletaing = vidboletaing  
  SELECT SUM (cantCajas) INTO lvar FROM pemdboleta WHERE  idboletaing = vidboletaing
  PRINT COLUMN 005, "INFORMACIÓN DE RECEPCIÓN"
  --PRINT COLUMN 005, "PRODUCTOR/"
  IF vTipoBoleta = 1 THEN
     PRINT COLUMN 005, "Estructura     : ", vEstructura
  ELSE
     PRINT COLUMN 005, "Productor      : ", cProductor
  END IF   
        --COLUMN 012, vEstructura, 
  PRINT COLUMN 005, "Fecha documento: ", lfecha
  PRINT COLUMN 005, "Hora           : ", TIME (lr_reg.fectran) 
  PRINT COLUMN 005, "# Documento    : ", lr_reg.docpropioserie CLIPPED, lr_reg.docpropio
  SKIP 1 LINE 
  PRINT COLUMN 038, "INFORMACIÓN DEL PRODUCTO"
  PRINT COLUMN 038, "========================"
        --COLUMN 068, "Boleta: ", vidboletaing USING "<<<,<<<"
  --SKIP 1 LINE 
  PRINT COLUMN 005, "CODIGO", 
        COLUMN 015, "PRODUCTO",
        --COLUMN 010, cItem,
        COLUMN 033, "TIPO CAJA        ",
        COLUMN 059, "CANT CAJAS",
        --COLUMN 036, lvar CLIPPED ,
        COLUMN 073, "PESO EN LBS" --,
        --COLUMN 060, vlnkItem USING "###,###" 
  PRINT COLUMN 005, "------", 
        COLUMN 015, "---------------",
        --COLUMN 010, cItem,
        COLUMN 033, "----------------------",
        COLUMN 059, "----------",
        --COLUMN 036, lvar CLIPPED ,
        COLUMN 073, "-----------" --,
        --COLUMN 060, vlnkItem USING "###,###"  
        
  --Para codigo y nombre de item
   IF lr_reg.tipoboleta = 1 THEN 
          
      SELECT c.iditem, c.desitemlg
        INTO vCodItem, vNomItem
        FROM  pemmboleta a, pemDOrdPro b, glbmitems c
       WHERE a.idordpro = b.idordpro
         AND a.iddordpro = b.iddordpro
         AND b.iditem = c.iditem
         AND a.idboletaing = lr_reg.idboletaing

   ELSE
       SELECT b.idItem, b.desItemLg
         INTO vCodItem, vNomItem
         FROM pemmboleta a, glbMItems b
        WHERE a.idItem = b.idItem
          AND a.idboletaing = lr_reg.idboletaing
          
   END IF
   --Para Tipo de Caja
   SELECT b.nomcatempaque
     INTO vTipoCaja
     FROM pemMBoleta a, pemmcatemp b
    WHERE a.tipocaja = b.idcatempaque
      AND  a.idBoletaIng = lr_reg.idboletaing
   --LET pLine[40,69] = vTipoCaja    
   --Total de cajas
   SELECT SUM(cantCajas) 
     INTO vSumCajas 
     FROM pemDBoleta
    WHERE idBoletaIng = lr_reg.idboletaing
   --LET pLine[75,80] = vSumCajas

   --Peso total
   --LET pLine[88,100] = grRep.pesototal
   --Peso total
   --LET pLine[88,100] = grRep.pesototal
   PRINT COLUMN 006, vCodItem USING "<<<<",
             COLUMN 015, vNomItem,
             COLUMN 033, vTipoCaja,
             COLUMN 063, vSumCajas USING "<<<",
             COLUMN 074, lr_reg.pesototal USING "###,###"
             
   --OUTPUT TO REPORT rRep(grRep.*,pLine)
   --LET pLine = ""
      
  SKIP 2 LINE 
  --obteniendo el peso total de exportacion
  SELECT a.idboletaing, SUM (b.pesoTotal)
    INTO vBoleta, vpesoTotal
    FROM pemmboletaexp a, pemdboletaexp b 
    WHERE a.idboletaexp = b.idboletaexp
    AND a.idboletaing = vidboletaing
  GROUP BY 1

  --agregando la merma
  SELECT merma INTO vpesomerma FROM pemmboletacau WHERE idboletaing = lr_reg.idboletaing
  IF vpesomerma IS NULL OR vpesomerma < 0 THEN LET vpesomerma = 0 END IF 
  LET vPesoTotal = vPesoTotal + vpesomerma
  
  --calculando el porcentaje
  --LET vResult = vPesoTotal * 100 / lr_reg.pesototal
  LET vPorc = round(vPesoTotal * 100 / lr_reg.pesototal, 0)
  IF vPorc > 0 THEN LET vPorcSaldo = 100 - vPorc ELSE LET vPorcSaldo = 100 END IF 
  
  PRINT COLUMN 005, "EXPORTACIÓN",
        COLUMN 017, "(", vPorc USING "###", "%)"
  PRINT COLUMN 015, "ITEM",
        COLUMN 059, "CANT CAJAS",
        COLUMN 073, "PESO EN LBS"
  PRINT COLUMN 015, "----------------------",
        COLUMN 059, "----------",
        COLUMN 073, "-----------"
        
  DECLARE cur11 CURSOR FOR 
    SELECT b.lnkItem, c.nombre, b.cantCajas, b.pesoTotal
    FROM pemmboletaexp a, pemdboletaexp b, itemsxgrupo c
    WHERE a.idboletaexp = b.idboletaexp
    AND a.idboletaing = vidboletaing
    AND b.lnkitem = c.lnkitem
  LET vSumPeso = 0
  FOREACH cur11 INTO vlnkItem, lvar, vcantCajas, vpesoTotal
     IF vpesoTotal IS NOT NULL THEN LET vSumPeso = vSumPeso + vpesoTotal END IF 
     PRINT COLUMN 015, lvar CLIPPED, --vlnkItem USING "###,###",
           COLUMN 058, vcantCajas USING "###,###",
           COLUMN 074, vpesoTotal USING "###,###" 
  END FOREACH  
  SKIP 1 LINE 
  --Para merma
  
    LET lvar = NULL 
    LET sqlTxt =  " SELECT a.nombre ",
      " FROM itemsxgrupo a, pemmboleta b ",
      " WHERE a.nombre LIKE '%MERMA%' ",
      " AND a.iditem = b.iditem ",
      " AND b.idboletaing = ", lr_reg.idboletaing ,
      " UNION ",
      " SELECT i.nombre ",
      " FROM itemsxgrupo i, pemmboleta b, pemdordpro d ",
      " WHERE i.nombre LIKE '%MERMA%' ",
      " AND b.idordpro = d.idordpro ",
      " AND b.iddordpro = d.iddordpro ",
      " AND d.iditem = i.iditem ",
      " AND b.idboletaing = ", lr_reg.idboletaing
    PREPARE ex_stmt FROM sqlTxt
    DECLARE ccur CURSOR FOR ex_stmt
    FOREACH ccur INTO lvar
    END FOREACH 
    LET vpesototal = 0  
    SELECT merma INTO vpesototal FROM pemmboletacau WHERE idboletaing = lr_reg.idboletaing
    IF vpesototal > 0 THEN LET vSumPeso = vSumPeso + vpesoTotal END IF 
    IF vpesototal IS NULL OR vpesototal < 0 THEN LET vpesototal = 0 END IF 
    --DISPLAY "Merma lleva ", vpesoTotal
    --EXECUTE IMMEDIATE sqlTxt INTO lvar
    --IF vpesoTotal IS NOT NULL THEN LET vSumPeso = vSumPeso + vpesoTotal END IF
    --IF lvar IS NULL THEN LET vpesoTotal = 0 END IF  
    PRINT COLUMN 015, lvar CLIPPED , 
          COLUMN 074, vpesoTotal USING "###,###"  

  --para Carreta
  SKIP 1 LINE
  LET vpesoTotal = 0
  LET vBoleta = 0 
  --calculando peso total
  SELECT  a.idboletaloc, SUM (a.pesoneto) 
    INTO vBoleta, vpesoTotal
    FROM pemdboletaloc a, pemmboletaloc b, pemmcli c 
    WHERE a.idboletaloc = b.idboletaloc
    AND b.idboletaing = vidboletaing
    AND b.idcliente = c.idcliente
    AND c.nombre LIKE "%CARRETA%"
  GROUP BY 1
  --calculando porcentaje
  LET vPorc = round(vPesoTotal * 100 / lr_reg.pesototal,0)
  IF vPorc > 0 THEN LET vPorcSaldo = vPorcSaldo - vPorc END IF 
  
  PRINT COLUMN 005, "CARRETA",
        COLUMN 017, "(", vPorc USING "###", "%)"
  PRINT COLUMN 015, "ITEM",
        COLUMN 073, "PESO EN LBS"
  PRINT COLUMN 015, "----------------------",
        COLUMN 073, "-----------"
 { PRINT COLUMN 001, "CARRETA"
  PRINT COLUMN 015, "ITEM",
        COLUMN 063, "LBS"}

  DECLARE curCarreta CURSOR FOR       
  SELECT d.nombre, a.pesoneto 
    FROM pemdboletaloc a, pemmboletaloc b, pemmcli c, itemsxgrupo d 
    WHERE a.idboletaloc = b.idboletaloc
    AND b.idboletaing = vidboletaing
    AND b.idcliente = c.idcliente
    AND c.nombre LIKE "%CARRETA%"
   AND a.lnkitem = d.lnkitem  
  LET lvar = NULL  
  FOREACH curCarreta INTO lvar, vpesototal
    IF vpesoTotal IS NOT NULL THEN LET vSumPeso = vSumPeso + vpesoTotal END IF
    PRINT COLUMN 015, lvar CLIPPED, 
          COLUMN 074, vpesototal USING "###,###" 
  END FOREACH
  IF lvar IS NULL THEN  
     PRINT COLUMN 015, "Vacio"    
  END IF   
  
  --para Cenma
  SKIP 1 LINE 
  LET vpesoTotal = 0
  LET vBoleta = 0 
  --calculando peso total
  SELECT  a.idboletaloc, SUM (a.pesoneto) 
    INTO vBoleta, vpesoTotal
    FROM pemdboletaloc a, pemmboletaloc b, pemmcli c 
    WHERE a.idboletaloc = b.idboletaloc
    AND b.idboletaing = vidboletaing
    AND b.idcliente = c.idcliente
    AND c.nombre LIKE "%CENMA%"
  GROUP BY 1
  --calculando porcentaje
  --Valida devolucion, si no hay cuadra el porcentaje
  LET vTest = 0
  SELECT SUM ( NVL (a.pesoneto, 0)) INTO vTest
    FROM pemdboletaloc a, pemmboletaloc b, pemmcli c 
    WHERE a.idboletaloc = b.idboletaloc
    AND b.idboletaing = vidboletaing
    AND b.idcliente = c.idcliente
    AND c.nombre LIKE "%DEVOLUC%"
    IF vTest = 0 THEN 
       LET vPorc = vPorcSaldo
    ELSE 
       LET vPorc = round(vPesoTotal * 100 / lr_reg.pesototal,0)
       IF vPorc > 0 THEN LET vPorcSaldo = vPorcSaldo - vPorc END IF 
    END IF 
  
  PRINT COLUMN 005, "CENMA",
        COLUMN 017, "(", vPorc USING "###", "%)"
  PRINT COLUMN 015, "ITEM",
        COLUMN 073, "PESO EN LBS"
  PRINT COLUMN 015, "----------------------",
        COLUMN 073, "-----------" 
  
  DECLARE curCenma CURSOR FOR       
  SELECT d.nombre, a.pesoneto 
    FROM pemdboletaloc a, pemmboletaloc b, pemmcli c, itemsxgrupo d 
    WHERE a.idboletaloc = b.idboletaloc
    AND b.idboletaing = vidboletaing
    AND b.idcliente = c.idcliente
    AND c.nombre LIKE "%CENMA%"
   AND a.lnkitem = d.lnkitem  
  LET lvar = NULL  
  FOREACH curCenma INTO lvar, vpesototal
    IF vpesoTotal IS NOT NULL THEN LET vSumPeso = vSumPeso + vpesoTotal END IF 
    PRINT COLUMN 015, lvar CLIPPED, 
          COLUMN 074, vpesototal USING "###,###" 
  END FOREACH         
  IF lvar IS NULL THEN  
     PRINT COLUMN 015, "Vacio"    
  END IF 
  
  SKIP 1 LINE 
  --para devolucion
  LET vpesototal = 0 
  LET fPorc = 0
  DECLARE curDev CURSOR FOR 
  SELECT d.nombre, a.pesoneto 
    --INTO lvar, vpesototal
    FROM pemdboletaloc a, pemmboletaloc b, pemmcli c, itemsxgrupo d 
    WHERE a.idboletaloc = b.idboletaloc
    AND b.idboletaing = vidboletaing
    AND b.idcliente = c.idcliente
    AND c.nombre LIKE "%DEVOLUC%"
    AND a.lnkitem = d.lnkitem  
    FOREACH curDev INTO lvar, vpesototal
       
       IF vpesoTotal IS NOT NULL THEN LET vSumPeso = vSumPeso + vpesoTotal END IF
       
       IF vpesototal > 0 THEN
          IF fPorc = 0 THEN
             PRINT COLUMN 015, lvar CLIPPED, 
                   COLUMN 074, vpesototal USING "###,###",
                   COLUMN 081, "(", vPorcSaldo USING "###", "%)"  
          ELSE 
             PRINT COLUMN 015, lvar CLIPPED, 
                   COLUMN 074, vpesototal USING "###,###"
                
          END IF 
          
       END IF
       LET fPorc = 1       
    END FOREACH 
  SKIP 1 LINE
  PRINT COLUMN 072, "-----------"
  PRINT COLUMN 055, "TOTAL EN LBS: ", 
        COLUMN 074, vSumPeso USING "###,###"
  PRINT COLUMN 072, "==========="      
  SKIP 1 LINE 
  PRINT COLUMN 005, "CAUSAS DE RECHAZO"
  PRINT COLUMN 015, "CAUSA",
        COLUMN 073, "PORCENTAJE"
  PRINT COLUMN 015, "----------------------",
        COLUMN 073, "-----------" 
  {PRINT COLUMN 005, "CAUSAS DE RECHAZO"
  PRINT COLUMN 015, "CAUSA",
        COLUMN 053, "%"}
        --COLUMN 058, "OBSERVACIONES" 

  DECLARE curCausa CURSOR FOR 
    SELECT a.nombre, b.porcentaje, b.observaciones
      FROM pemmcausa a, pemdboletacau b, pemmboletacau c
      WHERE c.idboletaing = vidboletaing
      AND   c.idboletacau = b.idboletacau
      AND   b.idcausa = a.idcausa
  FOREACH curCausa INTO lvar, vpesototal, lvar2
    PRINT COLUMN 015, lvar CLIPPED ,
          COLUMN 078, vpesototal USING "###" 
          --COLUMN 058, lvar2 CLIPPED
  END FOREACH 
  SKIP 3 LINES
  --Para nombre piloto
   IF lr_reg.tipoboleta = 1 THEN
      SELECT TRIM(b.nombre)||' '||TRIM(b.apellido)
        INTO vEmplEntrego
        FROM pemmboleta a, commempl b
       WHERE a.emplentrego = b.id_commempl
         AND idboletaing = lr_reg.idboletaing
   ELSE 
      SELECT nomentrego 
        INTO vEmplEntrego
        FROM pemmboleta 
       WHERE idboletaing = lr_reg.idboletaing 
   END IF 
   --Para receptor
   SELECT TRIM(b.nombre)||' '||TRIM(b.apellido)
     INTO vEmplRecibio
     FROM pemmboleta a, commempl b
    WHERE a.emplrecibio = b.id_commempl
      AND idboletaing = lr_reg.idboletaing
  PRINT COLUMN 010, "Recibido por: ", vEmplRecibio,
        COLUMN 050, "Entregado por: ", vEmplEntrego
  SKIP 2 LINES

  PRINT COLUMN 001, pline, pline, pline, pline, pline, pline, pline, pline, pline, "======"
  PRINT COLUMN 001, "| Elaborado por:",
        COLUMN 045, "| Revisado por:",
        COLUMN 075, "| Autorizado por:",
        COLUMN 096, "|"
  PRINT COLUMN 001, "| Asistencia de Costos, Exportaciones e",
        COLUMN 045, "| Administrador",
        COLUMN 075, "| Gerente",
        COLUMN 096, "|"
  PRINT COLUMN 001, "| Inventarios",
        COLUMN 045, "|", 
        COLUMN 075, "|",
        COLUMN 096, "|"
  PRINT COLUMN 001, pline, pline, pline, pline, pline, pline, pline, pline, pline, "======"      
  SKIP 1 LINES 
  PRINT COLUMN 001, pline, pline, pline, pline, pline, pline, pline, pline, pline, "======"      
  PRINT COLUMN 001, "| El producto se recibe sujeto a posterior inspección de calidad",
                    " y procesamiento, el proveedor ",
        COLUMN 096, "|"            
  PRINT COLUMN 001, "| se compromete a aceptar resultados de inspección y clasificación, así como normas",
                    " y/o ",
        COLUMN 096, "|"
  PRINT COLUMN 001, "| devoluciones originales del proceso, según convenio específico de compra.",
        COLUMN 096, "|"
  PRINT COLUMN 001, pline, pline, pline, pline, pline, pline, pline, pline, pline, "======"      
  
  
  END REPORT
  {PRINT COLUMN 01, "Fecha inicial: ", vfecini
  PRINT COLUMN 01, "Fecha final:   ", vfecfin
  SKIP 1 LINE 
  PRINT COLUMN 01, "ID Boleta", 
        COLUMN 15, "Productor",
        COLUMN 40, "Cant Cajas",
        COLUMN 55, "Peso Lbs",
        COLUMN 70, "Fecha",
        COLUMN 85, "Estructura"

  PRINT COLUMN 01, "---------", 
        COLUMN 15, "----------",
        COLUMN 40, "----------",
        COLUMN 55, "----------",
        COLUMN 70, "----------",
        COLUMN 85, "-----------------------------"}
        
        
 
  --Productor, pesoLbs, fecha
  {SELECT productor, pesoTotal, fecha
  INTO lproductor, lpesototal, lfecha
  FROM pemmboleta
  WHERE idboletaing = lr_reg.idboletaing

  SELECT nombre INTO cproductor 
  FROM commemp 
  WHERE id_commemp = lproductor

  --cantCajas
  SELECT SUM (cantCajas) 
  INTO lcantcajas
  FROM pemmboleta a, pemdboleta b
  WHERE a.idboletaing = b.idboletaing
  AND a.idboletaing = lr_reg.idboletaing
      
  PRINT COLUMN 01, lr_reg.idboletaing USING "###,##&", 
        COLUMN 15, cproductor, 
        COLUMN 40, lcantcajas USING "##,##&", 
        COLUMN 55, lpesototal USING "##,##&", 
        COLUMN 70, lfecha,
        COLUMN 85, lEstructura
  LET lproductor = NULL 
  LET lpesototal = NULL 
  LET lfecha = NULL 
  LET cproductor = NULL 
  LET lcantcajas = NULL }
 

FUNCTION llenatmp()
  
  DEFINE sqlTxt STRING 
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem, vlPeso1, vlPeso2 INTEGER
  DEFINE vestructura, cproductor, citem, vdes VARCHAR (100)
  DEFINE pesoSaldo SMALLINT  

  DELETE FROM ordenes WHERE 1=1
  --Para boletas locales
  LET sqlTxt = " SELECT idboletaing, idordpro, iddordpro ",
               " FROM pemmboleta ",
               " WHERE tipoBoleta = 1 ",
               " AND estado IN (1,2,3) "

  {CASE 
    WHEN laccion = "I" --Ingreso
      LET sqlTxt = sqlTxt || " AND estado = 1 "
    WHEN laccion = "B" --Busqueda
      LET sqlTxt = sqlTxt || " AND estado IN (1,2) "
  END CASE}
  PREPARE ex_bolLoc FROM sqlTxt  
  DECLARE cur111 CURSOR FOR ex_bolLoc

    FOREACH cur111 INTO vBol, vordpro, vdordpro
      SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || desItemLg
        INTO vestructura
        FROM pemdordpro a, pemmfinca b, mestructura c, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
         AND a.idestructura = c.idEstructura 
         AND a.idValvula = d.idValvula 
         AND a.idItem = e.idItem 
         AND a.idOrdPro = vordpro 
         AND a.iddordpro = vdordpro
         INSERT INTO ordenes VALUES (vBol, vestructura)
    END FOREACH 

    --Para productor
    LET sqlTxt = " SELECT idboletaing, productor, iditem ",
               " FROM pemmboleta ",
               " WHERE tipoBoleta = 2 ",
               " AND estado IN (1,2) "
               

  PREPARE ex_bolProd FROM sqlTxt
    DECLARE cur02 CURSOR FOR ex_bolProd

    FOREACH cur02 INTO vBol, vproductor, vitem
      SELECT trim(nombre)
        INTO cProductor 
        FROM commemp 
       WHERE id_commemp = vproductor
              --DISPLAY vProductor TO productor
    
      --Para producto / item
      SELECT desItemLg
        INTO cItem 
        FROM glbMItems
       WHERE idItem = vitem
              --DISPLAY vItem TO producto
      LET vdes = cproductor CLIPPED, "-",citem
      INSERT INTO ordenes VALUES (vBol, vdes)
    END FOREACH 

    --Borrando las boletas que tengan ingreso en exportacion y ya no tengan saldo
    DECLARE cur03 CURSOR FOR SELECT idOrden FROM ordenes
    FOREACH cur03 INTO vBol
      SELECT FIRST 1 idBoletaing FROM pemmboletaexp WHERE idboletaing = vBol
      IF sqlca.sqlcode = 0 THEN
         SELECT SUM (pesoTotal) INTO vlPeso1
         FROM pemmboletaexp
         WHERE idboletaing = vBol
         GROUP BY idBoletaIng
         SELECT a.pesoTotal INTO vlPeso2 
         FROM pemmboleta a
         WHERE a.idboletaing = vBol
         LET pesoSaldo = vlPeso2 - vlPeso1
         IF pesoSaldo <= 0 THEN
            DELETE FROM ordenes WHERE idOrden = vBol
            IF sqlca.sqlcode = 0 THEN
               DISPLAY "borre ", vBol 
            END IF  
         END IF  
      END IF 
    END FOREACH  
END FUNCTION 
FUNCTION creatmp()

    {CREATE TEMP TABLE tmpdet2
  (idcliente    INTEGER, 
   lnkItem      INTEGER ,
   cantcajas    SMALLINT , 
   peso         SMALLINT,
   pesoTotal    SMALLINT,
   PRIMARY KEY (idcliente, lnkitem) )}


  CREATE TEMP TABLE ordenes
  (
  idorden   INTEGER ,
  descripcion   VARCHAR (100)
  )
END FUNCTION 