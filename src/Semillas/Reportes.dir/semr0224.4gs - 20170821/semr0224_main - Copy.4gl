DATABASE db0001

DEFINE fecIni, fecFin DATE 
DEFINE gr_reg RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor
   
END RECORD

MAIN
  DEFINE prog_name2 STRING 

  DEFER INTERRUPT 

  LET prog_name2 = "semr0224.log"   -- El progr_name es definido como constante en el arch. globals
    
  CALL STARTLOG(prog_name2)
  CALL main_init()

END MAIN 

FUNCTION main_init()

  CALL ui.Interface.loadActionDefaults("actiondefaults")
  CALL ui.Interface.loadStyles("styles_sc")

  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "semr0224_form1"

  CALL fgl_settitle("ANDE - Reporte de boletas de Recepción")
  
MENU "Boletas de Recepción"
  BEFORE MENU 
    CALL pideparam()
    EXIT MENU 
    
  ON ACTION generar
     CALL pideParam()
  ON ACTION salir
    EXIT MENU 
END MENU 

END FUNCTION 

FUNCTION pideParam()
  DEFINE sqlTxt STRING 
  DEFINE vestructura, cproductor, citem, citem1, vdes VARCHAR (100)
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem INTEGER

  
  INPUT BY NAME fecIni, fecFin
    BEFORE INPUT 
      LET fecIni = "25102014"
      LET fecFin = "06112014"
      EXIT INPUT 
  END INPUT 
  
  IF INT_FLAG THEN 
     CALL msg("Reporte cancelado por el usuario")
     RETURN 
  END IF 

  --Boletas
  LET sqlTxt = " SELECT idboletaing, tipoBoleta, idOrdPro, idDOrdPro, productor ",
               " FROM pemmboleta ",
               " WHERE estado in (1,2) AND fecha BETWEEN '", fecIni, "'",
               " AND '", fecFin, "'"
  PREPARE ex_sql1 FROM sqlTxt
  DECLARE cur01 CURSOR FOR ex_sql1

  START REPORT repBolIng 
  
  FOREACH cur01 INTO gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     gr_reg.idDOrdPro, gr_reg.productor
    DISPLAY "Boleta ", gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     gr_reg.idDOrdPro, gr_reg.productor
    
    --Para datos de finca/productor
    IF gr_reg.tipoBoleta = 1 THEN
       --Para estructura
       SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || nombre || '-' || desItemLg 
       INTO vEstructura
       FROM pemdordpro a, pemmfinca b, mestructura c, glbmtip f, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
       AND a.idestructura = c.idEstructura 
       AND a.idValvula = d.idValvula 
       AND a.idItem = e.idItem 
       AND a.idOrdPro = gr_reg.idOrdPro 
       AND a.iddordpro = gr_reg.idDOrdPro
       AND e.idtipo = f.idtipo

       DISPLAY "vestructura ", vEstructura 
       --limpia productor
       LET vProductor = NULL 
       LET vItem = NULL 
       DISPLAY "vProductor ", vProductor
       DISPLAY "vItem ", vItem 
    ELSE 
       --Para productor
       SELECT trim(nombre)
       INTO cProductor 
       FROM commemp 
       WHERE id_commemp = gr_reg.productor
       DISPLAY "vProductor ", cProductor
              
       --Para producto / item
       SELECT nombre
       INTO cItem1
       FROM pemmboleta a, glbmitems b, glbmtip c
       WHERE c.idtipo = b.idtipo
       AND b.iditem = a.iditem
       AND a.idboletaing = gr_reg.idboletaing

       
       SELECT FIRST 1 desItemLg
       INTO cItem 
       FROM glbMItems a, pemmboleta c
       WHERE c.idItem = a.idItem 

       DISPLAY "vItem ", cItem

       --Limpia estructura
       LET vEstructura = cItem1 CLIPPED, "-", cItem 
       DISPLAY "vEstrctura ", vEstructura
    END IF 
    OUTPUT TO REPORT repBolIng(gr_reg.*, vEstructura)
  END FOREACH   

  FINISH REPORT repBolIng  
END FUNCTION 

REPORT repBolIng(lr_reg, lEstructura )
DEFINE lr_reg  RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor
   
END RECORD
DEFINE lproductor LIKE pemmboleta.productor
DEFINE lpesototal LIKE pemmboleta.pesototal
DEFINE lfecha     LIKE pemmboleta.fecha
DEFINE lcantcajas INT 
DEFINE cproductor LIKE commemp.nombre
DEFINE lEstructura VARCHAR (100)

FORMAT 

PAGE HEADER 
  PRINT COLUMN 01, "RESUMEN BOLETAS DE RECEPCIÓN"
  PRINT COLUMN 01, "Fecha inicial: ", fecini
  PRINT COLUMN 01, "Fecha final:   ", fecfin
  SKIP 1 LINE 
  PRINT COLUMN 01, "ID Boleta", 
        COLUMN 15, "Productor",
        COLUMN 40, "Cant Cajas",
        COLUMN 55, "Peso Lbs",
        COLUMN 70, "Fecha",
        COLUMN 85, "Estructura"

  PRINT COLUMN 01, "---------", 
        COLUMN 15, "----------",
        COLUMN 40, "----------",
        COLUMN 55, "----------",
        COLUMN 70, "----------",
        COLUMN 85, "-----------------------------"
        
        
ON EVERY ROW 
  --Productor, pesoLbs, fecha
  SELECT productor, pesoTotal, fecha
  INTO lproductor, lpesototal, lfecha
  FROM pemmboleta
  WHERE idboletaing = lr_reg.idboletaing

  SELECT nombre INTO cproductor 
  FROM commemp 
  WHERE id_commemp = lproductor

  --cantCajas
  SELECT SUM (cantCajas) 
  INTO lcantcajas
  FROM pemmboleta a, pemdboleta b
  WHERE a.idboletaing = b.idboletaing
  AND a.idboletaing = lr_reg.idboletaing
      
  PRINT COLUMN 01, lr_reg.idboletaing USING "###,##&", 
        COLUMN 15, cproductor, 
        COLUMN 40, lcantcajas USING "##,##&", 
        COLUMN 55, lpesototal USING "##,##&", 
        COLUMN 70, lfecha,
        COLUMN 85, lEstructura
  LET lproductor = NULL 
  LET lpesototal = NULL 
  LET lfecha = NULL 
  LET cproductor = NULL 
  LET lcantcajas = NULL 
END REPORT 
