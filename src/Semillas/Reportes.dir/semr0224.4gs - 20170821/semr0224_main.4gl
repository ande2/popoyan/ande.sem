DATABASE db0001

DEFINE fecIni, fecFin DATE 
DEFINE gr_reg RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  fecha        LIKE pemmboleta.fecha,
  tipoItem     CHAR (1),
  lnkitem      INTEGER ,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  tipo         LIKE glbmtip.nombre ,
  proveedor    VARCHAR (200),
  finca        VARCHAR (100) , --LIKE pemmfinca.fincanomct,
  mercado      VARCHAR (100),
  producto     VARCHAR (100),
  calidad1     VARCHAR (100) ,
  calidad      VARCHAR (100) ,
  cantCajas        INTEGER ,
  librasxcaja  SMALLINT,
  totalLibras  INTEGER ,
  precioConLocal VARCHAR (100),     
  precioLocal     VARCHAR (100),    
  precioConDol    VARCHAR (100),    
  precioSinDescCalidad  VARCHAR (100), 
  precioConDescCalidad  VARCHAR (100), 
  totalFlete            VARCHAR (100), 
  costoxLibra           VARCHAR (100), 
  porcxLibra            VARCHAR (100), 
  numSemana             SMALLINT ,    
  anio                  SMALLINT ,    
  numEntrada            VARCHAR (100)  
   
END RECORD

DEFINE vCalidad SMALLINT 

MAIN
  DEFINE prog_name2 STRING 

  DEFER INTERRUPT 

  LET prog_name2 = "semr0224.log"   -- El progr_name es definido como constante en el arch. globals
    
  CALL STARTLOG(prog_name2)
  CALL main_init()

END MAIN 

FUNCTION main_init()

  CALL ui.Interface.loadActionDefaults("actiondefaults")
  CALL ui.Interface.loadStyles("styles_sc")

  CLOSE WINDOW SCREEN
  OPEN WINDOW w1 WITH FORM "semr0224_form1"

  CALL fgl_settitle("ANDE - Reporte de boletas de Recepción")
  
MENU "Boletas de Recepción"
  BEFORE MENU 
    CALL pideparam()
    EXIT MENU 
    
  ON ACTION generar
     CALL pideParam()
  ON ACTION salir
    EXIT MENU 
END MENU 

END FUNCTION 

FUNCTION pideParam()
  DEFINE sqlTxt STRING 
  DEFINE vestructura, cproductor, citem, citem1, vdes VARCHAR (100)
  DEFINE vBol, vordpro, vdordpro, vproductor, vitem INTEGER
  DEFINE myHandler om.SaxDocumentHandler
  
  INPUT BY NAME fecIni, fecFin
    {BEFORE INPUT 
      LET fecIni = "01102014"
      LET fecFin = "06112014"
      EXIT INPUT }
    AFTER FIELD fecIni
      IF fecIni IS NULL THEN
         CALL msg("Ingrese fecha de inicio")
         NEXT FIELD fecIni
      END IF

   AFTER FIELD fecFin
      IF fecFin IS NULL THEN
         CALL msg("Ingrese fecha final")
         NEXT FIELD fecFin
      END IF      
  END INPUT 
  
  IF INT_FLAG THEN 
     CALL msg("Reporte cancelado por el usuario")
     RETURN 
  END IF 

  --Boletas
  {LET sqlTxt = " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'E', t3.lnkitem, t3.cantCajas, t3.pesototal ",
               " FROM pemmboleta t1, pemmboletaexp t2, pemdboletaexp t3 ", 
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaexp = t3.idboletaexp ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'L', t3.lnkitem, t3.cantCajas, t3.pesoneto ",
               " FROM pemmboleta t1, pemmboletaloc t2, pemdboletaloc t3 ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaloc = t3.idboletaloc ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " ORDER BY 1 "}

  LET sqlTxt = " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'E', t3.lnkitem, t3.cantCajas, t3.pesototal, ' ' ", 
               " FROM pemmboleta t1, pemmboletaexp t2, pemdboletaexp t3 ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaexp = t3.idboletaexp ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT t1.idboletaing, t1.tipoboleta, t1.fecha, 'L', t3.lnkitem, t3.cantCajas, t3.pesoneto, ' ' ", 
               " FROM pemmboleta t1, pemmboletaloc t2, pemdboletaloc t3 ",
               " WHERE t1.idboletaing = t2.idboletaing ",
               " AND t2.idboletaloc = t3.idboletaloc ",
               " AND t1.estado = 0 ",
               " AND fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT b.idboletaing, b.tipoboleta, b.fecha, 'E', 1, 0, c.merma, a.nombre ", 
               " FROM itemsxgrupo a, pemmboleta b, pemmboletacau c ",
               " WHERE a.nombre LIKE '%MERMA%' ",
               " AND a.iditem = b.iditem ",
               --" AND b.idboletaing = 1 ", lr_reg.idboletaing ,
               " AND b.idboletaing = c.idboletaing ",
               " AND b.fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
               " UNION ",
               " SELECT b.idboletaing, b.tipoboleta, b.fecha, 'E', 1, 0, c.merma, i.nombre ", 
               " FROM itemsxgrupo i, pemmboleta b, pemdordpro d, pemmboletacau c ",
               " WHERE i.nombre LIKE '%MERMA%' ",
               " AND b.idordpro = d.idordpro ", 
               " AND b.iddordpro = d.iddordpro ",
               " AND d.iditem = i.iditem ",
               " AND b.fecha BETWEEN '", fecIni, "'", " AND '", fecFin, "'",
                --AND b.idboletaing = 1 --", lr_reg.idboletaing
               " AND b.idboletaing = c.idboletaing ",
               " ORDER BY 1 "
--DISPLAY sqlTxt

               
  {LET sqlTxt = " SELECT idboletaing, tipoBoleta, idOrdPro, idDOrdPro, productor, fecha ",
               " FROM pemmboleta ",
               " WHERE estado in (1,2) AND fecha BETWEEN '", fecIni, "'",
               " AND '", fecFin, "'"}
  PREPARE ex_sql1 FROM sqlTxt
  DECLARE cur01 CURSOR FOR ex_sql1
  LET myHandler = gral_reporte_all("carta","horizontal","XLS",132,"con_r_08")
  START REPORT repBolIng 
  
  FOREACH cur01 INTO gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.fecha, 
          gr_reg.tipoItem, gr_reg.lnkitem, gr_reg.cantCajas, gr_reg.totalLibras, gr_reg.calidad1 -- gr_reg.tipoBoleta, gr_reg.idOrdPro,
                     --gr_reg.idDOrdPro, gr_reg.productor, gr_reg.fecha
    --DISPLAY "Boleta ", gr_reg.idboletaing, gr_reg.tipoBoleta, gr_reg.idOrdPro,
      --               gr_reg.idDOrdPro, gr_reg.productor
    
    --Para datos de finca/productor
    SELECT idordpro, iddordpro INTO gr_reg.idOrdPro, gr_reg.idDOrdPro FROM pemmboleta WHERE idboletaing = gr_reg.idboletaing
    IF gr_reg.tipoBoleta = 1 THEN
       --Para estructura
       SELECT fincaNomCt || '-'||nomEstructura || '-'|| NVL(nomValvula,'') || '-' || nombre || '-' || desItemLg 
       --INTO vEstructura
       INTO gr_reg.proveedor
       FROM pemdordpro a, pemmfinca b, mestructura c, glbmtip f, OUTER(pemMValvula d), glbMItems e 
       WHERE a.idfinca = b.idfinca 
       AND a.idestructura = c.idEstructura 
       AND a.idValvula = d.idValvula 
       AND a.idItem = e.idItem 
       AND a.idOrdPro = gr_reg.idOrdPro 
       AND a.iddordpro = gr_reg.idDOrdPro
       AND e.idtipo = f.idtipo

       --DISPLAY "vestructura ", vEstructura 
       --limpia productor
       LET vProductor = NULL 
       LET vItem = NULL 
       --DISPLAY "vProductor ", vProductor
       --DISPLAY "vItem ", vItem 
       
       --Para tipo
       SELECT t1.nombre
       INTO gr_reg.tipo
       FROM glbMTip t1, glbmitems t2, pemdordpro t3, pemmboleta t4
       WHERE t1.idtipo = t2.idtipo
       AND t2.iditem = t3.iditem
       AND t3.idordpro = t4.idordpro
       AND t3.iddordpro = t4.iddordpro
       AND t4.idboletaing = gr_reg.idboletaing

       --Para finca
       SELECT t1.fincanomct
         INTO gr_reg.finca
         FROM pemmfinca t1, pemdordpro t2, pemmboleta t3
         WHERE t1.idfinca = t2.idfinca
         AND t2.idordpro = t3.idordpro
         AND t2.iddordpro = t3.iddordpro
         AND t3.idboletaing = 3

       --Para producto
       SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg
         INTO gr_reg.producto 
         FROM glbMItems a, glbmfam b, glbmtip c  
        WHERE a.idfamilia = b.idfamilia 
          AND a.idtipo = c.idtipo
          AND a.iditem = 
      (SELECT e.iditem 
        FROM pemmboleta d, pemdordpro e  
       WHERE d.idordpro = e.idordpro 
         AND d.iddordpro = e.iddordpro
         AND d.idboletaing = gr_reg.idboletaing)

       --Para numero de entrada
       LET gr_reg.numEntrada = NULL 
       --SELECT t1.numentradainvsap
       SELECT t1.numreciboprosap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing

       --Para precios
       {LET gr_reg.precioConLocal = NULL 
       LET gr_reg.precioLocal = NULL 
       SELECT t1.costoart INTO gr_reg.precioConDol FROM pemddatossap t1 
       WHERE t1.idboletaing = gr_reg.idboletaing AND t1.lnkitem = gr_reg.lnkitem}
    ELSE 
       --Para productor
       SELECT trim(t1.nombre)
       --INTO cProductor
       INTO gr_reg.proveedor 
       FROM commemp t1, pemmboleta t2  
       WHERE t1.id_commemp = t2.productor
       AND t2.idboletaing = gr_reg.idboletaing
       --DISPLAY "Proveedor ", gr_reg.proveedor
              
       --Para producto / item
       SELECT nombre
       INTO cItem1
       FROM pemmboleta a, glbmitems b, glbmtip c
       WHERE c.idtipo = b.idtipo
       AND b.iditem = a.iditem
       AND a.idboletaing = gr_reg.idboletaing

       SELECT FIRST 1 desItemLg
       INTO cItem 
       FROM glbMItems a, pemmboleta c
       WHERE c.idItem = a.idItem 

       --DISPLAY "vItem ", cItem

       --Limpia estructura
       LET vEstructura = cItem1 CLIPPED, "-", cItem 
       --DISPLAY "vEstrctura ", vEstructura

       --Para tipo
       SELECT t1.nombre
       INTO gr_reg.tipo
       FROM glbMTip t1, glbmitems t2, pemmboleta t4
       WHERE t1.idtipo = t2.idtipo
       AND t2.iditem = t4.iditem
       AND t4.idboletaing = gr_reg.idboletaing

       --Para finca
       {SELECT t1.nombre
         INTO gr_reg.finca
         FROM commemp t1,pemmboleta t3
         WHERE t3.productor = t1.id_commemp
         AND t3.idboletaing = gr_reg.idboletaing}
       LET gr_reg.finca = "Productor"  

       --Para producto
       SELECT trim (b.nombre)||" "|| trim( c.nombre)|| " " || a.desItemLg
         INTO gr_reg.producto 
         FROM glbMItems a, glbmfam b, glbmtip c  
        WHERE a.idfamilia = b.idfamilia 
          AND a.idtipo = c.idtipo
          AND a.iditem = 
      (SELECT d.iditem 
        FROM pemmboleta d  
       WHERE d.idboletaing = gr_reg.idboletaing)

       --Para numero de entrada
       LET gr_reg.numEntrada = NULL
       SELECT t1.numentmercexpsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing
       IF gr_reg.numEntrada IS NULL THEN 
          SELECT t1.numentmerclocsap
         INTO gr_reg.numEntrada
         FROM pemmdatossap t1 
         WHERE t1.idboletaing = gr_reg.idboletaing
       END IF   

       --Para precios
       {LET gr_reg.precioConDol = NULL 
       LET gr_reg.precioConLocal = NULL 
       LET gr_reg.precioLocal = NULL 
       SELECT t1.costoart INTO gr_reg.precioConDol FROM pemddatossap t1 
       WHERE t1.idboletaing = gr_reg.idboletaing AND t1.lnkitem = gr_reg.lnkitem}  
    END IF

    --Separa condiciones para Items de Exportación y Locales
    IF gr_reg.tipoItem = "E" THEN --Exportacion
      --Para mercado
      LET gr_reg.mercado = "Semillas del Campo - Exportación"
      --Para Calidad
      LET gr_reg.calidad = "Aprovechamiento"

    ELSE --Locales  
      --Para mercado
      LET gr_reg.mercado = "Semillas del Campo - Mercado Local"

      --Para Calidad
      SELECT a.calidad INTO vCalidad FROM pemdboletaloc a, pemmboletaloc b, pemmboleta c
      WHERE a.idboletaloc = b.idboletaloc AND b.idboletaing = c.idboletaing
      AND a.lnkitem = gr_reg.lnkitem
      AND c.idboletaing = gr_reg.idboletaing

      CASE vCalidad
        WHEN 1 LET gr_reg.calidad = "Aprovechamiento"
        WHEN 2 LET gr_reg.calidad = "Mediano"
        WHEN 3 LET gr_reg.calidad = "Devolución"
      END CASE 
    END IF 

    --Para Calidad1
    --DISPLAY "calidad1 ", gr_reg.calidad1
       IF gr_reg.calidad1 = ' ' THEN 
          SELECT nombre, peso
          INTO gr_reg.calidad1, gr_reg.librasxcaja 
          FROM itemsxgrupo
          WHERE lnkitem = gr_reg.lnkitem
       END IF 
       IF gr_reg.calidad1 LIKE '%MERMA%' THEN LET gr_reg.librasxcaja = 0 END IF
--DISPLAY "boleta - item ", gr_reg.idboletaing, "-",gr_reg.lnkitem, "-",gr_reg.producto 

    --Para semana
      SELECT tb1.sem_com --idsem 
      INTO gr_reg.numSemana
      FROM  dsemanacal tb1, msemanas tb2
      WHERE tb1.lnksem = tb2.lnksem
      AND tb2.periodoactivo = 1 
      AND gr_reg.fecha >= tb1.fec_ini
      AND  gr_reg.fecha <= tb1.fec_fin


    
    OUTPUT TO REPORT repBolIng(gr_reg.*, vEstructura)
    
  END FOREACH   

  FINISH REPORT repBolIng  
  CALL fgl_report_stopGraphicalCompatibilityMode()
END FUNCTION 

REPORT repBolIng(lr_reg, lEstructura )
DEFINE lr_reg  RECORD
  idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  fecha        LIKE pemmboleta.fecha,
  tipoItem     CHAR (1),
  lnkitem      INTEGER ,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  tipo         LIKE glbmtip.nombre ,
  
  proveedor    VARCHAR (200),
  finca        VARCHAR (100), --LIKE pemmfinca.fincanomct,
  mercado      VARCHAR (100),
  producto     VARCHAR (100),
  calidad1     VARCHAR (100) ,
  calidad      VARCHAR (100) ,
  cantCajas        INTEGER,
  librasxcaja  SMALLINT,
  totalLibras  INTEGER ,
  precioConLocal VARCHAR (100),     
  precioLocal     VARCHAR (100),    
  precioConDol    VARCHAR (100),    
  precioSinDescCalidad  VARCHAR (100), 
  precioConDescCalidad  VARCHAR (100), 
  totalFlete            VARCHAR (100), 
  costoxLibra           VARCHAR (100), 
  porcxLibra            VARCHAR (100), 
  numSemana             SMALLINT ,    
  anio                  SMALLINT ,    
  numEntrada            VARCHAR (100) 
  
  {idboletaing LIKE pemmboleta.idboletaing,
  tipoBoleta  LIKE pemmboleta.tipoboleta,
  idOrdPro    LIKE pemmboleta.idordpro,
  idDOrdPro   LIKE pemmboleta.iddordpro,
  productor   LIKE pemmboleta.productor,
  tipo         LIKE glbmtip.nombre,
  fecha        LIKE pemmboleta.fecha,
  proveedor    VARCHAR (200),
  finca        LIKE pemmfinca.fincanomct} 
   
END RECORD
DEFINE lproductor LIKE pemmboleta.productor
DEFINE lpesototal LIKE pemmboleta.pesototal
DEFINE lfecha     LIKE pemmboleta.fecha
DEFINE lcantcajas INT 
DEFINE cproductor LIKE commemp.nombre
DEFINE lEstructura VARCHAR (100)

FORMAT 

PAGE HEADER 
  PRINT COLUMN 01, "RESUMEN BOLETAS DE RECEPCIÓN"
  PRINT COLUMN 01, "Fecha inicial: ", fecini
  PRINT COLUMN 01, "Fecha final:   ", fecfin
  PRINT COLUMN 01, "."
  
  PRINT COLUMN 01, "Tipo",
        COLUMN 10, "Fecha", 
        COLUMN 20, "No. Recepción", 
        COLUMN 30, "Proveedor",
        COLUMN 60, "Finca",
        COLUMN 80, "Mercado",
        COLUMN 130, "Producto",
        COLUMN 160, "Calidad 1",
        COLUMN 200, "Calidad",
        COLUMN 220, "Cajas",
        COLUMN 230, "Libras por Caja",
        COLUMN 250, "Total Libras",
        COLUMN 270, "Precio \nConsignacion \nLocal",
        COLUMN 290, "Precio Local",
        COLUMN 310, "Precio \nConsignación $",
        COLUMN 330, "Precio sin \ndescuento de \nCalidad $",
        COLUMN 350, "Precio con \ndescuento de \nCalidad $",
        COLUMN 370, "Total Flete",
        COLUMN 390, "Costo x Libra",
        COLUMN 410, "6% x Libra",
        COLUMN 430, "Semana",
        COLUMN 450, "Año",
        COLUMN 470, "Emo SAP"
        
        {COLUMN 50, "Cant Cajas",
        COLUMN 60, "Peso Lbs",
        COLUMN 70, "Estructura"}
        
ON EVERY ROW 
  --Productor, pesoLbs, fecha
  SELECT productor, pesoTotal
  INTO lproductor, lpesototal
  FROM pemmboleta
  WHERE idboletaing = lr_reg.idboletaing

  {SELECT nombre INTO cproductor 
  FROM commemp 
  WHERE id_commemp = lproductor}

  --cantCajas
  SELECT SUM (cantCajas) 
  INTO lcantcajas
  FROM pemmboleta a, pemdboleta b
  WHERE a.idboletaing = b.idboletaing
  AND a.idboletaing = lr_reg.idboletaing
      
  PRINT COLUMN 01, lr_reg.tipo , 
        COLUMN 10, lr_reg.fecha, 
        COLUMN 20, lr_reg.idboletaing USING "###,##&", 
        COLUMN 30, lr_reg.proveedor,
        COLUMN 60, lr_reg.finca,
        COLUMN 80, lr_reg.mercado,
        COLUMN 130, lr_reg.producto ,
        COLUMN 160, lr_reg.calidad1,
        COLUMN 200, lr_reg.calidad ,
        COLUMN 220, lr_reg.cantCajas,
        COLUMN 230, lr_reg.librasxcaja,
        COLUMN 250, lr_reg.totalLibras,
        COLUMN 270, lr_reg.precioConLocal, 
        COLUMN 270, lr_reg.precioLocal,
        COLUMN 310, lr_reg.precioConDol,
        COLUMN 430, lr_reg.numSemana,
        COLUMN 440, YEAR (lr_reg.fecha) USING "####",
        COLUMN 460, lr_reg.numEntrada CLIPPED 
        {COLUMN 50, lcantcajas USING "##,##&", 
        COLUMN 60, lpesototal USING "##,##&", 
        COLUMN 70, lEstructura}
  LET lproductor = NULL 
  LET lpesototal = NULL 
  LET lfecha = NULL 
  LET cproductor = NULL 
  LET lcantcajas = NULL 
END REPORT 
