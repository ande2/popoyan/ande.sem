################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
# Para probar SVN
#
################################################################################
SCHEMA db0001

DEFINE resp INTEGER
DEFINE i SMALLINT 
DEFINE grRep RECORD LIKE pemMBoleta.*
DEFINE pLine VARCHAR(255)

MAIN
  
  DATABASE db0001 

  PROMPT "Boleta? " FOR resp
  START REPORT rRep
  CALL report1()
  --SKIP 5 LINES
  FOR i = 1 TO  8
     OUTPUT TO REPORT rRep(grRep.*,pLine)
  END FOR 
  CALL report1()
  FINISH REPORT rRep
END MAIN 

FUNCTION report1()  
  
  DEFINE vcoditem SMALLINT 
  DEFINE vnomItem VARCHAR(50)
  DEFINE vTipoCaja VARCHAR(50)
  
  DEFINE pos, vSumCajas SMALLINT 
  DEFINE vEmpresa LIKE ande:commemp.nombre
  DEFINE lDbname CHAR(6)
  DEFINE lCodigo VARCHAR(30) 
  DEFINE vUsuario char(10)
  DEFINE lVersion VARCHAR(30)
  DEFINE vEstructura VARCHAR(50)
  DEFINE vproductor VARCHAR(50) 
  
  --DECLARE curRep CURSOR FOR 
      SELECT * 
        INTO grRep.* 
        FROM pemMBoleta
       WHERE idBoletaIng = resp

   --Para empresa
    LET lDbname = arg_val(1)
    SELECT nombre INTO vEmpresa FROM ande:commemp a WHERE dbname = lDbname
    LET pLine[1,30]  = vEmpresa
    LET pLine[75,100] = "HOJA No.: 1" --, PAGENO USING "&&"
    DISPLAY "pLine ", pLine
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""
    
    --Código
    SELECT valchr 
      INTO lCodigo
      FROM glb_paramtrs
      WHERE nompar = "RECEPCIÓN DE FRUTA - CODIGO";  
    LET pLine[1,29]  = "FECHA   : ", TODAY USING "dd/mm/yyyy"
    LET pLine[30,74] = "REGISTRO DE RECEPCIÓN DE FRUTA No. ", grRep.idboletaing USING "<<<,<<&"
    LET pLine[75,99] = "CÓDIGO  : ", lCodigo 
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""

    --Para usuario
    LET vUsuario = get_AndeVarLog("LOGNAME")
    IF vUsuario IS NULL THEN
       LET vUsuario = "prueba"
    END IF 
    
   --Version
   SELECT valchr 
      INTO lVersion
      FROM glb_paramtrs
      WHERE nompar = "RECEPCION DE FRUTA - VERSION"

    LET pLine[1,30]  = "USUARIO: ", vUsuario  
    LET pLine[75,99] = "VERSIÓN : ", lVersion
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""
          
    LET pLine = "============================================================================================="
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""
    OUTPUT TO REPORT rRep(grRep.*,pLine)  

    LET pLine[1,50] = "INFORMACIÓN DE RECEPCIÓN"
    OUTPUT TO REPORT rRep(grRep.*,pLine)  
    LET pLine = ""

    IF grRep.tipoboleta = 1 THEN 
       --Para estructura
       SELECT trim(numordpro)||'-'||trim(nomordpro) INTO vEstructura
       FROM pemmordpro WHERE idordpro = grRep.iddordpro
       LET pLine[1,50] = "Estructura : ", vEstructura
       OUTPUT TO REPORT rRep(grRep.*,pLine)  
       LET pLine = ""
       
       LET pLine[1,50] = "Fecha      : ", date(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)  
       LET pLine = ""
       
       LET pLine[1,50] = "Hora       : ", time(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)  
       LET pLine = ""
       
       LET pLine[1,50] = "# Documento: ", grRep.docpropio
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       
    ELSE 
       SELECT trim(nombre) INTO vProductor FROM ande:commemp WHERE id_commemp = grRep.productor
       LET pLine[1,50] = "Productor  : ", vProductor
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       LET pLine[1,50] = "Fecha      : ", date(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       LET pLine[1,50] = "Hora       : ", time(grRep.fectran)
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
       LET pLine[1,50] = "# Documento: ", grRep.docproductor
       OUTPUT TO REPORT rRep(grRep.*,pLine)
       LET pLine = ""
    END IF 
   OUTPUT TO REPORT rRep(grRep.*,pLine)

   LET pLine[37,60] = "INFORMACIÓN DEL PRODUCTO"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
   
   LET pLine[37,60] = "========================"
   
   LET pLine = ""
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   
   LET pLine[1,10]  = "CÓDIGO" 
   LET pLine[15,35] = "PRODUCTO"
   LET pLine[40,55] = "TIPO CAJA"
   LET pLine[60,74] = "CANT CAJAS"
   LET pLine[75,99] = "PESO EN LBS"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
   
   LET pLine[1,10]  = "------" 
   LET pLine[15,35] = "-----------------------"
   LET pLine[40,55] = "--------------"
   LET pLIne[60,74] = "----------"
   LET pLine[75,99] = "-----------"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
   
   --LET pos = 1

   --Para codigo y nombre de item
   IF grRep.tipoboleta = 1 THEN 
          
      SELECT c.iditem, c.desitemlg
        INTO vCodItem, vNomItem
        FROM  pemmboleta a, pemDOrdPro b, glbmitems c
       WHERE a.idordpro = b.idordpro
         AND a.iddordpro = b.iddordpro
         AND b.iditem = c.iditem
         AND a.idboletaing = grRep.idboletaing
          
       LET pLine[1,14]  = vCodItem
       LET pLIne[15,39] = vNomItem
          
   ELSE
       SELECT b.idItem, b.desItemLg
         INTO vCodItem, vNomItem
         FROM pemmboleta a, glbMItems b
        WHERE a.idItem = b.idItem

       LET pLine[1,14]  = vCodItem
       LET pLIne[15,39] = vNomItem
   END IF
   --Para Tipo de Caja
   SELECT b.nomcatempaque
     INTO vTipoCaja
     FROM pemMBoleta a, pemmcatemp b
    WHERE a.tipocaja = b.idcatempaque
      AND  a.idBoletaIng = grRep.idboletaing
   LET pLine[40,59] = vTipoCaja    
   --Total de cajas
   SELECT SUM(cantCajas) 
     INTO vSumCajas 
     FROM pemDBoleta
    WHERE idBoletaIng = grRep.idboletaing
   LET pLine[65,70] = vSumCajas

   --Peso total
   LET pLine[78,90] = grRep.pesototal
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""

      --SKIP 5 LINES
   FOR i = 1 TO  5
      OUTPUT TO REPORT rRep(grRep.*,pLine)
   END FOR 

   LET pLine[45,75] = "---------------"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
      
   LET pLine[50,75] = "Firma"
   OUTPUT TO REPORT rRep(grRep.*,pLine)
   LET pLine = ""
    
END FUNCTION 


REPORT rRep(lrRep,lLine)

DEFINE lrRep RECORD LIKE pemMBoleta.*
DEFINE lLine VARCHAR(255)

FORMAT 
    
    ON EVERY ROW 
       PRINT COLUMN 001, lLine

END REPORT 




