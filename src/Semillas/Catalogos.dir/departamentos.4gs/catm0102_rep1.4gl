SCHEMA db0001

DEFINE gr_commdep RECORD
  id_commdep  LIKE commdep.id_commdep,
  descripcion LIKE commdep.descripcion,
  hijo_de     LIKE commdep.hijo_de
END RECORD 

{MAIN

  MENU "Reporte"
    COMMAND "Generar"
      DATABASE db0001 
      CALL genera()
    COMMAND "Salir"
      EXIT MENU 
  END MENU 

END MAIN} 

FUNCTION repDepto()
  DEFINE pos SMALLINT
  DEFINE ant LIKE commdep.hijo_de
  --DEFINE pLine VARCHAR(100)
  DEFINE pLine VARCHAR(100)
  
  START REPORT rDeptos
  DECLARE ccursor CURSOR FOR 
      SELECT id_commdep, descripcion, hijo_de 
        FROM commdep 
       WHERE estado = 1 
         AND id_commdep > 0 
    ORDER BY hijo_de

    LET pos = 12
    LET ant = 0  
    FOREACH ccursor INTO gr_commdep.*
      LET pLine = gr_commdep.id_commdep
      
      IF gr_commdep.hijo_de <> ant THEN
         LET ant = gr_commdep.hijo_de
         LET pos = pos + 3
         LET pLine[pos,100] = gr_commdep.descripcion
      ELSE 
         LET pLine[pos,100] = gr_commdep.descripcion
      END IF 
      --OUTPUT TO REPORT rDeptos(gr_commdep.*)   
      OUTPUT TO REPORT rDeptos(gr_commdep.*, pLine)   
    END FOREACH

    FINISH REPORT rDeptos
    
END FUNCTION 

REPORT rDeptos(lr_commdep,lLine)
  DEFINE lr_commdep RECORD 
    id_commdep  LIKE commdep.id_commdep,
    descripcion LIKE commdep.descripcion,
    hijo_de     LIKE commdep.hijo_de

  END RECORD 
  DEFINE lLine VARCHAR(100)

DEFINE vEmpresa LIKE commemp.nombre
DEFINE vUsuario VARCHAR(50)

FORMAT 

  PAGE HEADER 
    --Para empresa
    LET vEmpresa = "SELECT nombre FROM ande:commemp WHERE id_commemp = 1"
    PREPARE ex_vEmpresa FROM vEmpresa
    EXECUTE ex_vEmpresa INTO vEmpresa
    PRINT COLUMN 001, vEmpresa,
          COLUMN 075, "FECHA   : ", TODAY USING "dd/mm/yyyy"

    PRINT COLUMN 025, "REPORTE DE ORGANIZACIÓN - DEPARTAMENTOS"

    --Para usuario
    LET vUsuario = "SELECT UNIQUE USER FROM systables WHERE tabid = 99"
    PREPARE ex_vUsuario FROM vUsuario 
    EXECUTE ex_vUsuario INTO vUsuario 
    PRINT COLUMN 001, "USUARIO: ", vUsuario,
          COLUMN 075, "HOJA No.: ", PAGENO USING "&&"
    PRINT COLUMN 000, "=============================================================================================="
    SKIP 2 LINES 

    PRINT COLUMN 001, " CODIGO", 
          COLUMN 015, "DESCRIPCIÓN"
    PRINT COLUMN 001, "--------",
          COLUMN 015, "--------------------------"
    --SKIP 1 LINES 

  ON EVERY ROW 
    --PRINT COLUMN 004, lr_commdep.id_commdep USING "&&&",
    --      COLUMN 015, lr_commdep.descripcion
    PRINT COLUMN 001, lLine
END REPORT 