################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idPais        LIKE paises.idpais,
      codigo        LIKE paises.codigo,
      nombre        LIKE paises.nombre
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "pemm0236"
END GLOBALS