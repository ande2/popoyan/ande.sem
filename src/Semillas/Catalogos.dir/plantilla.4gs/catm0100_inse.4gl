################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0002_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO bautizo ( ",
        " idBautizo, librob, foliob, partidab, nombre, sexo1, dia1, ",
        " mes1, ano1, dia2, mes2, ano2, sexo2, papa, mama, ",
        " padrino1, padrino2, notam, dia3, mes3, ano3 ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql

END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   LET g_reg.idBautizo = 0
   TRY 
      EXECUTE st_insertar USING 
         g_reg.idBautizo, g_reg.librob, g_reg.foliob, g_reg.partidab,
         g_reg.nombre, g_reg.sexo1, g_reg.dia1,
         g_reg.mes1, g_reg.ano1, g_reg.dia2, 
         g_reg.mes2, g_reg.ano2, g_reg.sexo2,
         g_reg.papa, g_reg.mama, g_reg.padrino1,
         g_reg.padrino2, g_reg.notam, g_reg.dia3,
         g_reg.mes3, g_reg.ano3
      --CS agregarlo cuando se agregue el ID 
      LET g_reg.idBautizo = SQLCA.sqlerrd[2]
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   RETURN TRUE 
END FUNCTION 