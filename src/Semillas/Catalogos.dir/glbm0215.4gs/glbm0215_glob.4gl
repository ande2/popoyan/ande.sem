################################################################################
# Funcion     : %M%
# nombre      : Catalogo de Tipos
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        nombre de la modificacion
#
################################################################################
--DATABASE comisiones
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      idTipo       LIKE glbmtip.idTipo,
      idFamilia    LIKE glbmtip.idFamilia,
      nombre       LIKE glbmtip.nombre,
      cueAfeFinca  LIKE glbmtip.cueAfeFinca,
      cueAfeProduc LIKE glbmtip.cueafeproduc
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "glbm0215"
END GLOBALS