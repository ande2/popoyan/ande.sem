################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Anderson Garcia 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0222_glob.4gl"

FUNCTION insert_init()
DEFINE strSql STRING 

   LET strSql =
      "INSERT INTO commemp ( ",
        " id_commemp, codigo, nit, nombre, iniciales, direccion, telefono, espropia, estado, dbname ",
        "   ) ",
      " VALUES (?,?,?,?,?,?,?,?,?,?)"

   PREPARE st_insertar FROM strSql


   LET strSql =
      "INSERT INTO itemsxempresa ( ",
        " id_commemp, idItem ",
        "   ) ",
      " VALUES (?,?)"

   PREPARE st_insitemsxempresa FROM strSql
END FUNCTION 

FUNCTION ingreso()
   CALL encabezado("Ingresar")
   --CALL info_usuario()
   IF captura_datos('I') THEN
      RETURN grabar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION grabar()
   DEFINE i SMALLINT 
   
   LET g_reg.id_commemp = 0
   LET g_reg.estado = 1
   TRY 
      EXECUTE st_insertar USING 
            g_reg.id_commemp, g_reg.codigo, g_reg.nit, g_reg.nombre, g_reg.iniciales, 
            g_reg.direccion, g_reg.telefono, g_reg.espropia, g_reg.estado, 
            g_reg.dbname

      --CS agregarlo cuando se agregue el ID 
      LET g_reg.id_commemp = SQLCA.sqlerrd[2]
      IF sqlca.sqlcode = 0 THEN
         FOR i = 1 TO gr_item.getLength() 
            IF gr_item[i].idItem IS NOT NULL THEN 
               EXECUTE st_insitemsxempresa USING 
                 g_reg.id_commemp, gr_item[i].idItem
            END IF      
         END FOR 
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY
   DISPLAY BY NAME g_reg.*
   CALL box_valdato ("Registro agregado")
   --CALL combo_din2("cmbdepartamento","SELECT * FROM commdep WHERE id_commdep > 0")
   RETURN TRUE 
END FUNCTION 