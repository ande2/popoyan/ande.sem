################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar datos de un umdor
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "catm0222_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE commemp ",
      "SET ",
      " codigo      = ?, ",
      " nit         = ?, ",
      " nombre      = ?, ",
      " iniciales   = ?, ",
      " direccion   = ?, ",
      " telefono    = ?, ", 
      " espropia    = ?, ",
      " estado      = ?, ",
      " dbname      = ?  ",
      " WHERE id_commemp = ?"

   PREPARE st_modificar FROM strSql

   LET strSql = 
      "DELETE itemsxempresa ",
      " WHERE id_commemp = ?"

   PREPARE st_delitemsxempresa FROM strSql

   LET strSql =
      "INSERT INTO itemsxempresa ( ",
        " id_commemp, idItem ",
        "   ) ",
      " VALUES (?,?)"

   PREPARE st_insitemsxempresau FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
DEFINE i SMALLINT 

   TRY
      EXECUTE st_modificar USING 
      g_reg.codigo, g_reg.nit, g_reg.nombre, g_reg.iniciales, g_reg.direccion, g_reg.telefono, 
      g_reg.espropia, g_reg.estado, g_reg.dbname,
      u_reg.id_commemp
      IF sqlca.sqlcode = 0 THEN 
      
         EXECUTE st_delitemsxempresa USING u_reg.id_commemp
         IF sqlca.sqlcode = 0 THEN 
            LET i = gr_item.getLength()
            IF gr_item.getLength() > 0 THEN 
            
               FOR i = 1 TO gr_item.getLength() 
                  IF gr_item[i].idItem IS NOT NULL THEN 
                     EXECUTE st_insitemsxempresau USING 
                             g_reg.id_commemp, gr_item[i].idItem
                  ELSE 
                     EXIT FOR  
                  END IF 
                    
               END FOR
            END IF 
      END IF  
      END IF 
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM commemp ",
      "WHERE id_commemp = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
DEFINE vresult SMALLINT 
   --Cuando la tabla es corporativa (creada en ANDE) y necesita validar que 
   -- no se haya usado en las bases de datos de empresas (db0001, db0002, etc)

   
   --IF NOT can_deleteAll("pemmfinca", "id_commemp", g_reg.id_commemp) THEN
      --CALL msg("Empresa no se puede eliminar, ya existe en finca(s)")
      --RETURN FALSE 
   --END IF

   LET vresult = 0 
   SELECT count(*) INTO vresult FROM db0001:pemmfinca WHERE id_commemp = g_reg.id_commemp
   IF vresult > 1 THEN
      CALL msg("Empresa no se puede eliminar, ya existe en finca(s)")
      RETURN FALSE
   END IF   
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.id_commemp
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 

