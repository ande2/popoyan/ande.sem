################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      id_commempl       LIKE commempl.id_commempl,
      nombre            LIKE commempl.nombre,
      apellido          LIKE commempl.apellido,
      direccion         LIKE commempl.direccion,
      telefono          LIKE commempl.telefono,
      
      usugrpid          LIKE commempl.usugrpid,
      usuLogin          LIKE commempl.usulogin,
      usuPwd            LIKE commempl.usupwd,
      
      codigo            LIKE commempl.codigo,
      id_commdep        LIKE commempl.id_commdep,
      id_commpue        LIKE commempl.id_commpue,
      jefe              LIKE commempl.jefe,
      estado            LIKE commempl.estado,
      
      comision          LIKE commempl.comision,
      numCuenta         LIKE commempl.numcuenta
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "catm0101"
END GLOBALS