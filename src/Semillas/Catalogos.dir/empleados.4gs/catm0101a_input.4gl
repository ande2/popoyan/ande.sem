################################################################################
# Funcion     : %M%
# Descripcion : Ingreso de un nuevo umdor 	
# Funciones   : input_umd() funcion para insertar datos
#              
#              
#               
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################

GLOBALS "catm0101a_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I' THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
     INPUT BY NAME  
      g_reg.id_commempl, g_reg.nombre, g_reg.apellido, 
      g_reg.direccion, g_reg.telefono, 
      g_reg.usuGrpId, g_reg.usuLogin, g_reg.usuPwd,
      g_reg.codigo, g_reg.id_commdep, g_reg.id_commpue, 
      g_reg.jefe, g_reg.estado, 
      g_reg.comision,  g_reg.numCuenta 
      ATTRIBUTES (WITHOUT DEFAULTS)

      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)
         LET g_reg.comision = 0
         DISPLAY BY NAME g_reg.comision

      AFTER FIELD nombre 
        IF g_reg.nombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

      AFTER FIELD apellido 
        IF g_reg.apellido IS NULL THEN
           CALL msg("Debe ingresar apellido")
           NEXT FIELD CURRENT
        END IF  

      AFTER FIELD codigo 
        {IF g_reg.codigo IS NULL THEN
           CALL msg("Debe ingresar codigo")
           NEXT FIELD CURRENT
        ELSE }
        IF g_reg.codigo IS NOT NULL THEN 
           SELECT * FROM commempl WHERE codigo = g_reg.codigo
           IF sqlca.sqlcode = 0 THEN
              CALL msg("Este código ya está utilizado, ingrese de nuevo")
              NEXT FIELD CURRENT 
           END IF 
        END IF 

      --AFTER FIELD id_commpue
        --SELECT 
      AFTER FIELD comision 
        IF g_reg.comision IS NULL THEN
           CALL msg("Debe ingresar comision")
           NEXT FIELD CURRENT
        END IF 
       
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
       

      {AFTER FIELD librob
      IF g_reg.librob IS NOT NULL THEN 
         IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
            IF existe_cat_nom(g_reg.librob) THEN 
               CALL msg("Este nombre ya esta siendo utilizado")
               LET g_reg.librob = NULL 
               NEXT FIELD CURRENT 
            END IF   
         END IF
      END IF} 
      
   END INPUT 

   ON ACTION ACCEPT
      
        IF g_reg.nombre IS NULL THEN
           CALL msg("Debe ingresar nombre")
           NEXT FIELD CURRENT
        END IF  

        IF g_reg.apellido IS NULL THEN
           CALL msg("Debe ingresar apellido")
           NEXT FIELD CURRENT
        END IF  

        IF g_reg.codigo IS NOT NULL THEN 
           IF g_reg.codigo <> u_reg.codigo THEN 
              SELECT * FROM commempl WHERE codigo = g_reg.codigo
              IF sqlca.sqlcode = 0 THEN
                 CALL msg("Este código ya está utilizado, ingrese de nuevo")
                 NEXT FIELD CURRENT 
              END IF
          END IF  
        END IF 

        IF g_reg.comision IS NULL THEN
           CALL msg("Debe ingresar comision")
           NEXT FIELD CURRENT
        END IF 

      --IF g_reg.cat_id IS NULL THEN
         --CALL msg("Debe ingresar Id.")
         --NEXT FIELD cat_id
      --END IF
      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF

      --CS
      --IF (operacion = 'I') OR (g_reg.idBautizo <> u_reg.idBautizo) THEN 
         --IF existe_cat_cod(g_reg.idBautizo) THEN 
            --CALL msg("Este codigo ya esta siendo utilizado")
            --LET g_reg.idBautizo = NULL 
            --NEXT FIELD idBautizo 
         --END IF   
      --END IF
      --CS
      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 
      
     ON ACTION CANCEL
        EXIT dialog
   END DIALOG
   
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION

--FUNCTION existe_cat_cod(l_cod)
--DEFINE l_cod LIKE bautizo.idBautizo
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE idBautizo = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener codigo")
   --END TRY  
   --RETURN resultado 
--END FUNCTION

--FUNCTION existe_cat_nom(l_cod)
--DEFINE l_cod LIKE bautizo.librob
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE librob = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener nombre")
   --END TRY  
   --RETURN resultado 
--END FUNCTION