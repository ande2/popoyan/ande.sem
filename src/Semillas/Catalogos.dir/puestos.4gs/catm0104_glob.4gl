################################################################################
# Funcion     : %M%
# id_commdep : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        id_commdep de la modificacion
#
################################################################################
SCHEMA db0001

GLOBALS 
TYPE 
   tDet RECORD 
      id_commpue  LIKE commpue.id_commpue,
      descripcion LIKE commpue.descripcion,
      id_commdep  LIKE commpue.id_commdep,
      estado      LIKE commpue.estado
   END RECORD

DEFINE
   reg_det DYNAMIC ARRAY OF tDet, 
   g_reg, u_reg tDet,
   dbname      STRING,
   
   condicion   STRING --Condicion de la clausula Where 
   CONSTANT    prog_name = "comm0104"
END GLOBALS