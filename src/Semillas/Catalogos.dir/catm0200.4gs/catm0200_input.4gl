################################################################################
# Funcion     : %M%
# Descripcion : Catalogo de Empresas
#               Funcion para definicion de globales
# Funciones   : 
#               
#               
#  
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "catm0200_glob.4gl"

FUNCTION captura_datos(operacion)
DEFINE operacion CHAR (1)
DEFINE resultado BOOLEAN
DEFINE w ui.Window
DEFINE f ui.Form

   LET resultado = FALSE 
   LET u_reg.* = g_reg.*
   IF operacion = 'I'THEN 
      
      INITIALIZE g_reg.* TO NULL
      DISPLAY BY NAME g_reg.*
   END IF
   DIALOG ATTRIBUTES(UNBUFFERED)
   INPUT BY NAME  
      g_reg.idEmpresa, g_reg.empNomCt, g_reg.empNomMd, g_reg.empNomLg,
      g_reg.flagPropia, g_reg.nomBd, g_reg.recBolCodigo, g_reg.recBolVersion 
    
   ATTRIBUTES (WITHOUT DEFAULTS)
      BEFORE INPUT
         CALL DIALOG.setActionHidden("close",true)

      AFTER FIELD empNomCt 
        IF g_reg.empNomCt IS NULL THEN
           CALL msg("Debe ingresar nombre corto")
           NEXT FIELD CURRENT
        END IF  

      AFTER FIELD empNomMd 
        IF g_reg.empNomMd IS NULL THEN
           CALL msg("Debe ingresar nombre medio")
           NEXT FIELD CURRENT
        END IF  

      AFTER FIELD empNomLg 
        IF g_reg.empNomLg IS NULL THEN
           CALL msg("Debe ingresar nombre largo")
           NEXT FIELD CURRENT
        END IF  

      AFTER FIELD flagPropia 
        IF g_reg.flagPropia IS NULL THEN
           CALL msg("Debe indicar si es empresa propia")
           NEXT FIELD CURRENT
        END IF  
        
            --IF (operacion = 'I') OR (g_reg.emNomCt <> u_reg.emNomCt) THEN 
            --IF existe_cat_cod(g_reg.idBautizo) THEN 
           
               --LET g_reg.idBautizo = NULL 
            
            --END IF   
         --END IF
       

      {AFTER FIELD librob
      IF g_reg.librob IS NOT NULL THEN 
         IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
            IF existe_cat_nom(g_reg.librob) THEN 
               CALL msg("Este nombre ya esta siendo utilizado")
               LET g_reg.librob = NULL 
               NEXT FIELD CURRENT 
            END IF   
         END IF
      END IF} 
      
   END INPUT 

   ON ACTION ACCEPT
      IF g_reg.empNomCt IS NULL THEN
         CALL msg("Debe ingresar nombre corto")
         NEXT FIELD empNomCt
      END IF

      IF g_reg.empNomMd IS NULL THEN
         CALL msg("Debe ingresar nombre medio")
         NEXT FIELD CURRENT
      END IF  

      IF g_reg.empNomLg IS NULL THEN
         CALL msg("Debe ingresar nombre largo")
         NEXT FIELD CURRENT
      END IF  

      IF g_reg.flagPropia IS NULL THEN
         CALL msg("Debe indicar si es finca propia")
         NEXT FIELD CURRENT
      END IF  
      
      --IF g_reg.cat_id IS NULL THEN
         --CALL msg("Debe ingresar Id.")
         --NEXT FIELD cat_id
      --END IF
      
      --IF g_reg.librob IS NULL THEN
         --CALL msg("Debe ingresar nombre")
         --NEXT FIELD librob
      --END IF
      --IF (operacion = 'I') OR (g_reg.cat_id <> u_reg.cat_id) THEN 
         --IF existe_cat_id(g_reg.cat_id) THEN 
            --CALL msg("Este Id. ya esta siendo utilizado")
            --LET g_reg.cat_id = NULL 
            --NEXT FIELD cat_id 
         --END IF   
      --END IF

      --CS
      --IF (operacion = 'I') OR (g_reg.idBautizo <> u_reg.idBautizo) THEN 
         --IF existe_cat_cod(g_reg.idBautizo) THEN 
            --CALL msg("Este codigo ya esta siendo utilizado")
            --LET g_reg.idBautizo = NULL 
            --NEXT FIELD idBautizo 
         --END IF   
      --END IF
      --CS
      {IF (operacion = 'I') OR (g_reg.librob <> u_reg.librob) THEN 
         IF existe_cat_nom(g_reg.librob) THEN 
            CALL msg("Este nombre ya esta siendo utilizado")
            LET g_reg.librob = NULL 
            NEXT FIELD librob 
         END IF   
      END IF}
      IF operacion = 'M' AND g_reg.* = u_reg.* THEN
         CALL msg("No se efectuaron cambios")
         EXIT DIALOG 
      END IF
      CASE box_gradato("Seguro de grabar")
         WHEN "Si"
            LET resultado = TRUE
            EXIT DIALOG
         WHEN "No"
            EXIT DIALOG 
         OTHERWISE
            CONTINUE DIALOG 
      END CASE 
      LET resultado = TRUE
      EXIT DIALOG 

   
      
   ON ACTION CANCEL
      EXIT dialog
   END DIALOG
   IF NOT resultado THEN
      LET g_reg.* = u_reg.*
      DISPLAY BY NAME g_reg.* 
   END IF 
   RETURN resultado 
END FUNCTION

--FUNCTION existe_cat_cod(l_cod)
--DEFINE l_cod LIKE bautizo.idBautizo
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE idBautizo = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener codigo")
   --END TRY  
   --RETURN resultado 
--END FUNCTION

--FUNCTION existe_cat_nom(l_cod)
--DEFINE l_cod LIKE bautizo.librob
--DEFINE resultado BOOLEAN 
--
   --LET resultado = FALSE 
   --TRY 
      --SELECT FIRST 1 * 
      --FROM bautizo
      --WHERE librob = l_cod
--
      --IF STATUS = 0 THEN
         --LET resultado = TRUE
      --END IF
   --CATCH 
      --CALL msgError(status, "Obtener nombre")
   --END TRY  
   --RETURN resultado 
--END FUNCTION