################################################################################
# Funcion     : %M%
# Descripcion : Funcion para modificar registros
# Funciones   : updel_init()
#	             delete_umd() 
#               update_umd()
#               pk_estado()
#               valida_modi()
#               verifica_modi()
#               existe_datos()
#               existe_dpro()
# Parametros
# Recibidos   :
# Parametros
# Devueltos   :
#
# SCCS ID No  : %Z% %W%
# Autor       : Carlos Santizo 
# Fecha       : %H% %T%
# Path        : %P%
#
# Control de cambios
#
# Programador Fecha                        Descripcion de la modificacion
#
################################################################################
GLOBALS "catm0203_glob.4gl"

FUNCTION update_init()
DEFINE strSql STRING 

   LET strSql = 
      "UPDATE pemmValvula ",
      "SET ",
      " nomValvula       = ? ",
      " WHERE idValvula  = ?"

   PREPARE st_modificar FROM strSql
END FUNCTION 

FUNCTION modifica()
   CALL encabezado("Modificar")
   --CALL info_usuario()
   
   IF captura_datos('M') THEN
      RETURN actualizar()
   ELSE 
      RETURN FALSE 
   END IF 
END FUNCTION 

FUNCTION actualizar()
   TRY
      EXECUTE st_modificar USING 
      g_reg.nomValvula,
      u_reg.idValvula
   CATCH 
      CALL msgError(sqlca.sqlcode,"Modificar Registro")
      RETURN FALSE 
   END TRY
   CALL msg ("Registro actualizado")
   RETURN TRUE 
END FUNCTION 

FUNCTION delete_init()
DEFINE strSql STRING 

   LET strSql =
      "DELETE FROM pemmValvula ",
      "WHERE idValvula = ? "
      
   PREPARE st_delete FROM strSql

END FUNCTION 

FUNCTION anular()
   IF NOT can_delete("pemDOrdPro", "idValvula", g_reg.idValvula) THEN
      CALL msg("No se puede eliminar, ya existe en orden de producción")
      RETURN FALSE 
   END IF
   IF NOT box_confirma("Esta seguro de anular el registro") THEN
      RETURN FALSE
   END IF 
   TRY 
      EXECUTE st_delete USING g_reg.idValvula
   CATCH 
      CALL msgError(sqlca.sqlcode,"Eliminar Registro")
      RETURN FALSE 
   END TRY
   CALL msg("Registro eliminado")
   RETURN TRUE 
END FUNCTION 