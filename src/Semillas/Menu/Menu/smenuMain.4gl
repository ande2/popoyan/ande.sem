IMPORT com 
IMPORT security 
GLOBALS "smenuGlobals.4gl"

DEFINE dbname varchar(15)
DEFINE vEmpresa varchar(50)
DEFINE vAndeSesId   char(25)

#########################################################################
## Function  : MAIN
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Funcion principal del programa
#########################################################################
MAIN
   DEFINE aui  om.DomNode
   DEFINE sm   om.DomNode
   DEFINE ok   SMALLINT
   DEFINE tit  STRING,
   prog_name2 	STRING
   
   
   OPTIONS
      INPUT NO WRAP,
      ON CLOSE APPLICATION STOP
   WHENEVER ERROR CONTINUE

   LET prog_name2 = prog_name||".log"   -- El progr_name es definido como constante en el arch. globals
    
   CALL STARTLOG(prog_name2)
   
   CALL ui.Interface.loadActionDefaults("actiondefaultsLogin")
   CALL ui.Interface.loadStyles("style")
   
   LET ok = login()
   
   IF (ok) THEN
      LET tit = "ANDE Software :: ", vEmpresa CLIPPED, "::", gUsuario.usuNombre CLIPPED, " (", gUsuario.usuLogin CLIPPED, ")"
      OPEN WINDOW menuBlank AT 1,1 WITH FORM "smenuBlank" ATTRIBUTES (TEXT = tit)
          
         CALL ui.Interface.setText(tit)
         LET aui = ui.Interface.getRootNode()
         LET sm = aui.createChild("StartMenu")
         CALL sm.setAttribute("text","Menu Principal")
         CALL men(cRaiz, sm)
         --DATABASE dbname
         MENU ""
            COMMAND "Salir"
               EXIT MENU
            COMMAND KEY(INTERRUPT)
               EXIT MENU
         END MENU
      CLOSE WINDOW menuBlank
   END IF
END MAIN

#########################################################################
## Function  : men()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Arma la estructura de menu
#########################################################################
FUNCTION men(p,sm)
   DEFINE arr    DYNAMIC ARRAY OF
      RECORD
         menId    LIKE menu.menId,
         menNombre   LIKE menu.menNombre,
         menTipo  LIKE menu.menTipo,
         menCmd   LIKE menu.menCmd,
         node     om.DomNode
      END RECORD
   DEFINE p       LIKE menu.menId
   DEFINE i, n, cnt  SMALLINT
   DEFINE qry     STRING
   DEFINE nom     LIKE menu.menNombre
   DEFINE sm      om.DomNode
   DEFINE smg     om.DomNode
   DEFINE smc     om.DomNode
   DEFINE vCommand STRING
   
   LET qry = "SELECT a.menId, a.menNombre, a.menTipo, a.menCmd FROM menu a, permiso b WHERE a.menId <> ", cRaiz, " AND a.menPadre = ", p, " AND b.perMenId = a.menId AND b.perGrpId = ", gUsuario.usuGrpId, " ORDER BY a.menId " 
   PREPARE prpM1 FROM qry
   DECLARE curM1 CURSOR FOR prpM1
   LET i = 1
   FOREACH curM1 INTO arr[i].menId, arr[i].menNombre, arr[i].menTipo, arr[i].menCmd
      LET i = i + 1
   END FOREACH
   LET i = i - 1
   IF i > 0 THEN
      FOR n = 1 TO i
         IF arr[n].menTipo = 0 THEN    -- Crea un submenu
            LET arr[n].node = createStartMenuGroup(sm,arr[n].menNombre)
         ELSE  
            DISPLAY "usulogin ", gUsuario.usuLogin
         -- Crea un comando
            LET vCommand = arr[n].menCmd CLIPPED, ' ', dbname, ' ', vAndeSesId, ' ',gUsuario.usuLogin
            --DISPLAY "dbname ", dbname
            LET arr[n].node = createStartMenuCommand(sm,arr[n].menNombre CLIPPED,vCommand CLIPPED, "circle.png")
            --DISPLAY "menus ", arr[n].*
         END IF
         CALL men(arr[n].menId,arr[n].node)
      END FOR
   END IF
END FUNCTION

#########################################################################
## Function  : login()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Firma del sistema
#########################################################################
FUNCTION login()
   DEFINE ok,c,s  SMALLINT
   DEFINE rec RECORD
       usuLogin  LIKE commempl.usulogin,
       usuPwd    LIKE commempl.usupwd,
       empresa    INT  
      END RECORD
   DEFINE qry     STRING
   LET ok = FALSE
   LET s = TRUE
   OPEN WINDOW menLogin AT 1,1 WITH FORM "smenLogin" ATTRIBUTES (TEXT = "Inicio de sesion...")
   CALL lib_cleanTinyScreen()
   CALL ui.Interface.setText("Inicio de sesion...")
   
   WHILE s
      LET int_flag = FALSE
      INPUT BY NAME rec.*  

        BEFORE INPUT 
          CALL combo_din2("empresa", "SELECT id_commemp, nombre FROM commemp WHERE dbname is not null")

         AFTER INPUT
            IF (NOT int_flag) THEN
               LET c = 0
               LET qry = "SELECT COUNT(*) FROM commempl WHERE usuLogin = ? AND usuPwd = ?"
               PREPARE prpL1 FROM qry
               EXECUTE prpL1 USING rec.usuLogin, rec.usuPwd INTO c
               IF STATUS = 0 THEN
                  IF c = 1 THEN
                     LET qry = "SELECT id_commempl, usuLogin, usuPwd, ",
                               " trim(nombre)||' '|| apellido, usuGrpId ",
                               " FROM commempl ",
                               " WHERE usuLogin = ? AND usuPwd = ?"
                     PREPARE prpL2 FROM qry
                     EXECUTE prpL2 USING rec.usuLogin, rec.usuPwd INTO gUsuario.*
                     IF STATUS = 0 THEN
                        
                        SELECT a.dbname, a.nombre INTO dbname, vEmpresa 
                        FROM commemp a, usuemp b, commempl u 
                        WHERE a.id_commemp = b.id_commemp
                          AND b.usuid = u.id_commempl
                          AND u.usuLogin = rec.usuLogin
                          AND a.id_commemp = rec.empresa
                        IF STATUS = 0 THEN
                           LET s = FALSE
                           LET ok = TRUE
                           CALL regUser(rec.usuLogin)
                        ELSE 
                           CALL msg("Usuario no tiene permiso en esta empresa")
                           NEXT FIELD usuLogin
                        END IF 
                     END IF
                  ELSE
                     CALL msg("Usuario/Password incorrecto")
                     NEXT FIELD usuLogin
                  END IF
               END IF
            ELSE
               LET int_flag = TRUE
               LET s = FALSE
            END IF
      END INPUT
   END WHILE
   CLOSE WINDOW menLogin
   RETURN ok
END FUNCTION

#########################################################################
## Function  : nm()
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Obtiene el nombre del menu
#########################################################################
FUNCTION nm(id)
   DEFINE id   LIKE menu.menId
   DEFINE nomb LIKE menu.menNombre
   DEFINE qry  STRING
   LET qry = "SELECT menNombre FROM menu WHERE menId = ?"
   PREPARE prpD1 FROM qry
   EXECUTE prpD1 USING id INTO nomb
   RETURN nomb
END FUNCTION

FUNCTION regUser(vUser)
 
DEFINE parNombre LIKE andeVarLog.parnombre
DEFINE fecha     LIKE andeVarLog.fecha

DEFINE vUser LIKE commempl.usuLogin

LET vAndeSesId = security.RandomGenerator.CreateRandomString(25)
LET parNombre = "LOGNAME"
LET fecha = TODAY 

TRY 
   INSERT INTO andeVarLog (andeSesId, parNombre, parValor, fecha) 
     VALUES(vAndeSesId, parNombre, vUser, fecha)
      {EXECUTE st_insertar USING 
         g_reg.numpar, g_reg.tippar, g_reg.nompar, g_reg.valchr, g_reg.userid, 
         g_reg.fecsis, g_reg.horsis}

   CATCH 
      CALL msgError(sqlca.sqlcode,"Grabar Registro")
      RETURN FALSE 
   END TRY


END FUNCTION 
