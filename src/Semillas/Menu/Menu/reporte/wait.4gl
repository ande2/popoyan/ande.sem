MAIN
DEFINE vos  STRING
DEFINE cmd  STRING
DEFINE vexe SMALLINT
DEFINE comando STRING
DEFINE hostname CHAR(25)
DEFINE vcuantos SMALLINT
DEFINE vusuario CHAR(20)
DEFINE vempresa CHAR(20)
DEFINE vprog_id INTEGER

LET vcuantos = 0
LET vusuario = FGL_GETENV("LOGNAME")

CALL ui.interface.frontcall("standard","getenv",["COMPUTERNAME"],[hostname])

	CONNECT TO "varios"

--se determina cuantos reportes tiene abiertos el usuario
	SELECT NVL(COUNT(*),0) 
	INTO vcuantos
	FROM sysmaster:syssessions syssessions 
	WHERE syssessions.username = 'iodbcusr'
	AND   syssessions.tty = hostname 

	IF vcuantos > 4 THEN
		#CLOSE WINDOW SCREEN
		CALL msg("Ha excedido el numero de reportes generados simultaneamente")
	ELSE

		LET comando =  arg_val(1)
		LET vempresa = arg_val(2)
		LET vprog_id = arg_val(3)

		WHENEVER ERROR CONTINUE
		INSERT INTO varios:mrepcuen VALUES(vempresa,vusuario,vprog_id,
                                         CURRENT,hostname)
		WHENEVER ERROR CONTINUE
		
		CALL fgl_settitle("Generando Reporte")
		CALL ui.interface.frontcall("standard","shellexec",[comando],[vexe])
	END IF
	
	DISCONNECT "varios"

END MAIN
