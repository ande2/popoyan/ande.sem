DATABASE db0001

#########################################################################
## Function  : GLOBALS
##
## Parameters: none
##
## Returnings: none
##
## Comments  : Declaracion de funciones globales
#########################################################################
GLOBALS
DEFINE gUsuario   RECORD --LIKE usuario.*
      usuId     LIKE commempl.id_commempl,
      usuLogin  LIKE commempl.usulogin,
      usuPwd    LIKE commempl.usupwd,
      usuNombre VARCHAR(30),
      usuGrpId  LIKE commempl.usugrpid
END RECORD 
CONSTANT cRaiz = -1
CONSTANT cErr = "Error. La operación no fue procesada."
CONSTANT cAddOK = "Registro agregado exitosamente"
CONSTANT cDelOK = "Registro eliminado"
CONSTANT cUpdOK = "Registro actualizado"
CONSTANT cPerOK = "Permisos actualizados"
END GLOBALS